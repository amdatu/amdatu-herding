/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.partitioner;

import java.util.List;
import java.util.Map;

/**
 * A partitioner partitions work across partitions. A partitioner is identified by a topic and is configured with
 * a partition count and has a simple API that returns the partition for a given resource. A resource can represent
 * anything that should be assigned a partition, e.g. a task.
 */
public interface Partitioner {

    /**
     * @return the topic for this partitioner.
     */
    String getTopic();

    /**
     * @return the amount of partitioner this partitioner is using.
     */
    int getPartitionCount();

    /**
     * Computes a partition for a resource.
     * @param resourceName the name of the resource to compute the partition for.
     * @return the computed partition.
     * @throws PartitionerException exception in case computing the partition fails.
     */
    int computePartitionForResource(String resourceName);

    /**
     * Discards the resource. I.e. the partitioner will no longer take the resource into
     * account for this node when computing resource partitions.
     * @param resourceName the name of the resource to discard.
     */
    void discardResource(String resourceName);

    /**
     * @return the current partition assignments.
     */
    Map<Integer, List<String>> getPartitionAssignments();

    /**
     * Tells the partitioner the partition count has changed.
     */
    void partitionsChanged();
}
