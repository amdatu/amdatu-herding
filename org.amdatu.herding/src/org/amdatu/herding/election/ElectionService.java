/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.election;

import java.util.Collection;

/**
 * The {@link ElectionService} provides information about the election for an {@link ElectionGroup}.
 */
public interface  ElectionService {

    /**
     * Is this node the group leader
     * @return <code>true</code> of this node is elected leader, <code>false</code> otherwise.
     */
    boolean isLeader();

    /**
     * Get the id of the leader node
     * @return The node identifier of the leader node
     */
    String getLeader();

    /**
     * Get all nodes participating in this election group
     * @return Collection containing the node identifier of each participant in this election group
     */
    Collection<String> getElectionCandidates();


}
