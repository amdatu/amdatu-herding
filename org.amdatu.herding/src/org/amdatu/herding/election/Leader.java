/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.election;

/**
 * Marker service that will be published on the node that's elected as leader.
 */
public class Leader {

    public static final String GROUP_NAME = "groupName";

    public static String groupFilter(String groupName) {
        return String.format("(%s=%s)", GROUP_NAME, groupName);
    }

}
