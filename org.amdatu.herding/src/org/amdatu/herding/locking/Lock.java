/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.locking;

/**
 * Interface for the locks provided but the {@link LockingService}
 *
 * <p>
 * <b>IMPORTANT</b><br>
 * As the {@link Lock} is a distributed lock the lock can become invalid when a node loses the ability to communicate
 * with the rest of the cluster. If this happens the {@link Lock} will be marked invalid, for long running operations
 * it's important to check validity of the lock regularly.
 * </p>
 */
public interface  Lock {

    /**
     * Lock the lock
     *
     * @param timeout timeout in milliseconds.
     * @return <code>true</code> if the lock has been obtained, <code>false</code> if the lock could not be acquired before timing out.
     * @throws InterruptedException If waiting for the lock was interrupted
     * @throws LockingException If an exception occurred while obtaining the lock
     */
    boolean lock(long timeout) throws LockingException, InterruptedException;

    /**
     * Release the lock
     * @throws LockingException If an exception occurred releasing obtaining the lock
     */
    void release() throws LockingException;

    /**
     * Check whether the lock is acquired
     *
     * @return <code>true</code> if the lock is both valid and acquired.
     */
    boolean acquired();

    /**
     * Check whether the lock is still valid.
     *
     * @return <code>true</code> if the lock is valid, <code>false</code> otherwise
     */
    boolean valid();

    /**
     * Get the node name of the node that currently holds this lock.
     *
     * @return node name of the node that holds the lock, <code>null</code> if no node holds the lock.
     * @throws LockingException If an exception occurred while retrieving the lock owner
     */
    String getLockOwner() throws LockingException;
}
