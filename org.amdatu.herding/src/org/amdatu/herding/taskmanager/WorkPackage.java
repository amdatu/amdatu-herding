/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.taskmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WorkPackage {

    private final String id;

    private final String name;

    private final List<TaskGroup> taskGroups;

    private WorkPackage(String id, String name, List<TaskGroup> taskGroups) {
        this.id = id;
        this.name = name;
        this.taskGroups = taskGroups;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<TaskGroup> getTaskGroups() {
        return taskGroups;
    }

    public static class Builder {

        private final List<TaskGroup> taskGroups = new ArrayList<>();
        private String name;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder addTaskGroup(TaskGroup taskGroup) {
            if (taskGroup == null) {
                throw new IllegalArgumentException("Task should not be null");
            }
            this.taskGroups.add(taskGroup);
            return this;
        }

        public WorkPackage build() {
            if (name == null || name.trim().isEmpty())  {
                throw new IllegalStateException("WorkPackage must have a name");
            }

            if (taskGroups.isEmpty()) {
                throw new IllegalStateException("WorkPackage must have at least one task group");
            }
            return new WorkPackage(UUID.randomUUID().toString(), name, taskGroups);
        }
    }
}
