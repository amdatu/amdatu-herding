/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.taskmanager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Description of the task to run.
 */
public class Task {

    public static final String INTERNAL_KEY_PREFIX = "AMDATU_HERDING_";

    private final String id;
    private final String name;
    private final String command;
    private final Map<String, String> context;
    
    private Task(String id, String name, String command, Map<String, String> context) {
        this.id = id;
        this.name = name;
        this.command = command;
        this.context = context;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public Map<String, String> getContext() {
        return context;
    }

    public static TaskBuilder builder() {
        return new TaskBuilder();
    }

    public static class TaskBuilder {

        private String id = UUID.randomUUID().toString();
        private String name;
        private String command;
        private Map<String, String> context = new HashMap<>();

        public TaskBuilder id(String identifier) {
            this.id = identifier;
            return this;
        }

        public TaskBuilder name(String name) {
            this.name = name;
            return this;
        }

        public TaskBuilder command(String command) {
            this.command = command;
            return this;
        }

        /**
         * Arguments required for running the task.
         *
         * The key prefix <code>AMDATU_HERDING_</code> is reserved for internal use and is not allowed to be used.
         *
         * @param key The key
         * @param value The value
         * @return the {@link TaskBuilder} to allow
         */
        public TaskBuilder setContextProperty(String key, String value) {
            if (key.startsWith(INTERNAL_KEY_PREFIX)) {
                throw new IllegalArgumentException(String.format("Task config key '%s' is not allowed, keys may not start with prefix: '%s' ", key, INTERNAL_KEY_PREFIX));
            }

            context.put(key, value);
            return this;
        }

        public Task build() {
            if (id == null) {
                throw new IllegalStateException("Task must have an id");
            }

            if (command ==  null) {
                throw new IllegalStateException("Task must have a command");
            }

            String taskName = this.name != null ? this.name : command + "(" + id + ")";
            return new Task(id, taskName, command, context);
        }

    }

}


