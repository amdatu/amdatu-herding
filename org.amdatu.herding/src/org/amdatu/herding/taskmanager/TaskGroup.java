/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.taskmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskGroup {

    private final String id;

    private final String name;

    private final List<Task> tasks;

    public TaskGroup(String id, String name, List<Task> tasks) {
        this.id = id;
        this.name = name;
        this.tasks = tasks;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public static class Builder {

        private String id = UUID.randomUUID().toString();
        private String name;
        private List<Task> tasks = new ArrayList<>();

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder addTask(Task task) {
            if (task == null) {
                throw new IllegalArgumentException("Task should not be null");
            }
            this.tasks.add(task);
            return this;
        }

        public TaskGroup build() {
            if (id == null) {
                throw new IllegalStateException("TaskGroup must have an id");
            }

            if (tasks.isEmpty()) {
                throw new IllegalStateException("TaskGroup must have at least one task");
            }
            return new TaskGroup(id, name, tasks);
        }
    }
}