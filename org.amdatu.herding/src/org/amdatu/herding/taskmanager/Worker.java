/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.taskmanager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

/**
 * Service capable of distributing work across a group of nodes. This service accepts {@link Task}s as the description
 * of the work to be done. These tasks will then be executed on one of the available nodes.
 *
 * The tasks are serializable work descriptions, in order to be able to execute these tasks a {@link WorkerContext} is
 * needed to turn this task into a {@link TaskRunnable} that can be executed.
 */
public interface Worker {

    /**
     *  Service property specifying the name of an {@link Worker} service.
     */
    String WORKER_NAME = "org.amdatu.herding.taskmanager.worker.name";

    /**
     * Submit work packages to the worker service. After this method terminated, the status of the work packages
     * can be monitored using the {@link #getStatus(String)} method.
     *
     * A work package can only be submitted once, to repeat the same tasks a new work package must be created.
     *
     * @param workPackages The work packages
     */
    void start(WorkPackage... workPackages);

    /**
     * Make the worker stop working on a work package that has been submitted earlier.
     *
     * @param workPackageId The identifier of the work package that should be cancelled.
     */
    void cancel(String workPackageId);


    /**
     * Make the worker delete the work package that has been submitted earlier.
     *
     * @param workPackageId The identifier of the work package that should be deleted.
     */
    void delete(String workPackageId);

    /**
     * Get the status of a work package that has been submitted earlier.
     *
     * @param workPackageId The identifier of the work package
     * @return The current status
     */
    Status getStatus(String workPackageId);

    /**
     * Get all work packages that are currently being worked on.
     *
     * @return Map where the map key is the work package identifier and the value is the work package name.
     */
    Map<String, String> listRunning();

    /**
     * Wait for a work package that has been submitted earlier to complete.
     *
     * @param workPackageId The identifier of the work package that should be cancelled.
     * @param timeout timeout in milliseconds
     * @return The status of the work package can be {@link Status#COMPLETED} or {@link Status#FAILED}
     * @throws TimeoutException if the work was not complete in time
     */
    Status awaitCompletion(String workPackageId, long timeout) throws InterruptedException, TimeoutException;

    /**
     * List the states of all the work packages including the jobs and tasks
     * @return map containing the states
     */
    default Map<String, String> listStates(){
        return Collections.emptyMap();
    }

    /**
     * Create an OSGi service filter to select a {@link Worker} service by name.
     *
     * @param name the name of the worker service
     * @return An OSGi service filter
     */
    static String workerFilter(String name) {
        return String.format("(%s=%s)", WORKER_NAME, name);
    }
}
