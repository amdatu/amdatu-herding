/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.partitionelection;

/**
 * The {@link PartitionElectionGroup} should be registered as a service by a node that wants to take part in the
 * partition election process for a group.
 *
 * When registering this service a node will take part in the election process for the group and can be elected
 * {@link PartitionLeader} for a partition.
 */
public class PartitionElectionGroup {

    private static final String NAME_REGEX = "[a-zA-Z][-._a-zA-Z0-9]*";

    private final String name;

    private final int partitions;

    public PartitionElectionGroup(String name, int partitions) {
        if (name == null) {
            throw new IllegalArgumentException("name is required");
        }
        if (!name.matches(NAME_REGEX)) {
            throw new IllegalArgumentException("name must match regex '" + NAME_REGEX  + "'");
        }
        if (partitions < 1) {
            throw new IllegalArgumentException("partitions can not be less than 1");
        }

        this.name = name;
        this.partitions = partitions;
    }

    public final String name() {
        return name;
    }

    public final int partitions() {
        return partitions;
    }

    @Override
    public String toString() {
        return "PartitionElectionGroup{" +
                "name='" + name + '\'' +
                ", partitions=" + partitions +
                '}';
    }
}
