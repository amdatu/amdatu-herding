/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.partitionelection;

import java.time.Instant;

public class PartitionLeader {

    private final String electionGroup;
    private final int partition;
    private final Instant leadingSince;


    public PartitionLeader(String electionGroup, int partition) {
        this.electionGroup = electionGroup;
        this.partition = partition;
        leadingSince = Instant.now();
    }

    public String getElectionGroup() {
        return electionGroup;
    }

    public int getPartition() {
        return partition;
    }

    public Instant getLeadingSince() {
        return leadingSince;
    }

    public static String groupFilter(String groupName) {
        return String.format("(%s=%s)", PartitionElectionConstants.GROUP_NAME, groupName);
    }

    public static String partitionFilter(String groupName, int partition) {
        String groupFilter = groupFilter(groupName);
        return String.format("(&%s(%s=%s))", groupFilter, PartitionElectionConstants.PARTITION, partition);

    }

}
