/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.testsupport.config;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

public class ConfigurationBuilder<C> {

    private final PropertyKeyRecorder<C> propertyKeyRecorder;
    private final Map<String, Object> properties = new LinkedHashMap<>();

    public ConfigurationBuilder(Class<C> configType) {
        if (Dictionary.class.isAssignableFrom(configType)) {
            this.propertyKeyRecorder = null;
        } else {
            this.propertyKeyRecorder = new PropertyKeyRecorder<>(configType);
        }
    }

    public <X> ValueReceiverImpl<X> set(String propertyKey) {
        return new ValueReceiverImpl<>(propertyKey);
    }

    public <X> ValueReceiverImpl<X> set(Function<C, X> propertyKeyRecordFunction) {
        if (propertyKeyRecorder == null) {
            throw new IllegalStateException("Configuration#set(Function) is not supported when using configType Dictionary");
        }
        propertyKeyRecorder.clear();

        propertyKeyRecordFunction.apply(propertyKeyRecorder.record());
        String key = propertyKeyRecorder.getPropertyKey();

        if (key == null) {
            throw new IllegalArgumentException("Failed to get property key");
        }

        return new ValueReceiverImpl<>(key);
    }

    public class ValueReceiverImpl<X> {

        private final String propertyKey;

        ValueReceiverImpl(String propertyKey) {
            this.propertyKey = propertyKey;
        }

        public ConfigurationBuilder<C> value(X value) {
            properties.put(propertyKey, value);
            return ConfigurationBuilder.this;
        }
    }

    Dictionary<String, Object> getConfigurationDictionary() {
        Dictionary<String, Object> dictionary = new Hashtable<>();
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                dictionary.put(entry.getKey(), value);
            }
        }
        return dictionary;
    }


}