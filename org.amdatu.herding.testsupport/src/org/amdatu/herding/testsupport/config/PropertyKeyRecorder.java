/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.testsupport.config;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Proxy object to record the name of the method that was called.
 *
 * NOTE: This is NOT a transparent proxy and the record method will always return null or the default primitive value
 * in case the return type is a primitive type.
 */
public  class PropertyKeyRecorder<T> implements InvocationHandler {

	private final T proxy;
	private String propertyKey;

	public PropertyKeyRecorder(Class<T> targetConfigurationClass) {
		this.proxy = (T) Proxy.newProxyInstance(targetConfigurationClass.getClassLoader(), new Class<?>[]{targetConfigurationClass}, this);
	}

	public String getPropertyKey() {
		return propertyKey;
	}

	public void clear() {
		propertyKey = null;
	}

	public T record() {
		return proxy;
	}

	public Object invoke(Object proxy, Method method, Object... args) {
		this.propertyKey = derivePropertyNameUsingMetaTypeConvention(method.getName());

		return getDefaultValue(method.getReturnType());
	}

	private static <T> T getDefaultValue(Class<T> clazz) {
		return (T) Array.get(Array.newInstance(clazz, 1), 0);
	}

	// see metatype spec, chapter 105.9.2 in osgi r6 cmpn.
	private String derivePropertyNameUsingMetaTypeConvention(String methodName) {
		StringBuilder sb = new StringBuilder(methodName);
		// replace "__" by "_" or "_" by ".": foo_bar -> foo.bar; foo__BAR_zoo -> foo_BAR.zoo
		for (int i = 0; i < sb.length(); i ++) {
			if (sb.charAt(i) == '_') {
				if (i < (sb.length() - 1) && sb.charAt(i+1) == '_') {
					// replace foo__bar -> foo_bar
					sb.replace(i, i+2, "_");
				} else {
					// replace foo_bar -> foo.bar
					sb.replace(i, i+1, ".");
				}
			} else if (sb.charAt(i) == '$') {
				if (i < (sb.length() - 1) && sb.charAt(i+1) == '$') {
					// replace foo__bar -> foo_bar
					sb.replace(i, i+2, "$");
				} else {
					// remove single dollar character.
					sb.delete(i, i+1);
				}
			}
		}
		return sb.toString();
	}

}
