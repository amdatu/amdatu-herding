/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.testsupport.config;

import java.io.IOException;
import java.util.Dictionary;
import java.util.function.Consumer;

import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import aQute.launchpad.Launchpad;

public class ConfigUtils {

    private ConfigUtils() {
        // prevent instantiation
    }

    public static Configuration updateConfiguration(Launchpad launchpad, String pid, Consumer<ConfigurationBuilder<Dictionary>> builder) {
        return updateConfiguration(launchpad, pid, Dictionary.class, builder);
    }

    public static <T> Configuration updateConfiguration(Launchpad launchpad, String pid, Class<T> configType, Consumer<ConfigurationBuilder<T>> builder) {
        try {
            ConfigurationAdmin configurationAdmin = launchpad.waitForService(ConfigurationAdmin.class, 5000)
                    .orElseThrow(() -> new IllegalStateException("Failed to update configuration with pid '" + pid + "', ConfigurationAdmin not available"));

            Configuration configuration = configurationAdmin.getConfiguration(pid, null);
            updateConfiguration(configuration, configType, builder);
            return configuration;
        } catch (IOException e) {
            throw new RuntimeException("Failed to update configuration with pid pid '" + pid + "'", e);
        }
    }

    public static <T> void updateConfiguration(Configuration configuration, Class<T> configType, Consumer<ConfigurationBuilder<T>> builder) {
        try {
            ConfigurationBuilder<T> configurationBuilder = new ConfigurationBuilder<>(configType);
            builder.accept(configurationBuilder);
            configuration.update(configurationBuilder.getConfigurationDictionary());
        } catch (IOException e) {
            throw new RuntimeException("Failed to update configuration with pid " + configuration.getPid(), e);
        }
    }

    public static <T> Configuration createFactoryConfiguration(Launchpad launchpad, String pid, Class<T> configType, Consumer<ConfigurationBuilder<T>> builder) {
        try {
            ConfigurationAdmin configurationAdmin = launchpad.waitForService(ConfigurationAdmin.class, 5000)
                    .orElseThrow(() -> new IllegalStateException("Failed to update configuration with pid '" + pid + "', ConfigurationAdmin not available"));

            ConfigurationBuilder<T> configurationBuilder = new ConfigurationBuilder<>(configType);

            builder.accept(configurationBuilder);

            Configuration configuration = configurationAdmin.createFactoryConfiguration(pid, null);
            configuration.update(configurationBuilder.getConfigurationDictionary());
            return configuration;

        } catch (IOException e) {
            throw new RuntimeException("Failed to update configuration with pid pid '" + pid + "'", e);
        }

    }
}
