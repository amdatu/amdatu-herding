/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.client;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import io.fabric8.kubernetes.client.jdkhttp.JdkHttpClientFactory;
import org.apache.felix.dm.*;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicReference;


public class Activator extends DependencyActivatorBase implements ComponentStateListener {

    private static final Logger LOG = LoggerFactory.getLogger(Activator.class);

    private final AtomicReference<KubernetesClient> clientReference = new AtomicReference<>();

    @Override
    public void init(BundleContext context, DependencyManager manager) {
        LOG.info("Init Kubernetes client - start");
        KubernetesClient client = new KubernetesClientBuilder()
                .withHttpClientFactory(new JdkHttpClientFactory())
                .build();
        clientReference.set(client);
        Component component = createComponent()
                .setInterface(KubernetesClient.class.getName(), null)
                .setImplementation(client)
                .add(this);
        manager.add(component);
        LOG.info("Init Kubernetes client - done");
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) {
        LOG.info("Kubernetes client - destroy start");
        KubernetesClient kubernetesClient = clientReference.getAndUpdate(null);
        if (kubernetesClient != null) {
            kubernetesClient.close();
        }
        LOG.info("Kubernetes client - destroy done");
    }

    @Override
    public void changed(Component c, ComponentState state) {
        LOG.info("Kubernetes client - state changed: {}", state);
    }
}
