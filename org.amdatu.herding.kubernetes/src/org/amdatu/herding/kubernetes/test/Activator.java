/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.test;

import org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.taskmanager.Task;
import org.amdatu.herding.taskmanager.TaskRunnable;
import org.amdatu.herding.taskmanager.WorkerContext;
import org.apache.felix.dm.*;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


public class Activator extends DependencyActivatorBase {

    private static final Logger LOG = LoggerFactory.getLogger(Activator.class);

    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);


    @Override
    public void init(BundleContext context, DependencyManager manager) {
        LOG.info("Init Kubernetes test - start");
        manager.add(createComponent()
                .setImplementation(new KubernetesImplConfigurator())
                .add(createServiceDependency()
                        .setService(ConfigurationAdmin.class)
                        .setRequired(true))
                .add(new ComponentStateListenerImpl("KubernetesImplConfigurator"))
        );
        manager.add(createComponent()
                .setImplementation(new KubernetesNodeLocker("locker1", "testlock", executorService))
                .add(createServiceDependency()
                        .setService(LockingService.class)
                        .setRequired(true))
                .add(new ComponentStateListenerImpl("KubernetesNodeLocker1"))
        );
        manager.add(createComponent()
                .setImplementation(new KubernetesNodeLocker("locker2", "testlock", executorService))
                .add(createServiceDependency()
                        .setService(LockingService.class)
                        .setRequired(true))
                .add(new ComponentStateListenerImpl("KubernetesNodeLocker2"))
        );
        manager.add(createComponent()
                .setInterface(WorkerContext.class, null)
                .setImplementation(new KubernetestTestWorkerContext())
        );
        LOG.info("Init Kubernetes test - done");
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) {
        LOG.info("Kubernetes test - destroy start");
        executorService.shutdownNow();
        LOG.info("Kubernetes test - destroy done");
    }


    private static class KubernetesNodeLocker {

        private final AtomicBoolean active = new AtomicBoolean(false);
        private final Random random = new Random();
        private final String lockerName;
        private final String lockName;
        private final ScheduledExecutorService executorService;

        private volatile LockingService lockingService;

        private Lock lock;

        public KubernetesNodeLocker(String lockerName, String lockName, ScheduledExecutorService executorService) {
            this.lockerName = lockerName;
            this.lockName = lockName;
            this.executorService = executorService;
        }

        public void start() {
            lock = lockingService.getLock(lockName);
            active.set(true);
            executorService.schedule(this::lock, random.nextInt(5) + 1L, TimeUnit.SECONDS);
        }

        public void stop() {
            active.set(false);
        }

        private void lock(){
            try {
                if(lock.lock(500)){
                    LOG.info("{}: GOT LOCK {}", lockerName, lockName);
                    Thread.sleep(random.nextInt(4) * 1000L);
                    lock.release();
                } else {
                    LOG.info("{}: FAILED TO GET LOCK {}", lockerName, lockName);
                }
            } catch (Exception e) {
                LOG.error("{}: Error while locking", lockerName, e);
            } finally {
                if(active.get()){
                    executorService.schedule(this::lock, random.nextInt(5) + 1L, TimeUnit.SECONDS);
                }
            }
        }
    }

    private static class KubernetesImplConfigurator {

        private volatile ConfigurationAdmin configurationAdmin;

        public void start() throws IOException {
            Dictionary<String, Object> configuration = new Hashtable<>();
            configuration.put("nodeName", System.getProperty("nodename"));
            configuration.put("namespace", "default");
            configurationAdmin
                    .getConfiguration(KubernetesConfiguration.PID, null)
                    .update(configuration);
        }
    }

    private static class KubernetestTestWorkerContext implements WorkerContext {

        public void start() {
            LOG.info("Kubernetes test worker context started");
        }

        public void stop() {
            LOG.info("Kubernetes test worker context stopped");
        }

        @Override
        public String name() {
            return "KubernetestTestWorkerContext";
        }

        @Override
        public int concurrentTasksPerNode() {
            return 4;
        }

        @Override
        public long timeoutPerTask() {
            return 5000L;
        }

        @Override
        public TaskRunnable createTaskRunnable(Task task) {
            return new TaskRunnable() {

                Random random = new Random();

                @Override
                public void run() {
                    LOG.info("Kubernetes test task started: {}", task);
                    try {
                        Thread.sleep(random.nextLong() % 5000);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        LOG.info("Kubernetes test task interrupted: {}", task);
                    }

                    LOG.info("Kubernetes test task completed: {}", task);
                }

                @Override
                public void cancel() {
                    LOG.info("Kubernetes test task cancelled: {}", task);
                }
            };
        }
    }

    private static class ComponentStateListenerImpl implements ComponentStateListener {

        private final String componentName;

        public ComponentStateListenerImpl(String componentName) {
            this.componentName = componentName;
        }

        @Override
        public void changed(Component c, ComponentState state) {
            LOG.info("Kubernetes test {} component- state changed: {}", componentName, state);
        }
    }

}
