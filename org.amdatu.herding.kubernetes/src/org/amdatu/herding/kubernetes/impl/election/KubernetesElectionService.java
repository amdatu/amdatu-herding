/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.election;

import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.election.Leader;
import org.amdatu.herding.kubernetes.impl.cluster.AbstractClusterStateProcessorService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterModel;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Leader election service implementation for Kubernetes.
 */
public final class KubernetesElectionService extends AbstractClusterStateProcessorService<KubernetesElectionModel> implements ElectionService {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesElectionService.class);
    private static final String ELECTION_PREFIX = "election";

    private final AtomicBoolean isActive = new AtomicBoolean(false);
    private final AtomicReference<Component> leaderComponent = new AtomicReference<>();

    // DM injected
    private volatile DependencyManager dependencyManager;
    private volatile Component component;
    private volatile KubernetesClusterService clusterService;
    private volatile ElectionGroup electionGroup;

    public KubernetesElectionService(ScheduledExecutorService executorService) {
        super(KubernetesElectionModel.class, executorService);
    }

    // DM config
    void updated(ElectionGroup electionGroup) throws ConfigurationException {
        // Note according the activator this is a factory, but the implementation as copied from
        // the zookeeper election service does not seem to support multiple instances?
        if (electionGroup == null) {
            return;
        }
        LOG.info("Update configuration for election {}", electionGroup.name());
        //noinspection ConstantConditions
        if (electionGroup.name() == null || electionGroup.name().trim().isEmpty()) {
            throw new ConfigurationException("name", "Name is required");
        }
        this.electionGroup = electionGroup;
        if (isActive.get()) {
            stop();
            start();
        }
    }

    // DM lifecycle
    void start()  {
        LOG.info("{}: Starting election service for {}", getIdentity(), electionGroup.name());
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        if (serviceProperties == null) {
            serviceProperties = new Hashtable<>();
        }
        serviceProperties.put(Leader.GROUP_NAME, electionGroup.name());
        component.setServiceProperties(serviceProperties);

        // Namespace the storage key so we do not collide with other elections
        // see multipleLeaderGroups test
        setModelKey(String.join("_", ELECTION_PREFIX, electionGroup.name()));
        registerProcessor();

        runElectionJoin();
        isActive.set(true);
    }

    void stop() {
        LOG.info("{}: Stopping election service for {}", getIdentity(), electionGroup.name());
        isActive.set(false);
        deregisterProcessor();
        runElectionLeave();
    }

    // AbstractClusterStateProcessorService
    @Override
    protected KubernetesClusterService getClusterService() {
        return clusterService;
    }

    // KubernetesClusterConfigProcessor
    @Override
    public void processWithModel(KubernetesClusterModel cluster, KubernetesElectionModel election) {
        handleElectionUpdate(cluster, election);
    }

    // ElectionService
    @Override
    public boolean isLeader() {
        ensureActive();
        return getIdentity().equals(getLeader());
    }

    @Override
    public String getLeader() {
        ensureActive();
        return getLocalModel().getLeader();
    }

    @Override
    public Collection<String> getElectionCandidates() {
        ensureActive();
        return getLocalModel().getParticipantNames();
    }

    // ClusterState
    private synchronized void runElectionJoin() {
        updateWithModel(this::handleElectionUpdate);
    }

    private synchronized void runElectionLeave() {
        updateWithModel((cluster, election) -> {
            for(ListIterator<KubernetesElectionModel.Participant> iter = election.getParticipants().listIterator(); iter.hasNext();){
                KubernetesElectionModel.Participant participant = iter.next();
                if(participant.getName().equals(getIdentity())){
                    LOG.info("{}: Removing self from participants: {}", getIdentity(), election);
                    iter.remove();
                    break;
                }
            }
            // If we are leader we transfer leadership to expedite the process
            if(getIdentity().equals(election.getLeader())){
                String newLeader = election.getParticipants().stream().map(KubernetesElectionModel.Participant::getName).findFirst().orElse(null);
                election.setLeader(newLeader);
                LOG.info("{}: Transferring leadership to {}: {}", getIdentity(), newLeader, election);
                getExecutorService().submit(this::runRemoveLeaderRegistration);
            }
        });
    }

    private void handleElectionUpdate(KubernetesClusterModel cluster, KubernetesElectionModel election) {
        // Update local state before tainting the model
        updateLeaderRegistration(election);

        // Update the election model as required
        handleElectionNameUpdate(election);
        handleElectionParticipantsUpdate(cluster, election);
        handleElectionLeadershipChange(election);
    }

    private void updateLeaderRegistration(KubernetesElectionModel election) {
        if(getIdentity().equals(election.getLeader())){
            if(leaderComponent.get() == null){
                getExecutorService().submit(this::runAddLeaderRegistration);
            }
        } else {
            if(leaderComponent.get() != null){
                getExecutorService().submit(this::runRemoveLeaderRegistration);
            }
        }
    }

    private void handleElectionNameUpdate(KubernetesElectionModel election){
        if(!electionGroup.name().equals(election.getName())){
            election.setName(electionGroup.name());
            LOG.info("{}: Updating election name to {}: {}", getIdentity(), electionGroup.name(), election);
        }
    }

    private void handleElectionParticipantsUpdate(KubernetesClusterModel cluster, KubernetesElectionModel election){
        if(cluster.getMember(getIdentity()) != null && election.getParticipant(getIdentity()) == null){
            election.addParticipant(getIdentity());
            LOG.info("{}: Adding self to participants: {}", getIdentity(), election);
        }
        for(ListIterator<KubernetesElectionModel.Participant> iter = election.getParticipants().listIterator(); iter.hasNext();){
            KubernetesElectionModel.Participant participant = iter.next();
            if(cluster.getMember(participant.getName()) == null){
                iter.remove();
                LOG.info("{}: Removing expired participant {}: {}", getIdentity(), participant.getName(), election);
            }
        }
    }

    private void handleElectionLeadershipChange(KubernetesElectionModel election){
        if(election.getLeader() == null || election.getParticipant(election.getLeader()) == null){
            LOG.info("{}: Taking over leadership from {}: {}", getIdentity(), election.getLeader(), election);
            election.setLeader(getIdentity());
        }
    }

    //DM components
    private void runAddLeaderRegistration() {
        LOG.info("{}: Starting leader component for {}", getIdentity(), electionGroup.name());
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Leader.GROUP_NAME, electionGroup.name());
        Component addComponent = dependencyManager.createComponent().setInterface(Leader.class.getName(), props).setImplementation(new Leader());
        dependencyManager.add(addComponent);
        leaderComponent.set(addComponent);
    }

    private void runRemoveLeaderRegistration() {
        LOG.info("{}: Stopping leader component for {}", getIdentity(), electionGroup.name());
        Component rmComponent = leaderComponent.get();
        if(rmComponent != null){
            dependencyManager.remove(rmComponent);
            leaderComponent.set(null);
        }
    }

    private void ensureActive() {
        if(!isActive.get()){
            throw new IllegalStateException("Election service is not active");
        }
    }
}
