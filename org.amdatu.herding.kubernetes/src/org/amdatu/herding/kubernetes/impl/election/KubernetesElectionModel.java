/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.election;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Model for the Kubernetes leader election state.
 */
public final class KubernetesElectionModel {

    public static final class Participant {

        private String name;

        public Participant() {
        }

        public Participant(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Participant{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    private String name;
    private String leader;
    private List<Participant> participants = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    @Override
    public String toString() {
        return "KubernetesElectionModel{" +
                "name='" + name + '\'' +
                ", leader='" + leader + '\'' +
                ", participants=" + participants +
                '}';
    }

    // Util
    public Participant getParticipant(String identity) {
        for (Participant participant : participants) {
            if (participant.getName().equals(identity)) {
                return participant;
            }
        }
        return null;
    }

    public void addParticipant(String identity) {
        if(getParticipant(identity) != null) {
            throw new IllegalArgumentException("Participant already exists: " + identity);
        }
        Participant participant = new Participant(identity);
        getParticipants().add(participant);
    }

    public Set<String> getParticipantNames(){
        return participants.stream().map(KubernetesElectionModel.Participant::getName).collect(Collectors.toSet());
    }
}
