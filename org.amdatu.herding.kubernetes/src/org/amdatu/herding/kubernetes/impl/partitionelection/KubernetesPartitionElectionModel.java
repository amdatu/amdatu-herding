/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.partitionelection;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class KubernetesPartitionElectionModel {

    public static class Participant {

        private String name;

        public Participant() {
        }

        public Participant(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "KubernetesPartitionElectionParticipant{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public static class Partition {

        private int id;
        private String owner;

        public Partition() {
        }

        public Partition(int id, String owner) {
            this.id = id;
            this.owner = owner;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "KubernetesPartitionElectionPartition{" +
                    "id=" + id +
                    ", owner='" + owner + '\'' +
                    '}';
        }
    }

    private String partitionCountOwner;
    private int partitionCount;
    private List<Participant> participants = new ArrayList<>();
    private List<Partition> partitions = new ArrayList<>();

    public String getPartitionCountOwner() {
        return partitionCountOwner;
    }

    public void setPartitionCountOwner(String partitionCountOwner) {
        this.partitionCountOwner = partitionCountOwner;
    }

    public int getPartitionCount() {
        return partitionCount;
    }

    public void setPartitionCount(int partitionCount) {
        this.partitionCount = partitionCount;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Partition> getPartitions() {
        return partitions;
    }

    public void setPartitions(List<Partition> partitions) {
        this.partitions = partitions;
    }

    @Override
    public String toString() {
        return "KubernetesPartitionElection{" +
                "partitionCountOwner='" + partitionCountOwner + '\'' +
                ", partitionCount=" + partitionCount +
                ", participants=" + participants +
                ", partitions=" + partitions +
                '}';
    }

    // Util
    public Participant getParticipant(String name) {
        for (Participant participant : participants) {
            if (participant.getName().equals(name)) {
                return participant;
            }
        }
        return null;
    }

    public void addParticipant(String identity) {
        if(getParticipant(identity) != null) {
            throw new IllegalArgumentException("Participant already exists: " + identity);
        }
        Participant participant = new Participant(identity);
        participants.add(participant);
    }

    public void removeParticipant(String identity) {
        participants.removeIf(p -> p.getName().equals(identity));
        partitions.stream().filter(p -> p.getOwner().equals(identity)).forEach(p -> p.setOwner(""));
    }

    public Set<Integer> getPartitionIds(String owner) {
        return getPartitions().stream()
                .filter(p -> p.getOwner().equals(owner))
                .map(KubernetesPartitionElectionModel.Partition::getId)
                .collect(Collectors.toSet());
    }

    public Set<String> getParticipantNames() {
        return getParticipants().stream()
                .map(KubernetesPartitionElectionModel.Participant::getName)
                .collect(Collectors.toSet());
    }

    public List<String> getPartitionOwners() {
        return getPartitions().stream()
                .map(KubernetesPartitionElectionModel.Partition::getOwner)
                .collect(Collectors.toList());
    }
}
