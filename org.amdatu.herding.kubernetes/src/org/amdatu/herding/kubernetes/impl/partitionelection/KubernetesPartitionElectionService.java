/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.partitionelection;

import org.amdatu.herding.kubernetes.impl.cluster.AbstractClusterStateProcessorService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterModel;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.amdatu.herding.partitioner.Partitioner;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Partition election service for Kubernetes.
 */
public final class KubernetesPartitionElectionService extends AbstractClusterStateProcessorService<KubernetesPartitionElectionModel> implements PartitionElectionService {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesPartitionElectionService.class);

    private static final String PARTITIONELECTIONGROUP_PREFIX = "partitionelection";

    private final Map<Integer, Component> activePartitionLeaderComponents = new HashMap<>();
    private final ExecutorService taskExecutor = Executors.newSingleThreadExecutor();
    private final AtomicBoolean isActive = new AtomicBoolean(false);
    private final PartitionElectionGroup partitionElectionGroup;

    // DM injected fields
    private volatile DependencyManager dependencyManager;
    private volatile Partitioner partitioner;
    private volatile KubernetesClusterService clusterService;

    public KubernetesPartitionElectionService(PartitionElectionGroup partitionElectionGroup, ScheduledExecutorService executorService) {
        super(KubernetesPartitionElectionModel.class, executorService);
        this.partitionElectionGroup = partitionElectionGroup;
    }

    // DM Lifecycle
    void start() {
        LOG.info("{}: Starting partition election service for group {}", getIdentity(), partitionElectionGroup);
        // Namespace the storage key so we do not collide with other elections
        // see multipleLeaderGroups test
        setModelKey(String.join("_", PARTITIONELECTIONGROUP_PREFIX, partitionElectionGroup.name()));
        registerProcessor();

        runPartitionElectionJoin();
        isActive.set(true);
        LOG.info("{}: Started Kubernetes partition election service for group {}", getIdentity(), partitionElectionGroup.name());
    }

    void stop() {
        LOG.info("{}: Stopping partition election service for group {}", getIdentity(), partitionElectionGroup);
        isActive.set(false);
        deregisterProcessor();
        runPartitionElectionLeave();
        taskExecutor.shutdown();
        LOG.info("{}: Stopped partition election service for group {}", getIdentity(), partitionElectionGroup);
    }

    // AbstractClusterStateProcessorService
    protected KubernetesClusterService getClusterService() {
        return clusterService;
    }

    // KubernetesClusterConfigProcessor
    @Override
    public void processWithModel(KubernetesClusterModel clusterState, KubernetesPartitionElectionModel partitionElectionState) {
        handlePartitionElectionUpdate(clusterState, partitionElectionState);
    }

    // PartitionElectionService Api
    @Override
    public boolean isLeader(int partition) {
        return getLeader(partition).equals(getIdentity());
    }

    @Override
    public String getLeader(int partition) {
        KubernetesPartitionElectionModel partitionElection = getLocalModel();
        if(partition < partitionElection.getPartitionCount()){
            return partitionElection.getPartitions().get(partition).getOwner();
        }
        throw new IllegalArgumentException("Partition " + partition + " does not exist");
    }

    @Override
    public Collection<String> getElectionCandidates() {
        return getLocalModel().getParticipants().stream()
                .map(KubernetesPartitionElectionModel.Participant::getName).collect(Collectors.toList());
    }

    // ClusterState
    private synchronized void runPartitionElectionJoin() {
        LOG.debug("{}: runPartitionElectionJoin - start...", getIdentity());
        boolean success = updateWithModel(this::handlePartitionElectionUpdate);
        if(!success){
            LOG.warn("{}: runPartitionElectionJoin - failed", getIdentity());
        }
    }

    private synchronized void runPartitionElectionLeave() {
        LOG.debug("{}: runPartitionElectionLeave - start...", getIdentity());
        boolean success = updateWithModel(this::removePartitionElectionGroupMembership);
        // We are leaving so just remove all local registrations regardless of success
        removeAllPartitionLeaderRegistrations();
        if(!success){
            LOG.warn("{}: runPartitionElectionLeave - failed", getIdentity());
        }
    }

    private void handlePartitionElectionUpdate(KubernetesClusterModel cluster, KubernetesPartitionElectionModel partitionElection) {
        // Update local state before tainting the model
        updatePartitionLeaderRegistrations(partitionElection);
        // Process model updates and ensure consistency
        handleParticipantUpdate(cluster, partitionElection);
        ensurePartitionCountChanges(partitionElection);
        ensurePartitionsAssigned(partitionElection);
        rebalancePartitions(partitionElection);
    }

    private void handleParticipantUpdate( KubernetesClusterModel cluster, KubernetesPartitionElectionModel partitionElection){
        if(cluster.getMember(getIdentity()) != null && partitionElection.getParticipant(getIdentity()) == null){
            LOG.info("{}: Adding self to participants", getIdentity());
            partitionElection.addParticipant(getIdentity());
        }
        List<String> toBeRemoved = partitionElection.getParticipants().stream()
                .map(KubernetesPartitionElectionModel.Participant::getName)
                .filter(name -> cluster.getMember(name) == null)
                .collect(Collectors.toList());
        toBeRemoved.forEach(p -> {
            LOG.info("{}: Removing expired participant {}", getIdentity(), p);
            partitionElection.removeParticipant(p);
        });
    }

    private void ensurePartitionCountChanges(KubernetesPartitionElectionModel partitionElection) {
        LOG.trace("{}: ensurePartitionCountChanges - start...", getIdentity());
        if(partitionElection.getPartitionCountOwner() != null
                && partitionElection.getParticipants().stream().anyMatch(p -> p.getName().equals(partitionElection.getPartitionCountOwner()))){
            LOG.trace("{}: ensurePartitionCountChanges - no changes!", getIdentity());
            return;
        }
        LOG.debug("{}: ensurePartitionCountChanges - owner changed from {} to {}", getIdentity(), partitionElection.getPartitionCountOwner(), getIdentity());
        partitionElection.setPartitionCountOwner(getIdentity());
        if(partitionElection.getPartitionCount() != partitionElectionGroup.partitions()){
            LOG.debug("{}: ensurePartitionCountChanges - count changed from {} to {}", getIdentity(), partitionElection.getPartitionCount(), partitionElectionGroup.partitions());
            if(partitionElection.getPartitionCount() > partitionElectionGroup.partitions()) {
                partitionElection.setPartitions(partitionElection.getPartitions().subList(0, partitionElectionGroup.partitions()));
            } else {
                IntStream.range(partitionElection.getPartitionCount(), partitionElectionGroup.partitions())
                        .forEach(i -> partitionElection.getPartitions().add(new KubernetesPartitionElectionModel.Partition(i, "")));
            }
            partitionElection.setPartitionCount(partitionElectionGroup.partitions());
            notifyPartitionsChanged();
        }
        LOG.trace("{}: ensurePartitionCountChanges - done!", getIdentity());
    }

    private void ensurePartitionsAssigned(KubernetesPartitionElectionModel partitionElection) {
        LOG.trace("{}: ensurePartitionsAssigned - start...", getIdentity());
        if(partitionElection.getParticipants().isEmpty()){
            LOG.warn("{}: No participants in partition election group {}", getIdentity(), partitionElectionGroup.name());
            partitionElection.getPartitions().forEach(p -> p.setOwner(""));
            return;
        }
        int nextParticipantIndex = 0;
        for(KubernetesPartitionElectionModel.Partition partition : partitionElection.getPartitions()){
            if (partition.getOwner() == null || partition.getOwner().isEmpty()){
                KubernetesPartitionElectionModel.Participant nextParticipant = partitionElection.getParticipants().get(nextParticipantIndex);
                nextParticipantIndex = (nextParticipantIndex + 1) % partitionElection.getParticipants().size();
                LOG.debug("{}: Assigning {} to {}", getIdentity(), partition, nextParticipant);
                partition.setOwner(nextParticipant.getName());
            }
        }
        LOG.trace("{}: ensurePartitionsAssigned - done!", getIdentity());
    }

    private void rebalancePartitions(KubernetesPartitionElectionModel partitionElection) {
        LOG.trace("{}: rebalancePartitions - start...", getIdentity());
        if(partitionElection.getParticipants().isEmpty()) {
            LOG.warn("{}: No participants in partition election group {}", getIdentity(), partitionElectionGroup.name());
            return;
        }
        int averageAssignments = partitionElection.getPartitionCount() / partitionElection.getParticipants().size();
        while(true) {
            Optional<KubernetesPartitionElectionModel.Participant> participantWithTooFewPartitions = partitionElection.getParticipants().stream()
                .filter(p -> partitionElection.getPartitions().stream().filter(partition -> partition.getOwner().equals(p.getName())).count() < averageAssignments)
                .findFirst();
            if(participantWithTooFewPartitions.isEmpty()) {
                break; // we are done
            }
            Optional<KubernetesPartitionElectionModel.Participant> participantWithTooManyPartitions = partitionElection.getParticipants().stream()
                    .filter(p -> partitionElection.getPartitions().stream().filter(partition -> partition.getOwner().equals(p.getName())).count() > averageAssignments)
                    .findFirst();
            if(participantWithTooManyPartitions.isEmpty()) {
                break; // we are done
            }
            partitionElection.getPartitions().stream()
                    .filter(p -> p.getOwner().equals(participantWithTooManyPartitions.get().getName()))
                    .findFirst()
                    .ifPresent(p -> p.setOwner(participantWithTooFewPartitions.get().getName()));
        }
        LOG.trace("{}: rebalancePartitions - done!", getIdentity());
    }

    private void removePartitionElectionGroupMembership(KubernetesClusterModel cluster, KubernetesPartitionElectionModel partitionElection) {
        LOG.trace("{}: removePartitionElectionGroupMembership - start..", getIdentity());
        partitionElection.removeParticipant(getIdentity());
        LOG.trace("{}: removePartitionElectionGroupMembership - done!", getIdentity());
    }

    // DM Components
    private void updatePartitionLeaderRegistrations(KubernetesPartitionElectionModel partitionElection){
        Set<Integer> myCurrentPartitions = partitionElection.getPartitionIds(getIdentity());
        if(!myCurrentPartitions.equals(activePartitionLeaderComponents.keySet())){
            taskExecutor.submit(() -> {
                runRemoveOldPartitionLeaderRegistrations(myCurrentPartitions);
                runAddNewPartitionLeaderRegistrations(myCurrentPartitions);
            });
        }
    }

    private void removeAllPartitionLeaderRegistrations(){
        taskExecutor.submit(() -> runRemoveOldPartitionLeaderRegistrations(Collections.emptySet()));
    }

    private void runAddNewPartitionLeaderRegistrations(Set<Integer> currentPartitions){
        currentPartitions.stream()
                .filter(p -> !activePartitionLeaderComponents.containsKey(p))
                .forEach(p -> {
                    LOG.debug("{}: updatePartitionLeaderRegistrations - registering leader for partition {}", getIdentity(), p);
                    Dictionary<String, Object> properties = new Hashtable<>();
                    properties.put(PartitionElectionConstants.GROUP_NAME, partitionElectionGroup.name());
                    properties.put(PartitionElectionConstants.PARTITION, p);
                    Component newComponent = dependencyManager.createComponent()
                            .setInterface(PartitionLeader.class.getName(), properties)
                            .setImplementation(new PartitionLeader(partitionElectionGroup.name(), p));
                    dependencyManager.add(newComponent);
                    activePartitionLeaderComponents.put(p, newComponent);

                });
    }

    private void runRemoveOldPartitionLeaderRegistrations(Set<Integer> currentPartitions){
        Set<Integer> toBeRemoved = activePartitionLeaderComponents.keySet().stream()
                .filter(p -> !currentPartitions.contains(p))
                .collect(Collectors.toSet());
        toBeRemoved.forEach(p -> {
            LOG.debug("{}: updatePartitionLeaderRegistrations - removing leader for partition {}", getIdentity(), p);
            Component c = activePartitionLeaderComponents.remove(p);
            dependencyManager.remove(c);
        });
    }

    private void notifyPartitionsChanged() {
        taskExecutor.submit(() -> partitioner.partitionsChanged());
    }
}
