/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.locking;

import org.amdatu.herding.kubernetes.impl.cluster.AbstractClusterStateProcessorService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterModel;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import static org.amdatu.herding.kubernetes.impl.Util.*;

/*
 * Locking service that uses the KubernetesClusterService for distributed locking.
 *
 * The locks are used for distributed locking and therefore do not check thread ownership. A lock instance may be
 * created by one thread and reused by another.
 *
 * However, the distributed lock is exclusive to a lock instance. A second instance for the same named lock will
 * fail to lock until the first instance releases the lock.
 *
 * Locks are coupled to the cluster membership and will expire when the owning member is removed.
 */
public final class KubernetesLockingService extends AbstractClusterStateProcessorService<KubernetesLockingModel> implements LockingService {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesLockingService.class);

    // Keeps track of active lock instances ids so we can expire orphaned locks that are not active.
    private static final Set<String> activeLockInstances = new CopyOnWriteArraySet<>(); //
    private static final AtomicBoolean isActive = new AtomicBoolean(false);

    // DM injected fields
    private volatile KubernetesClusterService clusterService;

    private KubernetesConfiguration configuration;

    public KubernetesLockingService(ScheduledExecutorService executorService) {
        super("lock", KubernetesLockingModel.class, executorService);
    }

    // DM Config
    void updated(KubernetesConfiguration configuration) {
        LOG.info("Locking service config {}", configuration);
        this.configuration = configuration;
    }

    // DM lifecycle
    void start() {
        LOG.info("{}: Starting locking service in namespace {}", getIdentity(), configuration.namespace());
        isActive.set(true);
        clusterService.addClusterConfigProcessor(this);
    }

    void stop() {
        LOG.info("{}: Stopping locking service", getIdentity());
        // Not actively removing locks, but will let them expire.
        isActive.set(false);
        clusterService.removeClusterConfigProcessor(this);
    }

    // AbstractClusterStateProcessorService
    @Override
    protected KubernetesClusterService getClusterService() {
        return clusterService;
    }

    @Override
    public void processWithModel(KubernetesClusterModel cluster, KubernetesLockingModel locking) {
        for(ListIterator<KubernetesLockingModel.Lock> iterator = locking.getLocks().listIterator(); iterator.hasNext(); ) {
            KubernetesLockingModel.Lock lock = iterator.next();
            String lockOwner = instanceIdToIdentity(lock.getOwner());
            if (cluster.getMember(lockOwner) == null) {
                // A member has been removed, remove the lock
                LOG.info("{}: Removing lock for unknown member {}", getIdentity(), lock);
                iterator.remove();
            } else if (getIdentity().equals(lockOwner) && !activeLockInstances.contains(lock.getOwner())) {
                // Should not happen but just in case we have an orphaned lock
                LOG.warn("{}: Removing orphaned inactive lock {}", getIdentity(), lock);
                iterator.remove();
            }
        }
    }

    // LockingService
    @Override
    public Lock getLock(String name) {
        ensureActive();
        return new KubernetesLock(safeResourceName(name), identityToInstanceId(getIdentity()));
    }

    @Override
    public Lock getReentrantLock(String name) {
        ensureActive();
        return new KubernetesReentrantLock(safeResourceName(name),  identityToInstanceId(getIdentity()));
    }

    // Util
    private static final AtomicLong LOCK_INSTANCE_ID = new AtomicLong(0);

    private static final Pattern INSTANCE_POSTFIX = Pattern.compile("-\\d+$");

    private static String identityToInstanceId(String identity) {
        return String.join("-", identity, String.valueOf(LOCK_INSTANCE_ID.incrementAndGet()));
    }

    private static String instanceIdToIdentity(String owner) {
        return INSTANCE_POSTFIX.matcher(owner).replaceAll("");
    }

    private void ensureActive() {
        if(!isActive.get()){
            throw new IllegalStateException("Locking service is not active");
        }
    }

    private class KubernetesLock implements Lock {

        protected final String lockName;
        protected final String instanceId;

        protected KubernetesLock(String lockName, String instanceId) {
            this.lockName = lockName;
            this.instanceId = instanceId;
            LOG.debug("{}: created lock {}/{}", getIdentity(), lockName, instanceId);
        }

        @Override
        public synchronized boolean lock(long timeout) throws LockingException, InterruptedException {
            LOG.trace("{}: Acquiring lock {}", getIdentity(), lockName);
            ensureValid();
            return acquireLock(timeout);
        }

        @Override
        public synchronized boolean acquired() {
            LOG.trace("{}: check acquired for lock {}", getIdentity(), lockName);
            if(!valid()) {
                return false;
            }
            return readWithModel(lockingModel -> {
                KubernetesLockingModel.Lock lock = lockingModel.getLock(lockName);
                return lock != null && instanceId.equals(lock.getOwner());
            });
        }

        @Override
        public synchronized boolean valid() {
            // Valid as long as the service is active
            return isActive.get();
        }

        @Override
        public synchronized String getLockOwner() throws LockingException {
            LOG.trace("{}: Getting owner for lock {}", getIdentity(), lockName);
            ensureValid();
            return readWithModel(lockingModel -> {
                KubernetesLockingModel.Lock lock = lockingModel.getLock(lockName);
                return lock != null && !isEmpty(lock.getOwner()) ? instanceIdToIdentity(lock.getOwner()) : null;
            });
        }

        @Override
        public synchronized void release() throws LockingException {
            LOG.trace("{}: Releasing lock {}", getIdentity(), lockName);
            ensureValid();
            AtomicBoolean released = new AtomicBoolean(false);
            boolean success = updateWithModel((cluster, locking) -> {
                if(locking.removeLock(lockName, instanceId)) {
                    released.set(true);
                }
            });
            activeLockInstances.remove(instanceId);
            if (!success || !released.get()) {
                throw new LockingException("Failed to release lock " + lockName);
            }
            LOG.debug("{}: Released lock {}", getIdentity(), lockName);
        }

        protected void ensureValid() throws LockingException {
            if (!valid()) {
                throw new LockingException("The lock is not valid.");
            }
        }

        protected boolean acquireLock(long timeout) {
            long startedAt = System.currentTimeMillis();
            while (true) {
                if (tryAcquireLock()) {
                    LOG.debug("{}: Acquired lock {}", getIdentity(), lockName);
                    activeLockInstances.add(instanceId);
                    return true;
                }
                long timeRemaining = timeout - (System.currentTimeMillis() - startedAt);
                if (timeRemaining <= 0L) {
                    LOG.debug("{}: Failed to acquire lock {}", getIdentity(), lockName);
                    return false;
                }
                sleep(Math.min(timeRemaining, configuration.lockRetryPeriod()));
            }
        }

        private boolean tryAcquireLock() {
            AtomicBoolean acquired = new AtomicBoolean(false);
            boolean success = updateWithModel((cluster, locking) -> {
                KubernetesLockingModel.Lock lock = locking.getLock(lockName);
                if (lock == null) {
                    lock = new KubernetesLockingModel.Lock(lockName, instanceId);
                    LOG.debug("{}: Creating new lock {}: {}", getIdentity(), lockName, lock);
                    locking.getLocks().add(lock);
                    acquired.set(true);
                } else if(lock.getOwner().isEmpty()) {
                    LOG.debug("{}: Taking over orphan lock {}: {}", getIdentity(), lockName, lock);
                    lock.setOwner(instanceId);
                    acquired.set(true);
                } else if(instanceId.equals(lock.getOwner())) {
                    LOG.warn("{}: Can not reacquire own lock {}: {}", getIdentity(), lockName, lock);
                } else {
                    LOG.warn("{}: Can not acquire valid lock {}: {}", getIdentity(), lockName, lock);
                }
            });
            return success && acquired.get();
        }
    }

    private class KubernetesReentrantLock extends KubernetesLock {

        private final AtomicInteger lockCount = new AtomicInteger(0);

        public KubernetesReentrantLock(String lockName, String ownerId) {
            super(lockName, ownerId);
        }

        @Override
        public synchronized boolean lock(long timeout) throws LockingException, InterruptedException  {
            ensureValid();
            if(acquired()){
                int count = lockCount.incrementAndGet();
                LOG.debug("{}: reacquired reentrant lock {} count {}", getIdentity(), lockName, count);
                return true;
            }
            if(acquireLock(timeout)){
                int count = lockCount.incrementAndGet();
                LOG.debug("{}: acquired reentrant lock {} count {}", getIdentity(), lockName, count);
                return true;
            }
            LOG.debug("{}: failed reentrant lock {}", getIdentity(), lockName);
            return false;
        }

        @Override
        public synchronized void release() throws LockingException {
            ensureValid();
            if (lockCount.decrementAndGet() == 0) {
                LOG.debug("{}: released reentrant lock {} at count 0", getIdentity(), lockName);
                super.release();
            } else {
                LOG.debug("{}: kept reentrant lock {} count {}", getIdentity(), lockName, lockCount.get());
            }
        }
    }
}