/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.locking;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for the Kubernetes leader election state.
 */
public final class KubernetesLockingModel {

    public static final class Lock {
        private String name;
        private String owner;

        public Lock() {
        }

        public Lock(String name, String owner) {
            this.name = name;
            this.owner = owner;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        @Override
        public String toString() {
            return "Lock{" +
                    "name='" + name + '\'' +
                    ", owner='" + owner + '\'' +
                    '}';
        }
    }

    private final List<Lock> locks = new ArrayList<>();

    public List<Lock> getLocks() {
        return locks;
    }

    @Override
    public String toString() {
        return "KubernetesLockingModel{" +
                "locks=" + locks +
                '}';
    }

    // Util
    public Lock getLock(String name) {
        return locks.stream().filter(lock -> lock.getName().equals(name)).findFirst().orElse(null);
    }

    public boolean removeLock(String name, String owner) {
        return locks.removeIf(lock -> lock.getName().equals(name) && lock.getOwner().equals(owner));
    }
}
