/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl;

import io.fabric8.kubernetes.client.KubernetesClient;
import org.amdatu.herding.ClusterService;
import org.amdatu.herding.CoordinatorStatusService;
import org.amdatu.herding.NodeService;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterStateProcessor;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.amdatu.herding.kubernetes.impl.election.KubernetesElectionService;
import org.amdatu.herding.kubernetes.impl.locking.KubernetesLockingService;
import org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration;
import org.amdatu.herding.kubernetes.impl.partitionelection.KubernetesPartitionElectionGroupAdapter;
import org.amdatu.herding.kubernetes.impl.partitioner.KubernetesPartitioner;
import org.amdatu.herding.kubernetes.impl.taskmanager.KubernetesWorker;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.taskmanager.Worker;
import org.amdatu.herding.taskmanager.WorkerContext;
import org.apache.felix.dm.*;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Activator extends DependencyActivatorBase {

    private static final Logger LOG = LoggerFactory.getLogger(Activator.class);

    private ScheduledExecutorService executorService;
    @Override
    public void init(BundleContext context, DependencyManager manager) {
        LOG.info("Init Kubernetes impl - start");

        // Create a shared single-threaded executor to offload async tasks to in a guaranteed serial manner.
        executorService = new ScheduledThreadPoolExecutor(1, r -> new Thread(r, "Herding-Worker"));

        manager.add(createComponent()
                .setInterface(new Class<?>[] {ClusterService.class, NodeService.class, CoordinatorStatusService.class, KubernetesClusterService.class}, null)
                .setImplementation(new KubernetesClusterService(executorService))
                .add(createConfigurationDependency()
                        .setPid(KubernetesConfiguration.PID)
                        .setConfigType(KubernetesConfiguration.class))
                .add(createServiceDependency().setService(KubernetesClient.class).setRequired(true))
                .add(createServiceDependency().setService(KubernetesClusterStateProcessor.class)
                        .setCallbacks("addClusterConfigProcessor", "removeClusterConfigProcessor")
                        .setRequired(false))
        );

        manager.add(createComponent()
                .setInterface(LockingService.class.getName(), null)
                .setImplementation(new KubernetesLockingService(executorService))
                .setAutoConfig(Component.class, false)
                .add(createConfigurationDependency()
                        .setPid(KubernetesConfiguration.PID)
                        .setConfigType(KubernetesConfiguration.class))
                .add(createServiceDependency().setService(KubernetesClusterService.class).setRequired(true))
        );

        manager.add(createFactoryComponent()
                .setFactoryPid(ElectionGroup.PID)
                .setConfigType(ElectionGroup.class)
                .setInterface(ElectionService.class.getName(), null)
                .setFactory(this, "newKubernetesElectionService")
                .add(createServiceDependency().setService(KubernetesClusterService.class).setRequired(true))
        );

        manager.add(createAdapterComponent()
                .setAdaptee(PartitionElectionGroup.class, null)
                .setFactory(this, "newKubernetesPartitionElectionGroupAdapter")
                .add(createServiceDependency().setService(KubernetesClusterService.class).setRequired(true))
                .setAutoConfig(Component.class, false)
        );

        manager.add(createAdapterComponent()
                .setAdaptee(PartitionElectionGroup.class, null)
                .setInterface(Partitioner.class.getName(), null)
                .setFactory(this, "newKubernetesPartitioner")
                .add(createServiceDependency().setService(KubernetesClusterService.class).setRequired(true))
        );

        manager.add(createAdapterComponent()
                .setAdaptee(WorkerContext.class, null)
                .setInterface(Worker.class, null)
                .setFactory(this, "newKubernetesWorker")
                .add(createServiceDependency().setService(KubernetesClusterService.class).setRequired(true))
        );
        LOG.info("Init Kubernetes impl - done");
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws InterruptedException {
        LOG.info("Destroy Kubernetes impl");
        executorService.schedule(() -> executorService.shutdownNow(), 4, TimeUnit.SECONDS);
    }

    private KubernetesElectionService newKubernetesElectionService() {
        return new KubernetesElectionService(executorService);
    }

    private KubernetesPartitionElectionGroupAdapter newKubernetesPartitionElectionGroupAdapter() {
        return new KubernetesPartitionElectionGroupAdapter(executorService);
    }

    private KubernetesPartitioner newKubernetesPartitioner() {
        return new KubernetesPartitioner(executorService);
    }

    private KubernetesWorker newKubernetesWorker() {
        return new KubernetesWorker(executorService);
    }
}
