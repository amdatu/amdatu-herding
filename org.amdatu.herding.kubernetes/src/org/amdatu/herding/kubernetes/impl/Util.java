/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl;

import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

public final class Util {

    // Best effort to make a string Kubernetes resource name safe
    // * contain no more than 253 characters
    // * contain only lowercase alphanumeric characters, '-' or '.'
    // * start with an alphanumeric character
    // * end with an alphanumeric character
    public static final Pattern SAFE_NAME = Pattern.compile("[a-z0-9]([-a-z0-9.]*[a-z0-9])?");
    public static final Pattern ILLEGAL_CHARS = Pattern.compile("[^-a-z0-9.]");
    public static final Pattern ILLEGAL_STARTCHAR = Pattern.compile("^[^a-z0-9]");
    public static final Pattern ILLEGAL_ENDCHAR = Pattern.compile("[^a-z0-9]$");

    public static String safeResourceName(String name){
        if(name == null || name.trim().isEmpty()){
            throw new IllegalArgumentException("Resource name must not be empty");
        }
        name = name.trim().toLowerCase();
        if(name.length() > 253){
            throw new IllegalArgumentException("Resource name must not exceed 253 chars");
        }
        if(SAFE_NAME.matcher(name).matches()){
            return name;
        }
        name = ILLEGAL_CHARS.matcher(name).replaceAll("-");
        name = ILLEGAL_STARTCHAR.matcher(name).replaceAll("a");
        name = ILLEGAL_ENDCHAR.matcher(name).replaceAll("z");
        return name;
    }

    public static void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static long jitter(long bound){
        long random = ThreadLocalRandom.current().nextLong();
        return Math.abs(random % bound);
    }

    public static String emptyToNull(String str){
        return str == null || str.isEmpty() ? null : str;
    }

    public static boolean isEmpty(String str){
        return str == null || str.isEmpty();
    }

    private Util() {
    }
}
