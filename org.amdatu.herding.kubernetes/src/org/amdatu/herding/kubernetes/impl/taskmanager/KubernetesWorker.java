/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.taskmanager;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.kubernetes.impl.cluster.AbstractClusterStateProcessorService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterModel;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.amdatu.herding.taskmanager.*;
import org.apache.felix.dm.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * TaskManager Worker implementation for Kubernetes.
 */
public final class KubernetesWorker extends AbstractClusterStateProcessorService<KubernetesWorkerModel> implements Worker {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesWorker.class);
    private static final String WORKPACKAGE_PREFIX = "workpackage";
    private static final long WORKPACKAGE_EXPIRE_MS = 60000L;
    private static final long AWAITCOMPLETIONR_RETRY_SLEEP_MS = 100L;

    private final AtomicBoolean isActive = new AtomicBoolean(false);
    private final Map<String, Long> completedWorkPackages = new HashMap<>();
    private final Map<String, TaskRunnableHandler> activeTaskHandlers = new HashMap<>();
    private final AtomicInteger threadCounter = new AtomicInteger(0);

    // DM injected fields
    private volatile Component component;
    private volatile WorkerContext workerContext;
    private volatile NodeService nodeService;
    private volatile KubernetesClusterService clusterService;

    private ExecutorService taskExecutor;

    public KubernetesWorker(ScheduledExecutorService executorService) {
        super(KubernetesWorkerModel.class, executorService);
    }

    // DM Lifecycle
    void start() {
        LOG.info("{}: Starting taskmanager worker for namespace {}", getIdentity(), getNamespace());
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(Worker.WORKER_NAME, workerContext.name());
        component.setServiceProperties(serviceProperties);
        setModelKey(String.join("_", WORKPACKAGE_PREFIX, getNamespace()));
        taskExecutor = new ThreadPoolExecutor(1, workerContext.concurrentTasksPerNode(), 10L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(), r -> new Thread(r, "Herding-TaskExecutor-" + getNamespace() + "-" + threadCounter.incrementAndGet()));
        registerProcessor();
        isActive.set(true);
        LOG.info("{}: Started taskmanager worker for namespace {}", getIdentity(), getNamespace());
    }

    void stop() {
        LOG.info("{}: Stopping taskmanager worker for namespace {}", getIdentity(), getNamespace());
        isActive.set(false);
        deregisterProcessor();
        taskExecutor.shutdownNow();
        LOG.info("{}: Stopped taskmanager worker for namespace {}", getIdentity(), getNamespace());
    }

    // AbstractClusterStateService
    @Override
    protected KubernetesClusterService getClusterService() {
        return clusterService;
    }

    // KubernetesClusterConfigProcessor
    @Override
    public void processWithModel(KubernetesClusterModel cluster, KubernetesWorkerModel worker) {
        handleTaskManagerUpdate(cluster, worker);
    }

    // Worker
    @Override
    public void start(WorkPackage... workPackages) {
        ensureActive();
        boolean success = updateWithModel((cluster, worker) -> {
            Set<String> existingIds = worker.getWorkPackageIds();
            for (WorkPackage workPackage : workPackages) {
                if (existingIds.contains(workPackage.getId())) {
                    throw new WorkerException("Workpackage with id " + workPackage.getId() + " already exists");
                }
                existingIds.add(workPackage.getId());
                KubernetesWorkerModel.WorkPackage wp = KubernetesWorkerModel.WorkPackage.from(workPackage);
                worker.addWorkPackage(wp);
                LOG.debug("{}: Starting new {}", getIdentity(), wp);
            }
        });
        if (!success) {
            throw new WorkerException("Failed to start workpackages");
        }
    }

    @Override
    public void cancel(String workPackageId) {
        ensureActive();
        boolean success = updateWithModel((cluster, worker) -> {
            KubernetesWorkerModel.WorkPackage workPackage = worker.getWorkPackage(workPackageId);
            if (workPackage == null) {
                throw new WorkerException("Workpackage with id " + workPackageId + " not found");
            }
            workPackage.setStatus(Status.FAILED);
            LOG.debug("{}: Cancelling {}", getIdentity(), workPackage);
        });
        if (!success) {
            throw new WorkerException("Failed to cancel workpackage");
        }
    }

    @Override
    public void delete(String workPackageId) {
        ensureActive();
        throw new WorkerException("Not implemented");
    }

    @Override
    public Status getStatus(String workPackageId) {
        ensureActive();
        KubernetesWorkerModel.WorkPackage workPackage = getLocalModel().getWorkPackage(workPackageId);
        if(workPackage == null){
            throw new WorkerException("Workpackage with id " + workPackageId + " not found");
        }
        return workPackage.getStatus();
    }

    @Override
    public Status awaitCompletion(String workPackageId, long timeout) throws TimeoutException, InterruptedException {
        ensureActive();
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < timeout) {
            Status status = getStatus(workPackageId);
            LOG.debug("{}: Awaiting completion of work package {}, current status: {}", getIdentity(), workPackageId, status);
            if (status == Status.COMPLETED || status == Status.FAILED) {
                return status;
            }
            Thread.sleep(AWAITCOMPLETIONR_RETRY_SLEEP_MS);
        }
        throw new TimeoutException("Timeout while waiting for workpackage " + workPackageId + " to complete");
    }

    @Override
    public Map<String, String> listStates() {
        ensureActive();
        Map<String, String> states = new HashMap<>();
        getLocalModel().getWorkPackages().forEach(
                workPackage -> {
                    states.put("Workflow: " + workPackage.getId(), workPackage.getStatus().name());
                    workPackage.getTaskGroups().forEach(
                            taskGroup -> {
                                states.put("TaskGroup: " + taskGroup.getId(), taskGroup.getStatus().name());
                                taskGroup.getTasks().forEach(
                                        task -> states.put("Task: " + task.getId(), task.getStatus().name())
                                );
                            }
                    );
                }
        );
        return states;
    }

    @Override
    public Map<String, String> listRunning() {
        ensureActive();
        return getLocalModel().getWorkPackages().stream()
                .filter(workPackage -> workPackage.getStatus() == Status.TODO || workPackage.getStatus() == Status.IN_PROGRESS)
                .collect(Collectors.toMap(KubernetesWorkerModel.WorkPackage::getId, KubernetesWorkerModel.WorkPackage::getName));
    }

    // Context helpers
    private String getNamespace() {
        return workerContext.name();
    }

    // Processing
    private synchronized void handleTaskManagerUpdate(KubernetesClusterModel cluster, KubernetesWorkerModel worker) {
        for(ListIterator<KubernetesWorkerModel.WorkPackage> iterator = worker.getWorkPackages().listIterator(); iterator.hasNext(); ) {
            KubernetesWorkerModel.WorkPackage workPackage = iterator.next();
            if (workPackage.getStatus() == Status.COMPLETED || workPackage.getStatus() == Status.FAILED) {
                // Completed/failed packages expire after WORKPACKAGE_EXPIRE_MS. Once that time has passed
                // the package can be dropped from the model.
                if(handleWorkPackageCleanup(workPackage)) {
                    iterator.remove();
                }
            } else {
                handleWorkPackageProcessing(cluster, workPackage);
            }
        }
    }

    private boolean handleWorkPackageCleanup(KubernetesWorkerModel.WorkPackage workPackage) {
        Long expiry = completedWorkPackages.get(workPackage.getId());
        if(expiry == null){
            LOG.debug("{}: Cleaning {}", getIdentity(), workPackage);
            completedWorkPackages.put(workPackage.getId(), System.currentTimeMillis() + WORKPACKAGE_EXPIRE_MS);
            workPackage.getTaskGroups().stream()
                    .flatMap(taskGroup -> taskGroup.getTasks().stream())
                    .forEach(task -> {
                        TaskRunnableHandler handler = activeTaskHandlers.remove(task.getId());
                        if (handler != null) {
                            LOG.info("{}: Removing {}", getIdentity(), handler);
                            handler.cancel();
                        }
                    });
        } else if(expiry < System.currentTimeMillis()){
            LOG.debug("{}: Expiring {}", getIdentity(), workPackage);
            completedWorkPackages.remove(workPackage.getId());
            return true;
        }
        return false;
    }

    private void handleWorkPackageProcessing(KubernetesClusterModel clusterState, KubernetesWorkerModel.WorkPackage workPackage) {
        LOG.debug("{}: Handling {}", getIdentity(), workPackage);

        // Select the first group for this package that has not been completed yet. Groups are processed one at a time.
        KubernetesWorkerModel.TaskGroup selectedTaskGroup = getNextTaskGroup(workPackage);
        if (selectedTaskGroup == null) {
            // No more task groups to process. Work package is completed.
            LOG.debug("{}: Completing {}", getIdentity(), workPackage);
            workPackage.setStatus(Status.COMPLETED);
            return;
        }
        LOG.debug("{}: Handling {}", getIdentity(), selectedTaskGroup);

        // Start and manage local handlers of previously adopted tasks in this group. Do this before
        // adopting a new task so we do not activate based on uncommitted state.
        handleActiveTasks(selectedTaskGroup);

        // Try to adopt a new unassigned task in this group which will be processed in a next epoch.
        if(adoptNewTask(selectedTaskGroup, clusterState.getMemberNames())){
            // Set package in progress once we adopted the first task
            if(workPackage.getStatus() == Status.TODO) {
                workPackage.setOwner(getIdentity());
                workPackage.setStatus(Status.IN_PROGRESS);
                LOG.debug("{}: Starting {}", getIdentity(), workPackage);
            }
            // Set group in progress once we adopted the first task
            if (selectedTaskGroup.getStatus() == Status.TODO) {
                selectedTaskGroup.setOwner(getIdentity());
                selectedTaskGroup.setStatus(Status.IN_PROGRESS);
                LOG.debug("{}: Starting {}", getIdentity(), selectedTaskGroup);
            }
        }

        // Check whether the group has been completed or failed and set status accordingly
        checkGroupCompletion(selectedTaskGroup, clusterState.getMemberNames());

        // If the group failed wee fail the entire package to prevent further processing.
        if(selectedTaskGroup.getStatus() == Status.FAILED){
            LOG.debug("{}: Failing {}", getIdentity(), workPackage);
            workPackage.setStatus(Status.FAILED);
        }
    }

    private KubernetesWorkerModel.TaskGroup getNextTaskGroup(KubernetesWorkerModel.WorkPackage workPackage) {
        for (KubernetesWorkerModel.TaskGroup taskGroup : workPackage.getTaskGroups()) {
            if (taskGroup.getStatus() != Status.COMPLETED) {
                return taskGroup;
            }
        }
        return null;
    }

    private void handleActiveTasks(KubernetesWorkerModel.TaskGroup taskGroup) {
        for(KubernetesWorkerModel.Task task : getOwnActiveTasks(taskGroup)){
            Status handlerStatus = handleActiveTask(task);
            switch (handlerStatus) {
                case FAILED:
                    // Based on the failingTask test we assume that a failed task will fail
                    // the taskgroup and workpackage as a whole
                    task.setStatus(Status.FAILED);
                    LOG.debug("{}: Failing {}", getIdentity(), task);
                    taskGroup.setStatus(Status.FAILED);
                    LOG.debug("{}: Failing {}", getIdentity(), taskGroup);
                    return; // stop processing
                case COMPLETED:
                    task.setStatus(Status.COMPLETED);
                    LOG.debug("{}: Completing {}", getIdentity(), task);
                    break;
                default:
                    LOG.debug("{}: Active {}", getIdentity(), task);
                    break;
            }
        }
    }

    private boolean adoptNewTask(KubernetesWorkerModel.TaskGroup taskGroup, Set<String> activeMembers) {
        int concurrentTasks = workerContext.concurrentTasksPerNode();
        int activeTaskCount = getActiveTaskCount();
        int openSlots = concurrentTasks - activeTaskCount;
        // If we have open slots we adopt a single new task per epoch to prevent adopting all tasks leaving other
        // nodes without work.
        if(openSlots > 0){
            KubernetesWorkerModel.Task selectedTask = getNextOpenTask(taskGroup, activeMembers);
            if (selectedTask != null) {
                selectedTask.setOwner(getIdentity());
                selectedTask.setStatus(Status.IN_PROGRESS);
                LOG.debug("{}: Adopting {}", getIdentity(), selectedTask);
            }
            return true;
        }
        return false;
    }

    private List<KubernetesWorkerModel.Task> getOwnActiveTasks(KubernetesWorkerModel.TaskGroup taskGroup) {
        return taskGroup.getTasks().stream()
                .filter(task -> task.getStatus() == Status.IN_PROGRESS && getIdentity().equals(task.getOwner()))
                .collect(Collectors.toList());
    }

    private KubernetesWorkerModel.Task getNextOpenTask(KubernetesWorkerModel.TaskGroup taskGroup, Set<String> activeMembers) {
        for (KubernetesWorkerModel.Task task : taskGroup.getTasks()) {
            if (task.getStatus() == Status.TODO &&
                    (task.getOwner() == null || !activeMembers.contains(task.getOwner()))) {
                return task;
            }
        }
        return null;
    }

    private void checkGroupCompletion(KubernetesWorkerModel.TaskGroup taskGroup, Set<String> activeMembers) {
        boolean completed = true;
        boolean failed = false;
        for(KubernetesWorkerModel.Task task : taskGroup.getTasks()){
            if(task.getStatus() == Status.IN_PROGRESS && !activeMembers.contains(task.getOwner())){
                task.setStatus(Status.FAILED);
                LOG.debug("{}: Failing abandoned {}", getIdentity(), task);
            }
            completed &= task.getStatus() == Status.COMPLETED;
            failed |= task.getStatus() == Status.FAILED;
        }
        if(completed){
            taskGroup.setStatus(Status.COMPLETED);
            LOG.debug("{}: Completing {}", getIdentity(), taskGroup);
        } else if(failed){
            taskGroup.setStatus(Status.FAILED);
            LOG.debug("{}: Failing {}", getIdentity(), taskGroup);
        }
    }

    private Status handleActiveTask(KubernetesWorkerModel.Task task) {
        TaskRunnableHandler taskHandler = activeTaskHandlers.get(task.getId());
        if (taskHandler == null) {
            TaskRunnable taskRunnable = workerContext.createTaskRunnable(task.toTask());
            taskHandler = new TaskRunnableHandler(task.getId(), taskRunnable, workerContext.timeoutPerTask());
            LOG.debug("{}: Submitting {}", getIdentity(), taskHandler);
            taskExecutor.submit(taskHandler);
            activeTaskHandlers.put(task.getId(), taskHandler);
        }
        return taskHandler.getStatus();
    }

    private int getActiveTaskCount() {
        return (int) activeTaskHandlers.values().stream()
                .filter(handler -> handler.getStatus() == Status.TODO || handler.getStatus() == Status.IN_PROGRESS)
                .count();
    }

    private void ensureActive() {
        if(!isActive.get()){
            throw new IllegalStateException("Worker is not active");
        }
    }

    public void setClusterService(KubernetesClusterService clusterService) {
        this.clusterService = clusterService;
    }

    private class TaskRunnableHandler implements Runnable {

        private final String id;
        private final TaskRunnable task;
        private final long timeout;
        private volatile Status status;
        private volatile long startedAt;

        public TaskRunnableHandler(String id, TaskRunnable task, long timeout) {
            this.id = id;
            this.task = task;
            this.timeout = timeout;
            this.status = Status.TODO;
        }

        public String getId() {
            return id;
        }

        public Status getStatus() {
            checkTimeout();
            return status;
        }

        public void setStatus(Status value) {
            status = value;
        }

        public long getStartedAt() {
            return startedAt;
        }

        public void setStartedAt(long value) {
            startedAt = value;
        }

        private void checkTimeout() {
            if (status != Status.IN_PROGRESS){
                return;
            }
            long duration = System.currentTimeMillis() - getStartedAt();
            if (duration > timeout) {
                LOG.warn("{}: Task timed out after {}ms {}", getIdentity(), duration, this);
                status = Status.FAILED;
                task.cancel();
            }
        }

        @Override
        public void run() {
            setStartedAt(System.currentTimeMillis());
            setStatus(Status.IN_PROGRESS);
            LOG.debug("{}: [{}] Running {}", getIdentity(), Thread.currentThread().getName(), this);
            try {
                task.run();
                if (getStatus() != Status.FAILED) {
                    setStatus(Status.COMPLETED);
                }
                LOG.debug("{}: Completed {}", getIdentity(), this);
            } catch (Exception e) {
                LOG.error("{}: Exception while running task {}", getIdentity(), getId(), e);
                setStatus(Status.FAILED);
            }
        }

        public void cancel() {
            if(getStatus() == Status.COMPLETED || getStatus() == Status.FAILED){
                return;
            }
            setStatus(Status.FAILED);
            task.cancel();
        }

        @Override
        public String toString() {
            return "TaskRunnableHandler{" +
                    "id='" + id + '\'' +
                    ", task=" + task +
                    ", timeout=" + timeout +
                    ", status=" + status +
                    ", startedAt=" + startedAt +
                    '}';
        }
    }
}