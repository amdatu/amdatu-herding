/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.taskmanager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.amdatu.herding.taskmanager.Status;

import java.util.*;
import java.util.stream.Collectors;

public class KubernetesWorkerModel {

    public abstract static class TaskState {
        protected String id;
        protected String name;
        protected String owner;
        protected Status status;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String value) {
            owner = value;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status value) {
            status = value;
        }
    }

    public static class WorkPackage extends TaskState {

        public static KubernetesWorkerModel.WorkPackage from(org.amdatu.herding.taskmanager.WorkPackage source){
            KubernetesWorkerModel.WorkPackage workPackage = new KubernetesWorkerModel.WorkPackage();
            workPackage.id = source.getId();
            workPackage.name = source.getName();
            workPackage.taskGroups = new ArrayList<>();
            for (org.amdatu.herding.taskmanager.TaskGroup taskGroup : source.getTaskGroups()) {
                workPackage.taskGroups.add(TaskGroup.from(taskGroup));
            }
            workPackage.status= Status.TODO;
            return workPackage;
        }

        private List<TaskGroup> taskGroups;

        public List<TaskGroup> getTaskGroups() {
            return taskGroups;
        }

        @Override
        public String toString() {
            return "WorkPackage{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", owner='" + owner + '\'' +
                    ", status=" + status +
                    '}';
        }
    }

    public static class TaskGroup extends TaskState {

        public static KubernetesWorkerModel.TaskGroup from(org.amdatu.herding.taskmanager.TaskGroup source) {
            KubernetesWorkerModel.TaskGroup taskGroup = new KubernetesWorkerModel.TaskGroup();
            taskGroup.id = source.getId();
            taskGroup.name = source.getName();
            taskGroup.tasks = new ArrayList<>();
            for (org.amdatu.herding.taskmanager.Task task : source.getTasks()) {
                taskGroup.tasks.add(KubernetesWorkerModel.Task.from(task));
            }
            taskGroup.status = Status.TODO;
            return taskGroup;
        }

        private List<KubernetesWorkerModel.Task> tasks;

        public  List<KubernetesWorkerModel.Task> getTasks() {
            return tasks;
        }

        @Override
        public String toString() {
            return "TaskGroup{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", owner='" + owner + '\'' +
                    ", status=" + status +
                    '}';
        }
    }

    public static class Task extends TaskState {

        public static KubernetesWorkerModel.Task from(org.amdatu.herding.taskmanager.Task source) {
            KubernetesWorkerModel.Task task = new KubernetesWorkerModel.Task();
            task.id = source.getId();
            task.name = source.getName();
            task.command = source.getCommand();
            task.context = new HashMap<>(source.getContext());
            task.status = Status.TODO;
            return task;
        }

        private String command;
        private Map<String, String> context;

        public String getCommand() {
            return command;
        }

        public Map<String, String> getContext() {
            return context;
        }

        public org.amdatu.herding.taskmanager.Task toTask() {
            org.amdatu.herding.taskmanager.Task.TaskBuilder builder =  org.amdatu.herding.taskmanager.Task.builder()
                    .id(id)
                    .name(name)
                    .command(command);
            context.forEach(builder::setContextProperty);
            return builder.build();
        }

        @Override
        public String toString() {
            return "Task{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", owner='" + owner + '\'' +
                    ", status=" + status +
                    '}';
        }
    }

    private List<WorkPackage> workPackages = new ArrayList<>();

    public List<WorkPackage> getWorkPackages() {
        return workPackages;
    }

    // Util
    public void addWorkPackage(WorkPackage workPackage) {
        workPackages.add(workPackage);
    }

    public WorkPackage getWorkPackage(String id) {
        return workPackages.stream()
            .filter(wp -> wp.getId().equals(id))
            .findFirst().orElse(null);
    }

    @JsonIgnore
    public Set<String> getWorkPackageIds(){
        return workPackages.stream()
            .map(KubernetesWorkerModel.WorkPackage::getId)
            .collect(Collectors.toSet());
    }
}
