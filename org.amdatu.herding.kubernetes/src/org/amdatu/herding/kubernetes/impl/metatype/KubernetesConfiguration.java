/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.metatype;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(pid = KubernetesConfiguration.PID,
        description = "Configuration for the Kubernetes based Amdatu Herding services")
public @interface KubernetesConfiguration {

    String PID = "org.amdatu.herding.kubernetes.impl";

    @AttributeDefinition(description = "Unique name for this node in the cluster.", required = true)
    String nodeName();

    @AttributeDefinition(description = "Name of the the cluster to join.", required = false)
    String clusterName() default "default";

    @AttributeDefinition(description = "Kubernetes namespace where resources are stored", required = false)
    String namespace() default "default";

    @AttributeDefinition(description = "Kubernetes name of the backing configmap", required = false)
    String configMap() default "kubernetes-cluster-service";

    @AttributeDefinition(description = "Duration in millis before a membership expires", required = false)
    long membershipDuration() default 30000L;

    @AttributeDefinition(description = "Duration in millis before starting membership renewal", required = false)
    long membershipRenewAfter() default 25000L;

    @AttributeDefinition(description = "Duration in millis between scheduled heartbeats", required = false)
    long membershipHeartbeat() default 2000L;

    @AttributeDefinition(description = "Duration in millis to backoff and accrue events before running an update", required = false)
    long stateUpdateBackoff() default 100L;

    @AttributeDefinition(description = "Upperbound of random jitter millis being added to updates and retries", required = false)
    long stateUpdateJitter() default 10L;

    @AttributeDefinition(description = "Number of retries when updating the configmap before giving up", required = false)
    int stateUpdateRetries() default 3;

    @AttributeDefinition(description = "Kubernetes lease retry period in milliseconds", required = false)
    long lockRetryPeriod() default 100L;
}
