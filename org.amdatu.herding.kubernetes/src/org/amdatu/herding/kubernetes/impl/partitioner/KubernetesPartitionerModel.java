/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.partitioner;

import java.util.ArrayList;
import java.util.Set;
import java.util.List;

public class KubernetesPartitionerModel {

    private List<Set<String>> partitions = new ArrayList<>();

    public KubernetesPartitionerModel() {
    }

    public KubernetesPartitionerModel(List<Set<String>> partitions) {
        this.partitions = partitions;
    }

    public List<Set<String>> getPartitions() {
        return partitions;
    }

    public Set<String> getPartition(int i) {
        return partitions.get(i);
    }

    public void setPartitions(List<Set<String>> partitions) {
        this.partitions = partitions;
    }

    @Override
    public String toString() {
        return "KubernetesPartitions{" +
                "partitions=" + partitions +
                '}';
    }
}
