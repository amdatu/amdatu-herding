/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.partitioner;

import org.amdatu.herding.kubernetes.impl.cluster.AbstractClusterStateProcessorService;
import org.amdatu.herding.kubernetes.impl.cluster.KubernetesClusterService;
import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.partitioner.PartitionerException;
import org.apache.felix.dm.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Partitioner that uses Kubernetes for semi-persistent storage.
 */
public final class KubernetesPartitioner extends AbstractClusterStateProcessorService<KubernetesPartitionerModel> implements Partitioner {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesPartitioner.class);
    private static final String PARTITIONER_PREFIX = "partitioner";

    private final AtomicBoolean isActive = new AtomicBoolean(false);

    // DM injected fields
    private volatile KubernetesClusterService clusterService;
    private volatile PartitionElectionGroup partitionElectionGroup;

    public KubernetesPartitioner(ScheduledExecutorService executorService) {
        super(KubernetesPartitionerModel.class, executorService);
    }

    // DM lifecycle
    void start(Component component) {
        LOG.info("{}: starting partitioner", getIdentity());
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(PartitionElectionConstants.GROUP_NAME, partitionElectionGroup.name());
        component.setServiceProperties(serviceProperties);
        setModelKey(String.join("_", PARTITIONER_PREFIX, partitionElectionGroup.name()));
        isActive.set(true);
        initializePartitions();
        LOG.info("{}: started partitioner", getIdentity());
    }

    void stop() {
        LOG.info("{}: stopping partitioner", getIdentity() );
        isActive.set(false);
        LOG.info("{}: stopped partitioner", getIdentity() );
    }

    protected KubernetesClusterService getClusterService() {
        return clusterService;
    }

    // Partitioner
    @Override
    public String getTopic() {
        ensureActive();
        return partitionElectionGroup.name();
    }

    @Override
    public int getPartitionCount() {
        ensureActive();
        return partitionElectionGroup.partitions();
    }

    @Override
    public int computePartitionForResource(String resourceName) {
        ensureActive();
        AtomicInteger result = new AtomicInteger(-1);
        boolean success = updateWithModel((cluster, partitions) -> {
            int smallestPartition = 0;
            int smallestPartitionSize = partitions.getPartition(smallestPartition).size();
            for(int i = 0; i < partitions.getPartitions().size(); i++){
                Set<String> partition = partitions.getPartition(i);
                if(partition.contains(resourceName)){
                    result.set(i);
                    return;
                }
                if(partition.size() < smallestPartitionSize){
                    smallestPartitionSize = partition.size();
                    smallestPartition = i;
                }
            }
            partitions.getPartition(smallestPartition).add(resourceName);
            result.set(smallestPartition);
        });
        if(!success){
            throw new PartitionerException("Failed to assign resource to partition " + resourceName);
        }
        LOG.debug("Assigned resource {} to partition {}", resourceName, result.get());
        return result.get();
    }

    @Override
    public void discardResource(String resourceName) {
        ensureActive();
        AtomicInteger result = new AtomicInteger(-1);
        boolean success = updateWithModel((cluster, partitions) -> {
            for(int i = 0; i < partitions.getPartitions().size(); i++){
                Set<String> partition = partitions.getPartition(i);
                if(partition.remove(resourceName)){
                    result.set(i);
                    return;
                }
            }
        });
        if(!success){
            throw new PartitionerException("Failed to discard resource " + resourceName);
        }
        LOG.debug("Discarded resource {} from partition {}", resourceName, result.get());
    }

    @Override
    public Map<Integer, List<String>> getPartitionAssignments() {
        ensureActive();
        return readWithModel(partitions -> {
            Map<Integer, List<String>> result = new HashMap<>();
            for(int i = 0; i < partitions.getPartitions().size(); i++){
                if(!partitions.getPartition(i).isEmpty()){
                    result.put(i, new ArrayList<>(partitions.getPartition(i)));
                }
            }
            return result;
        });
    }

    @Override
    public void partitionsChanged() {
        ensureActive();
        LOG.info("Partitions changed. Flushing partition assignments.");
        boolean success = updateWithModel((cluster, partitions) -> {
            partitions.getPartitions().clear();
            for (int i = 0; i < partitionElectionGroup.partitions(); i++) {
                partitions.getPartitions().add(new HashSet<>());
            }
        });
        if(!success){
            throw new PartitionerException("Failed to initialize partitions for election group " + partitionElectionGroup.name());
        }
    }

    // Helpers
    private void ensureActive() {
        if(!isActive.get()){
            throw new IllegalStateException("Partitioner is not active");
        }
    }

    private void initializePartitions() {
        boolean success = updateWithModel((cluster, partitions) -> {
            for (int i = partitions.getPartitions().size(); i < partitionElectionGroup.partitions(); i++) {
                partitions.getPartitions().add(new HashSet<>());
            }
        });
        if(!success){
            throw new RuntimeException("Failed to initialize partitions for election group " + partitionElectionGroup.name());
        }
    }
}
