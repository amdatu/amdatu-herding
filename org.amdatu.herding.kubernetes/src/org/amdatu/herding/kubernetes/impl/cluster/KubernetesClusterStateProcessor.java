/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.cluster;

/**
 * Interface for processing updates of the cluster state provided by the KubernetesClusterService.
 * Implementations of this interface must be registered with the KubernetesClusterService to receive updates.
 */
public interface KubernetesClusterStateProcessor {

    /**
     * Process an update of the cluster state. The context provides the current state and can be modified
     * to update it. However, as there is no guarantee that the update will be successful, care should
     * be taken not to update the local state until the changes are reflected in the next update.
     * @param context the context of the update
     */
    void process(KubernetesClusterStateUpdateContext context);
}
