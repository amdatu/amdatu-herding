/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.cluster;

import io.fabric8.kubernetes.api.model.ConfigMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Context object for processing updates of the cluster state provided by the KubernetesClusterService.
 */
public class KubernetesClusterStateUpdateContext {

    private final ConfigMap configMap;
    private final KubernetesClusterModel clusterModel;
    private final Map<String, String> data;
    private final List<Runnable> successCallbacks = new ArrayList<>();

    /**
     * Create a new context with the given ConfigMap and ClusterModel.
     * @param configMap the ConfigMap that is being processed
     * @param clusterModel the ClusterModel that is being processed
     */
    KubernetesClusterStateUpdateContext(ConfigMap configMap, KubernetesClusterModel clusterModel) {
        if (configMap != null) {
            this.configMap = configMap;
            this.data = new HashMap<>(configMap.getData());
        } else {
            this.configMap = null;
            this.data = new HashMap<>();
        }
        this.clusterModel = clusterModel;
    }

    /**
     * Get the current version of the ClusterModel. Processors can use this to read the member data.
     * @return the current version of the ClusterModel
     */
    public KubernetesClusterModel getClusterModel() {
        return clusterModel;
    }

    /**
     * Get the data that is being processed. Processors can modify this map to update their cluster state.
     * @return the current data of the ConfigMap
     */
    public Map<String, String> getData() {
        return data;
    }


    /**
     * Add a callback that will be executed when the cluster state update is successful.
     * @param callback the callback to execute
     */
    public void addSuccessCallback(Runnable callback) {
        successCallbacks.add(callback);
    }

    /**
     * Get the current version of the ConfigMap. Processors can use this to read the unmodified metadata and data.
     * @return the current version of the ConfigMap
     */
    ConfigMap getConfigMap() {
        return configMap;
    }

    /**
     * Indicates if the state has been updated, i.e. the backing ConfigMap has to be updated.
     * @return true if the state has been updated
     */
    boolean isUpdated() {
        return configMap == null || !configMap.getData().equals(data);
    }

    /**
     * Get the resource version of the ConfigMap. Processors can use this to read the version without fear of NPEs.
     * @return the current version of the ConfigMap
     */
    long getConfigMapVersion() {
        if (configMap == null || configMap.getMetadata() == null || configMap.getMetadata().getResourceVersion() == null) {
            return 0L;
        }
        return Long.parseLong(configMap.getMetadata().getResourceVersion());
    }

    /**
     * Indicates if the state is new, i.e. it has not been processed before and the backing ConfigMap is null.
     * @return true if the state is new
     */
    boolean isNew() {
        return configMap == null;
    }

    /**
     * Get the success callbacks that have been added to this context.
     * @return the success callbacks
     */
    List<Runnable> getSuccessCallbacks() {
        return successCallbacks;
    }
}
