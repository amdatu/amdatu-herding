/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.cluster;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Abstract base class for services that maintain a model in the cluster state provided by the KubernetesClusterService.
 * @param <T> the model type
 */
public abstract class AbstractClusterStateProcessorService<T> extends AbstractClusterStateService<T> implements KubernetesClusterStateProcessor {

    /**
     * Create a new service with the given model key, model class and executor service.
     * @param modelKey the model key used to store the model in the backing map
     * @param modelClass the model class used to (un)marshal the model
     * @param executorService a single threaded executor service use to execute simple tasks
     */
    protected AbstractClusterStateProcessorService(String modelKey, Class<T> modelClass, ScheduledExecutorService executorService) {
        super(modelKey, modelClass, executorService);
    }

    /**
     * Create a new service with the given model class and executor service. The model key will be derived from the
     * model class name.
     * @param modelClass the model class used to (un)marshal the model
     * @param executorService the executor service use to execute simple tasks
     */
    protected AbstractClusterStateProcessorService(Class<T> modelClass, ScheduledExecutorService executorService) {
        super(modelClass, executorService);
    }

    /**
     * Get the KubernetesClusterService that provides the cluster state. Musr be implemented by concrete implementations.
     * @return the KubernetesClusterService
     */
    protected abstract KubernetesClusterService getClusterService();

    /**
     * Get the identity of this cluster node which is the name of the cluster service.
     * @return the identity of this cluster node.
     */
    protected final String getIdentity() {
        return getClusterService().getName();
    }

    /**
     * Register this processor with the cluster service. This method should be called in the activate method of the
     * concrete implementation if the processor should be active.
     */
    protected final void registerProcessor(){
        getClusterService().addClusterConfigProcessor(this);
    }

    /**
     * Deregister this processor with the cluster service. This method should be called in the deactivate method of the
     * concrete implementation if the processor was registered before.
     */
    protected final void deregisterProcessor(){
        getClusterService().removeClusterConfigProcessor(this);
    }

    /**
     * Run an update with the latest cluster state. The processor will receive the current context and can modify it.
     * @param processor the processor to run
     * @return true if the update was successful, false otherwise
     */
    protected final boolean updateWithContext(KubernetesClusterStateProcessor processor) {
        return getClusterService().runClusterConfigUpdate(processor);
    }

    /**
     * Run an update with the latest cluster state. The processor will receive the current model and can modify it.
     * @param processor the processor to run
     * @return true if the update was successful, false otherwise
     */
    protected final boolean updateWithModel(BiConsumer<KubernetesClusterModel, T> processor) {
        return getClusterService().runClusterConfigUpdate(context -> handleUpdateWithModel(context, model -> {
            processor.accept(context.getClusterModel(), model);
            context.addSuccessCallback(() -> setLocalModel(model));
        }));
    }

    /**
     * Run a read with the latest cluster state. The processor will receive the current model for reading and potential
     * changes are not persisted.
     * @param processor the processor to run
     * @return the result of the processor
     */
    protected final <R> R readWithModel(Function<T,R> processor) {
        AtomicReference<R> result = new AtomicReference<>();
        getClusterService().runClusterConfigUpdate(context -> handleReadWithModel(context, model -> {
            result.set(processor.apply(model));
            return null;
        }));
        return result.get();
    }

    /**
     * Process a cluster state update. This default implementation loads the model from the context and delegates to
     * #processWithModel(KubernetesClusterModel, T) to process the update. Concrete implementation that register as
     * ClusterConfigProcessor should override #processWithModel(KubernetesClusterModel, T) to process the update.
     * @param context the update context
     */
    @Override
    public void process(KubernetesClusterStateUpdateContext context) {
        String encodedModel = context.getData().get(getModelKey());
        T model1 = loadModel(encodedModel);
        setLocalModel(model1);

        // Pass a copy of the model to the processor and write it back to the update context
        T model2 = loadModel(encodedModel);
        processWithModel(context.getClusterModel(), model2);
        storeModel(context, getModelKey(), model2);
    }

    /**
     * Process a cluster state update with the given model. This method should be overridden by concrete implementations
     * that register as ClusterConfigProcessor to process the update.
     * @param cluster the current cluster model
     * @param model the model to process
     */
    public void processWithModel(KubernetesClusterModel cluster, T model) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
