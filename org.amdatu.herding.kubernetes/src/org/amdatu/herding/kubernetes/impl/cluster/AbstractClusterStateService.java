/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.cluster;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.fabric8.kubernetes.api.model.ConfigMap;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Abstract base class for services that maintain a model in the cluster state. The model is encoded using the Jackson
 * ObjectMapper.
 * @param <T> the model type
 */
public abstract class AbstractClusterStateService<T> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    // The latest decoded model as received from the update context, unless explicitly overridden by setLatestModel.
    private final AtomicReference<T> localModel = new AtomicReference<>();
    private final AtomicReference<String> modelKey = new AtomicReference<>();
    private final ScheduledExecutorService executorService;
    private final Class<T> modelClass;

    /**
     * Create a new service with the given model key, model class and executor service.
     * @param modelKey the model key used to store the model in the backing map
     * @param modelClass the model class used to (un)marshal the model
     * @param executorService a single threaded executor service use to execute simple tasks
     */
    protected AbstractClusterStateService(String modelKey, Class<T> modelClass, ScheduledExecutorService executorService){
        this.modelKey.set(modelKey);
        this.modelClass = modelClass;
        this.executorService = executorService;
        // Ensure empty model at start to prevent NPEs downstream
        this.localModel.set(loadModel((String) null));
    }

    /**
     * Create a new service with the given model class and executor service. The model key will be derived from the
     * model class name.
     * @param modelClass the model class used to (un)marshal the model
     * @param executorService a single threaded executor service use to execute simple tasks
     */
    protected AbstractClusterStateService(Class<T> modelClass, ScheduledExecutorService executorService){
        this(modelClass.getName(), modelClass, executorService);
    }

    /**
     * Set the model key used to store the model in the backing map.
     * @param modelKey the model key
     */
    protected final void setModelKey(String modelKey) {
        this.modelKey.set(modelKey);
    }

    /**
     * Get the model key used to store the model in the backing map.
     * @return the model key
     */
    protected final String getModelKey(){
        return modelKey.get();
    }

    /**
     * Get the executor service used to execute simple tasks.
     * @return the executor service
     */
    protected final ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    /**
     * Get the latest model local that was committed to the cluster state or set through #setLatestModel().
     * @return the latest model
     */
    protected final T getLocalModel(){
        return localModel.get();
    }

    /**
     * Set the latest model that was committed to the cluster state.
     */
    protected final void setLocalModel(T model){
        localModel.set(model);
    }

    /**
     * Get the latest model that was committed to the cluster state, store it as the local model and apply the provided
     * processor to a copy. If the processor modifies the copy, the cluster service will attempt to commit it to the
     * cluster state, but this may fail if the model was modified concurrently by another processor.
     */
    protected final void handleUpdateWithModel(KubernetesClusterStateUpdateContext context, Consumer<T> processor) {
        // Store this latest committed model for read access through #getLatestModel().
        String encodedModel = context.getData().get(getModelKey());
        T model1 = loadModel(encodedModel);
        setLocalModel(model1);

        // Pass a copy of the model to the processor and write it back to the update context
        T model2 = loadModel(encodedModel);
        processor.accept(model2);
        storeModel(context, getModelKey(), model2);
    }

    /**
     * Get the latest model that was committed to the cluster state, store it as the local model and apply the provided
     * processor to a copy. Any potential modifications to the copy will not be committed to the cluster state.
     */
    protected final <R> void handleReadWithModel(KubernetesClusterStateUpdateContext context, Function<T, R> processor) {
        // Store this latest committed model for read access through #getLatestModel().
        String encodedModel = context.getData().get(getModelKey());
        T model1 = loadModel(encodedModel);
        setLocalModel(model1);

        // Pass a copy of the model to the processor and write it back to the update context
        T model2 = loadModel(encodedModel);
        processor.apply(model2);
    }

    /**
     * Load the model from the given context.
     * @param context the context
     * @return the model
     */
    protected final T loadModel(KubernetesClusterStateUpdateContext context) {
        String encodedModel = null;
        if(context != null){
            encodedModel = context.getData().get(getModelKey());
        }
        return loadModel(encodedModel);
    }

    /**
     * Load the model from the given config map.
     * @param configMap the config map
     * @return the model
     */
    protected final T loadModel(ConfigMap configMap) {
        String encodedModel = null;
        if(configMap != null){
            encodedModel = configMap.getData().get(getModelKey());
        }
        return loadModel(encodedModel);
    }

    /**
     * Load the model from the given encoded string.
     * @param encoded the encoded model
     * @return the model
     */
    protected final T loadModel(String encoded) {
        if(encoded != null){
            return decodeModel(encoded);
        }
        try {
            return modelClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Store the model in the given context.
     * @param context the context
     * @param modelKey the model key
     * @param model the model
     */
    protected final void storeModel(KubernetesClusterStateUpdateContext context, String modelKey, T model) {
        String encoded = encodeModel(model);
        context.getData().put(modelKey, encoded);
    }

    /**
     * Store the model in the given context.
     * @param context the context
     * @param model the model
     */
    protected final void storeModel(KubernetesClusterStateUpdateContext context, T model) {
        storeModel(context, getModelKey(), model);
    }

    /**
     * Encode the model. This uses the Jackson ObjectMapper to encode the model to a JSON string.
     * @param model the model
     * @return the encoded model
     */
    protected final String encodeModel(T model) {
        try {
            return MAPPER.writeValueAsString(model);
        } catch (Exception e) {
            throw new RuntimeException("Failed to encode model", e);
        }
    }

    /**
     * Decode the model. This uses the Jackson ObjectMapper to decode the model from a JSON string.
     * @param encoded the encoded model
     * @return the model
     */
    protected final  T decodeModel(String encoded) {
        try {
            return MAPPER.readValue(encoded, modelClass);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
