/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes.impl.cluster;

import io.fabric8.kubernetes.api.model.ConfigMap;
import io.fabric8.kubernetes.api.model.ConfigMapBuilder;
import io.fabric8.kubernetes.client.*;
import org.amdatu.herding.ClusterService;
import org.amdatu.herding.CoordinatorStatusService;
import org.amdatu.herding.NodeService;
import org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.amdatu.herding.kubernetes.impl.Util.jitter;
import static org.amdatu.herding.kubernetes.impl.Util.sleep;

/**
 * ClusterService implementation that maintains cluster state in a Kubernetes ConfigMap. It provides basic membership
 * logic and allows other services to register processors that can extend the cluster state.
 */
public final class KubernetesClusterService extends AbstractClusterStateService<KubernetesClusterModel>
        implements ClusterService, NodeService, CoordinatorStatusService {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesClusterService.class);

    private final List<KubernetesClusterStateProcessor> clusterStateProcessors = new CopyOnWriteArrayList<>();
    private final AtomicReference<Watch> configMapWatch = new AtomicReference<>();
    private final AtomicReference<ConfigMap> latestConfigMap = new AtomicReference<>();
    private final AtomicBoolean isActive = new AtomicBoolean(false);
    private final AtomicBoolean isHealthy = new AtomicBoolean(false);

    // DM injected services
    private volatile KubernetesClient client;

    private ScheduledFuture<?> heartBeatFuture;

    private KubernetesConfiguration configuration;
    private String clusterConfigMapName;
    private String clusterConfigMapNamespace;
    private long membershipRenewAfter;

    public KubernetesClusterService(ScheduledExecutorService executorService) {
        super("cluster", KubernetesClusterModel.class, executorService);
    }

    // DM Config
    void updated(KubernetesConfiguration configuration) {
        LOG.debug("Cluster Service configuration update: {}", configuration);
        if(configuration.nodeName() == null || configuration.nodeName().trim().isEmpty()){
            throw new IllegalArgumentException("nodeName is required");
        }
        this.configuration = configuration;
    }

    // DM Lifecycle
    void start() {
        LOG.info("{}: Starting cluster service with config {}", getName(), configuration);

        // Scope on the logical clustername so we can have multiple clusters in the same namespace
        clusterConfigMapName = String.join("-", configuration.clusterName(), configuration.configMap()).toLowerCase();
        clusterConfigMapNamespace = configuration.namespace();
        membershipRenewAfter = configuration.membershipDuration() - configuration.membershipRenewAfter();

        isActive.set(true);
        runClusterStateUpdate();
        heartBeatFuture = getExecutorService().scheduleAtFixedRate(this::runClusterStateUpdate, 0L, configuration.membershipHeartbeat(), TimeUnit.MILLISECONDS);
        LOG.info("{}: Started cluster service!", getName());
    }

    void stop() {
        LOG.info("{}: Stopping cluster service with config {}", getName(), configuration);
        runClusterStateLeave();
        isActive.set(false);
        closeConfigMapWatch();
        heartBeatFuture.cancel(true);
        LOG.info("{}: Stopped cluster service!", getName());
    }

    // NodeService
    @Override
    public String getName() {
        return configuration.nodeName();
    }

    // CoordinatorStatusService
    @Override
    public boolean isHealthy() {
        return isHealthy.get();
    }

    // ClusterService
    @Override
    public Collection<String> getMembers() {
        ensureActive();
        return getLocalModel().getMemberNames();
    }

    // KubernetesClusterService
    public void addClusterConfigProcessor(KubernetesClusterStateProcessor processor) {
        LOG.info("{}: Adding cluster config processor {}", getName(), processor);
        clusterStateProcessors.add(processor);
    }

    public void removeClusterConfigProcessor(KubernetesClusterStateProcessor processor) {
        LOG.info("{}: Removing cluster config processor {}", getName(), processor);
        clusterStateProcessors.remove(processor);
    }

    public boolean runClusterConfigUpdate(KubernetesClusterStateProcessor processor) {
        return runConfigMapUpdate(processor);
    }

    // Cluster State
    private boolean runClusterStateUpdate(){
        LOG.trace("{}: Cluster state update running..", getName());
        ensureConfigMapWatch();
        boolean success = runConfigMapUpdate(context -> {
            KubernetesClusterModel cluster = context.getClusterModel();
            setLocalModel(cluster);
            processMembershipUpdate(cluster);
            processMembershipExpiry(cluster);
            for(KubernetesClusterStateProcessor processor : clusterStateProcessors){
                processor.process(context);
            }
            context.addSuccessCallback(() -> setLocalModel(cluster));
        });
        LOG.trace("{}: Cluster state update finished.", getName());
        return success;
    }

    private boolean runClusterStateLeave(){
        LOG.trace("{}: Cluster state leave running..", getName());
        return runConfigMapUpdate(context -> {
            KubernetesClusterModel cluster = context.getClusterModel();
            cluster.removeMember(getName());
            storeModel(context, cluster);
            context.addSuccessCallback(() -> setLocalModel(cluster));
        });
    }

    private void processMembershipUpdate(KubernetesClusterModel cluster) {
        KubernetesClusterModel.Member member = cluster.getMember(getName());
        if(member == null){
            cluster.addMember(getName(), newMembershipExpiration());
            LOG.debug("{}: processMembershipUpdate - adding self: {}", getName(), cluster);
        } else if(clusterMembershipExpiring(member.getExpires())){
            member.setExpires(newMembershipExpiration());
            LOG.debug("{}: processMembershipUpdate - updating expires: {}", getName(), cluster);
        }
    }

    private void processMembershipExpiry(KubernetesClusterModel cluster) {
        for (Iterator<KubernetesClusterModel.Member> it = cluster.getMembers().iterator(); it.hasNext(); ) {
            KubernetesClusterModel.Member member = it.next();
            if(clusterMembershipExpired(member.getExpires())){
                LOG.trace("{}: handleMembershipExpiry - removing expired member {}", getName(), member.getName());
                it.remove();
            }
        }
    }

    private long newMembershipExpiration() {
        return System.currentTimeMillis() + configuration.membershipDuration();
    }

    private boolean clusterMembershipExpiring(long expiration) {
        return expiration - System.currentTimeMillis() < membershipRenewAfter;
    }

    private boolean clusterMembershipExpired(long expiration) {
        return expiration < System.currentTimeMillis();
    }

    // ConfigMap processing and updates are synchronized to prevents concurrent runs that would certainly fail
    // on the optimistic resource version lock.
    private synchronized boolean runConfigMapUpdate(KubernetesClusterStateProcessor processor) {
        for(int i = 0; i < configuration.stateUpdateRetries(); i++){
            ensureActive();
            try {
                LOG.trace("{}: ConfigMap update try {} starting", getName(), i);
                ConfigMap updated = tryConfigMapUpdate(processor);
                setLatestConfigMap(updated);
                LOG.trace("{}: ConfigMap update succeeded: {}", getName(), updated);
                return true;
            } catch (Exception e){
                if(e instanceof KubernetesClientException && ((KubernetesClientException) e).getCode() == 409) {
                    // Optimistic lock failed, probably because another node updated the ConfigMap concurrently, so
                    // we retry the update from the start.
                    long delay = jitter(configuration.stateUpdateJitter());
                    LOG.debug("{}: ConfigMap update try {} failed. Retrying in {}ms", getName(), i, delay);
                    sleep(delay);
                } else {
                    LOG.warn("{}: ConfigMap update threw exception: {}", getName(), e.getMessage(), e);
                    isHealthy.set(false);
                    break;
                }
            }
        }
        LOG.warn("{}: ConfigMap update  failed permanently", getName());
        return false;
    }

    private ConfigMap tryConfigMapUpdate(KubernetesClusterStateProcessor processor) {
        // Set local model to the current state
        ConfigMap configMap = loadCurrentConfigMap();
        String encodedModel = configMap == null ? null : configMap.getData().get(getModelKey());
        setLocalModel(loadModel(encodedModel));
        // Run update with a copy of the model
        KubernetesClusterStateUpdateContext updateContext = new KubernetesClusterStateUpdateContext(configMap, loadModel(encodedModel));
        processor.process(updateContext);
        storeModel(updateContext, updateContext.getClusterModel());
        if(updateContext.isUpdated()){
            LOG.trace("{}: ConfigMap modified -> attempting update", getName());
            configMap = createOrUpdateConfigMap(updateContext);
            updateContext.getSuccessCallbacks().forEach(Runnable::run);
            isHealthy.set(true);
        } else {
            LOG.trace("{}: ConfigMap not modified -> skipping update", getName());
        }
        return configMap;
    }

    private ConfigMap createOrUpdateConfigMap(KubernetesClusterStateUpdateContext context) {
        if (context.isNew()) {
            ConfigMap configMap = new ConfigMapBuilder().withNewMetadata().withName(clusterConfigMapName).endMetadata().withData(context.getData()).build();
            return client.configMaps()
                    .inNamespace(clusterConfigMapNamespace)
                    .resource(configMap)
                    .create();
        }
        context.getConfigMap().setData(context.getData());
        return client.configMaps()
                .inNamespace(clusterConfigMapNamespace)
                .resource(context.getConfigMap())
                .update();
    }

    private ConfigMap loadCurrentConfigMap() {
        return client.configMaps()
                .inNamespace(clusterConfigMapNamespace)
                .withName(clusterConfigMapName)
                .get();
    }

    // Helpers
    private void ensureConfigMapWatch() {
        if(configMapWatch.get() == null) {
            configMapWatch.set(openConfigMapWatch());
        }
    }

    private Watch openConfigMapWatch() {
        try {
            return client.configMaps()
                .inNamespace(clusterConfigMapNamespace)
                .withName(clusterConfigMapName)
                .watch(new ConfigMapWatcher());
        } catch (Exception e){
            LOG.warn("{}: Error while opening configmap watch", getName(), e);
        }
        return null;
    }

    private void closeConfigMapWatch() {
        Watch watch = configMapWatch.getAndSet(null);
        if(watch != null){
            try {
                watch.close();
            } catch (Exception e){
                LOG.warn("{}: Error while closing configmap watch", getName(), e);
            }
        }
    }

    private void setLatestConfigMap(ConfigMap configMap) {
        latestConfigMap.set(configMap);
    }

    private ConfigMap getLatestConfigMap() {
        return latestConfigMap.get();
    }

    private long getVersion(ConfigMap configMap) {
        if(configMap == null){
            return -1;
        }
        return Long.parseLong(configMap.getMetadata().getResourceVersion());
    }

    private void ensureActive() {
        if(!isActive.get()){
            throw new IllegalStateException("Cluster service is not active");
        }
    }

    private class ConfigMapWatcher implements Watcher<ConfigMap> {

        private final AtomicReference<ScheduledFuture<?>> nextUpdate = new AtomicReference<>();

        @Override
        public void eventReceived(Action action, ConfigMap configMap) {

            // Do not schedule updates for old or current versions. The former could corrupt state while
            // the latter just redundant.
            long latestVersion = getVersion(getLatestConfigMap());
            long updateVersion = getVersion(configMap);
            if(updateVersion <= latestVersion){
                LOG.trace("{}: Skipping configmap update for version {}", getName(), updateVersion);
                return;
            }
            synchronized (nextUpdate){
                if(nextUpdate.get() != null){
                    LOG.trace("{}: Cluster state update already scheduled", getName());
                    return;
                }
                long delay = configuration.stateUpdateBackoff() + jitter(configuration.stateUpdateJitter());
                nextUpdate.set(getExecutorService().schedule(() -> {
                    nextUpdate.set(null);
                    runClusterStateUpdate();
                }, configuration.stateUpdateBackoff() + jitter(configuration.stateUpdateJitter()), TimeUnit.MILLISECONDS));
                LOG.trace("{}: Cluster state update scheduled in {}ms", getName(), delay);
            }
        }

        @Override
        public void onClose(WatcherException cause) {
            LOG.warn("{}: ConfigMap watch closed", getName(), cause);
            closeConfigMapWatch();
        }
    }
}
