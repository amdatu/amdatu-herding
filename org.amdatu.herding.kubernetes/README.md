# Amdatu Herding Kubernetes

Amadatu Herding implementation that uses the Fabric8 Kubernetes client library to interact with Kubernetes as the
backend for cluster state management.

## Implementation

The `KubernetesClusterService` manages a single Kubernetes ConfigMap resource to maintain cluster membership state. 
Every node has a heartbeat to refresh membership and when any node changes the state all nodes get notified quickly 
through a watch.

The other service implementations use the `KubernetesClusterService` to maintain their state by calling it directly
and also may register as a processor that is called when the cluster state changes. Base classes provide a convenient
way for these services to extend the models with their own model class.

Concurrency in the `KubernetesClusterService` is controlled through a share single thread scheduled executor as well
as synchronization on the actual update processing of the ConfigMap. At the cluster level this implementation relies
on optimistic locking of the ConfigMap resource to ensure that only one node can update the state at a time.

## Configuration

See org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration for configuration options and defaults.


