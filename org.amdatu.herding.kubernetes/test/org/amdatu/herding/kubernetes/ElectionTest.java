/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.election.Leader;
import org.junit.jupiter.api.Test;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;

import java.util.Collections;

import static org.amdatu.herding.testsupport.config.ConfigUtils.createFactoryConfiguration;
import static org.junit.jupiter.api.Assertions.*;

public class ElectionTest extends KubernetesTestBase {

    @Test
    public void electionTest() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String groupId = "electionTest";
        String groupFilter = Leader.groupFilter(groupId);

        // No groups configured yet
        assertNotLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on a single node and expect that node to become leader
        configureLeaderElectionForGroup(frameworkOne, groupId);
        ServiceReference<Leader> leaderServiceReference = frameworkOne.waitForServiceReference(Leader.class, TEST_HEARTBEAT_INTERVAL, groupFilter).orElseThrow(AssertionError::new);
        assertEquals(groupId, leaderServiceReference.getProperty(Leader.GROUP_NAME));

        ElectionService electionService = frameworkOne.getService(ElectionService.class, groupFilter).orElseThrow(AssertionError::new);
        assertTrue(electionService.isLeader());
        assertEquals(Collections.singleton("fwOne"), electionService.getElectionCandidates());

        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on the second node, no changes in group leadership expected
        configureLeaderElectionForGroup(frameworkTwo, groupId);
        frameworkOne.waitForServiceReference(Leader.class, TEST_HEARTBEAT_INTERVAL, groupFilter).orElseThrow(AssertionError::new);
        assertTrue(electionService.isLeader());


        ElectionService fw2ElectionService = frameworkTwo.waitForService(ElectionService.class, TEST_HEARTBEAT_INTERVAL, groupFilter)
                .orElseThrow(AssertionError::new);

        assertNotLeader(frameworkTwo, groupFilter);
        assertFalse(fw2ElectionService.isLeader());

        // Stop leading node and expect the other node to become leader
        frameworkOne.stop();

        Thread.sleep(TEST_HEARTBEAT_INTERVAL);
        assertLeader(frameworkTwo, groupFilter);
        assertTrue(fw2ElectionService.isLeader());
    }

    @Test
    public void fw2() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String groupId = "test3";
        String groupFilter = Leader.groupFilter(groupId);

        // No groups configured yet
        assertNotLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on a single node and expect that node to become leader
        configureLeaderElectionForGroup(frameworkOne, groupId);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on the second node, no changes in group leadership expected
        configureLeaderElectionForGroup(frameworkTwo, groupId);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Re-configure the Kubernetes client. This should re-configure the component so leadership should be released
        // this should make the other node take leadership
        unregisterClient(frameworkOne);
        registerClient(frameworkOne);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter);
        assertLeader(frameworkTwo, groupFilter);

        // Re-configure the Kubernetes clien. this should re-configure the component so leadership should be released
        // this should make the other node take leadership
        unregisterClient(frameworkTwo);
        registerClient(frameworkTwo);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);
    }

    @Test
    public void fw3() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String groupId = "test4";
        String groupFilter = Leader.groupFilter(groupId);

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on a single node and expect that node to become leader
        Configuration factoryConfiguration = configureLeaderElectionForGroup(frameworkOne, groupId);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on the second node, no changes in group leadership expected
        configureLeaderElectionForGroup(frameworkTwo, groupId);
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Delete group configuration from leader node, expect other node to take over`
        factoryConfiguration.delete();
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter);
        assertLeader(frameworkTwo, groupFilter);
    }

    @Test
    public void multipleLeaderGroups() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String groupId1 = "multipleLeaderGroups-1";
        String groupFilter1 = Leader.groupFilter(groupId1);

        String groupId2 = "multipleLeaderGroups-2";
        String groupFilter2 = Leader.groupFilter(groupId2);

        // No groups configured yet
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter1);
        assertNotLeader(frameworkTwo, groupFilter1);

        // Configure one group on each node
        configureLeaderElectionForGroup(frameworkOne, groupId1);
        configureLeaderElectionForGroup(frameworkTwo, groupId2);

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter1);
        assertNotLeader(frameworkOne, groupFilter2);

        assertNotLeader(frameworkTwo, groupFilter1);
        assertLeader(frameworkTwo, groupFilter2);

        // Configure second group on each node, this should not cause any leadership changes
        configureLeaderElectionForGroup(frameworkOne, groupId2);
        configureLeaderElectionForGroup(frameworkTwo, groupId1);

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkOne, groupFilter1);
        assertNotLeader(frameworkOne, groupFilter2);

        assertNotLeader(frameworkTwo, groupFilter1);
        assertLeader(frameworkTwo, groupFilter2);

        // Stop one of the frameworks, the other framework should become leader for both groups
        frameworkOne.stop();

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertLeader(frameworkTwo, groupFilter1);
        assertLeader(frameworkTwo, groupFilter2);
    }

    @Test
    public void connectionLoss() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String groupId = "test9";
        String groupFilter = Leader.groupFilter(groupId);

        // No groups configured yet
        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

        // Configure group on a both nodes and expect both tho become leader as they are using a different namespace
        configureLeaderElectionForGroup(frameworkOne, groupId);
        configureLeaderElectionForGroup(frameworkTwo, groupId);

        assertOneNodeLeading(frameworkOne, frameworkTwo, groupFilter);

        unregisterClient(frameworkOne);
        unregisterClient(frameworkTwo);

//        We have no active connectivity monitoring (yet)
//        testingServer.stop();

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); //NOSONAR Some time to settle before validating something is not there
        assertNotLeader(frameworkOne, groupFilter);
        assertNotLeader(frameworkTwo, groupFilter);

//        Does not work with k8s. When we pull down the client the ElectionService will not be available.
//        Also the expected RuntimeException is herding zookeeper implementation specific..
//        try {
//            frameworkOne.waitForService(ElectionService.class, 1000)
//                    .map(ElectionService::isLeader)
//                    .orElseThrow(AssertionError::new);
//            fail("Expected exception");
//        } catch (RuntimeException e) {
//            // expected
//        }


        registerClient(frameworkOne);
        registerClient(frameworkTwo);
//        testingServer.restart(); // Use restart, start will silently do nothing on a stopped instance!!

        assertOneNodeLeading(frameworkOne, frameworkTwo, groupFilter);
    }

    private void assertOneNodeLeading(Launchpad frameworkOne, Launchpad frameworkTwo, String groupFilter) throws InterruptedException {
        Boolean node1Leading = false;
        Boolean node2Leading = false;
        long start = System.currentTimeMillis();
        while (!(node1Leading || node2Leading)) {
            if (start + 5000 < System.currentTimeMillis()) {
                fail("No leader 5 seconds after connection issues");
            }
            node1Leading = isLeader(frameworkOne, groupFilter);
            node2Leading = isLeader(frameworkTwo, groupFilter);
            Thread.sleep(100); //NOSONAR just a test
        }

        if(isLeader(frameworkOne, groupFilter)) {
            assertFalse(isLeader(frameworkTwo, groupFilter));
        } else {
            assertTrue(isLeader(frameworkTwo, groupFilter));
        }
    }

    private Boolean isLeader(Launchpad launchpad, String groupFilter) {
        try {
            return launchpad.getService(ElectionService.class, groupFilter).map(ElectionService::isLeader).orElse(false);
        } catch (RuntimeException e) {
            // this can happen as this is used in a connectivity test, ignore try later
            return false;
        }
    }

    private void assertLeader(Launchpad launchpad, String groupFilter) {
        launchpad.getServices(Leader.class, groupFilter, 1, 5000, true);
    }

    private void assertNotLeader(Launchpad launchpad, String groupFilter) {
        launchpad.getService(Leader.class, groupFilter).ifPresent(unexpected -> fail("Didn't expect a leader at this point"));
    }

    private Configuration configureLeaderElectionForGroup(Launchpad launchpad, String value) {
        return createFactoryConfiguration(launchpad, ElectionGroup.PID, ElectionGroup.class, builder -> builder
                .set(ElectionGroup::name).value(value));
    }
}
