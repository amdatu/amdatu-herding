/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.taskmanager.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.osgi.framework.ServiceRegistration;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.*;

public class WorkerTest extends KubernetesTestBase {

//    @Test
//    public void helloWorkerTest(TestInfo testInfo) throws Exception {
//        Launchpad frameworkOne = startFramework("fwOne");
//
//        frameworkOne.register(WorkerContext.class,  new TestWorkerContext(testInfo));
//        Worker worker = frameworkOne.waitForService(Worker.class, 2000).orElseThrow(AssertionError::new);
//
//        WorkPackage workPackage1 = WorkPackage.builder()
//                .name("test-workPackage1")
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .build();
//        WorkPackage workPackage2 = WorkPackage.builder()
//                .name("test-workPackage2")
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .build();
//
////            worker.start(workPackage1);
//        worker.start(workPackage1, workPackage2);
//        worker.awaitCompletion(workPackage1.getId(), 50000);
//        worker.awaitCompletion(workPackage2.getId(), 50000);
//    }

//    @Test
//    public void helloWorkerTest2(TestInfo testInfo) throws Exception {
//
//        Launchpad frameworkOne = startFramework("fwOne");
//        Launchpad frameworkTwo = startFramework("fwTwo");
//
//        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo);
//        TestWorkerContext fwTwoWorkerContext = new TestWorkerContext(testInfo);
//
//        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);
//        Thread.sleep(20);
//        frameworkTwo.register(WorkerContext.class, fwTwoWorkerContext);
//        Thread.sleep(20);
//
//        Worker worker = frameworkOne.waitForService(Worker.class, 2000).orElseThrow(AssertionError::new);
//
//        WorkPackage workPackage1 = WorkPackage.builder()
//                .name("test-workPackage1")
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .build();
//        WorkPackage workPackage2 = WorkPackage.builder()
//                .name("test-workPackage2")
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .addTaskGroup(createTaskGroup("osgi-command", 10))
//                .build();
//
////            worker.start(workPackage1);
//        worker.start(workPackage1, workPackage2);
//        worker.awaitCompletion(workPackage1.getId(), 10000);
//        worker.awaitCompletion(workPackage2.getId(), 10000);
//
//        System.out.println(fwOneWorkerContext.executionHistory.size());
//        System.out.println(fwTwoWorkerContext.executionHistory.size());
//
//        assertEquals(40, fwOneWorkerContext.executionHistory.size() + fwTwoWorkerContext.executionHistory.size());
//
//        assertFalse(fwOneWorkerContext.executionHistory.isEmpty(), "Ensure fwOne did part of the work");
//        assertFalse(fwTwoWorkerContext.executionHistory.isEmpty(), "Ensure fwTwo did part of the work");
//
//        assertTrue(fwOneWorkerContext.executionHistory.stream()
//                .map(Task::getCommand)
//                .allMatch("osgi-command"::equals));
//
//        assertTrue(fwTwoWorkerContext.executionHistory.stream()
//                .map(Task::getCommand)
//                .allMatch("osgi-command"::equals));
//    }

    @Test
    public void multiNodeWorkerContextAvailableOnAllNodes(TestInfo testInfo) throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo);
        TestWorkerContext fwTwoWorkerContext = new TestWorkerContext(testInfo);

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);
        frameworkTwo.register(WorkerContext.class, fwTwoWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 10))
                .build();
        worker.start(workPackage);

        Status status = worker.awaitCompletion(workPackage.getId(), 20000);
        assertEquals(Status.COMPLETED, status);

        assertEquals(10, fwOneWorkerContext.executionHistory.size() + fwTwoWorkerContext.executionHistory.size());

        assertFalse(fwOneWorkerContext.executionHistory.isEmpty(), "Ensure fwOne did part of the work");
        assertFalse(fwTwoWorkerContext.executionHistory.isEmpty(), "Ensure fwTwo did part of the work");

        assertTrue(fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("osgi-command"::equals));

        assertTrue(fwTwoWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("osgi-command"::equals));
    }


    @Test
    public void multiNodeWorkerContextAvailableOnSingleNode(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo);
        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);
        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 5))
                .build();
        worker.start(workPackage);

        worker.awaitCompletion(workPackage.getId(), 40000);

        assertEquals(5, fwOneWorkerContext.executionHistory.size());

        assertTrue(fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("osgi-command"::equals));
    }

    @Test
    public void multiNodeWorkerWithMultipleWorkPackages(TestInfo testInfo) throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo);

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackageOne = WorkPackage.builder()
                .name("test-workPackage-one")
                .addTaskGroup(createTaskGroup("osgi-command-one", 5))
                .build();
        WorkPackage workPackageTwo = WorkPackage.builder()
                .name("test-workPackage-two")
                .addTaskGroup(createTaskGroup("osgi-command-two", 5))
                .build();
        worker.start(workPackageOne, workPackageTwo);

        worker.awaitCompletion(workPackageOne.getId(), 40000);
        worker.awaitCompletion(workPackageTwo.getId(), 40000);

        assertEquals(10, fwOneWorkerContext.executionHistory.size());

        assertEquals(5, fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .filter("osgi-command-one"::equals)
                .count());
        assertEquals(5, fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .filter("osgi-command-two"::equals)
                .count());
    }

    @Test
    public void noWorkPackages(TestInfo testInfo) throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo);

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        worker.start();

        assertTrue(worker.listRunning().isEmpty());
        assertTrue(fwOneWorkerContext.executionHistory.isEmpty());
    }

    @Test
    public void multiNodeWorkerJoinAfterStarting(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo, (ctx, task) -> ctx.executionHistory.size() == 2); // Pause when running the second task to allow starting a the second node
        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);

        Worker worker = frameworkOne.waitForService(Worker.class, 2000).orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 10))
                .build();
        worker.start(workPackage);

        TestWorkerContext fwTwoWorkerContext = new TestWorkerContext(testInfo);
        frameworkTwo.register(WorkerContext.class, fwTwoWorkerContext);
        frameworkTwo.waitForService(Worker.class, TEST_HEARTBEAT_INTERVAL).orElseThrow(AssertionError::new);

        fwOneWorkerContext.resumeLatch.countDown();

        worker.awaitCompletion(workPackage.getId(), TEST_HEARTBEAT_INTERVAL * 10);

        assertEquals(10, fwOneWorkerContext.executionHistory.size() + fwTwoWorkerContext.executionHistory.size());

        assertFalse(fwOneWorkerContext.executionHistory.isEmpty(), "Ensure fwOne did part of the work");
        assertFalse(fwTwoWorkerContext.executionHistory.isEmpty(), "Ensure fwTwo did part of the work");

        assertTrue(fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("osgi-command"::equals));

        assertTrue(fwTwoWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("osgi-command"::equals));
    }

    @Test
    public void executionOrder(TestInfo testInfo) throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        BiFunction<TestWorkerContext, Task, Boolean> pauseCondition = (ctx, task) -> !ctx.executionHistory.isEmpty() && task.getCommand().equals("second-command");
        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo, pauseCondition);
        TestWorkerContext fwTwoWorkerContext = new TestWorkerContext(testInfo, pauseCondition);

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);
        frameworkTwo.register(WorkerContext.class, fwTwoWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("first-command", 10))
                .addTaskGroup(createTaskGroup("second-command", 10))
                .build();

        worker.start(workPackage);

        assertTrue(fwOneWorkerContext.pauseLatch.await(20, TimeUnit.SECONDS));
        assertTrue(fwTwoWorkerContext.pauseLatch.await(20, TimeUnit.SECONDS));

        assertEquals(10, fwOneWorkerContext.executionHistory.size() + fwTwoWorkerContext.executionHistory.size());

        assertFalse(fwOneWorkerContext.executionHistory.isEmpty(), "Ensure fwOne did part of the work");
        assertFalse(fwTwoWorkerContext.executionHistory.isEmpty(), "Ensure fwTwo did part of the work");

        assertTrue(fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("first-command"::equals));

        assertTrue(fwTwoWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("first-command"::equals));


        // Resume
        fwOneWorkerContext.executionHistory.clear();
        fwTwoWorkerContext.executionHistory.clear();
        fwOneWorkerContext.resumeLatch.countDown();
        fwTwoWorkerContext.resumeLatch.countDown();


        worker.awaitCompletion(workPackage.getId(), 20000);

        assertEquals(10, fwOneWorkerContext.executionHistory.size() + fwTwoWorkerContext.executionHistory.size());

        assertFalse(fwOneWorkerContext.executionHistory.isEmpty(), "Ensure fwOne did part of the work");
        assertFalse(fwTwoWorkerContext.executionHistory.isEmpty(), "Ensure fwTwo did part of the work");

        assertTrue(fwOneWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("second-command"::equals));

        assertTrue(fwTwoWorkerContext.executionHistory.stream()
                .map(Task::getCommand)
                .allMatch("second-command"::equals));
    }

    @Test
    public void failingTask(TestInfo testInfo) throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo) {
            @Override
            public TaskRunnable createTaskRunnable(Task task) {
                return new TaskRunnable() {
                    @Override
                    public void run() {
                        throw new RuntimeException("Intentional exception");
                    }

                    @Override
                    public void cancel() {

                    }
                };
            }
        };

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 10))
                .build();
        worker.start(workPackage);

        assertEquals(Status.FAILED, worker.awaitCompletion(workPackage.getId(), 10000));
    }

    @Test
    public void failingTestTaskShouldNotBeExecuted(TestInfo testInfo) throws Exception {

        Launchpad frameworkTwo = startFramework("fwTwo");
        Launchpad frameworkOne = startFramework("fwOne");

        List<String> executedTasks = Collections.synchronizedList(new ArrayList<>());

        TestWorkerContext fwOneWorkerContext = createFailingRunnable(testInfo, "second_", executedTasks);
        TestWorkerContext fwTwoWorkerContext = createFailingRunnable(testInfo, "second_", executedTasks);

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);
        frameworkTwo.register(WorkerContext.class, fwTwoWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 2,"first_"))
                .addTaskGroup(createTaskGroup("osgi-command", 3,"second_"))
                .addTaskGroup(createTaskGroup("osgi-command", 2,"third_"))
                .build();
        worker.start(workPackage);

        Status status = worker.awaitCompletion(workPackage.getId(), 10000);
        assertEquals(Status.FAILED, status);

        assertTrue(executedTasks.size() == 2, "Expected one task to be executed, actual is " + String.join(",", executedTasks));
        assertTrue(executedTasks.contains("first_0"));
        assertTrue(executedTasks.contains("first_1"));
    }

    private TestWorkerContext createFailingRunnable(TestInfo testInfo, String taskNamePrefix, List<String> executedTasks){
        return new TestWorkerContext(testInfo){
            @Override
            public TaskRunnable createTaskRunnable(Task task) {
                return new TaskRunnable() {
                    @Override
                    public void run() {
                        if(task.getName().startsWith(taskNamePrefix)) {
                            throw new RuntimeException(task.getName() + " failed");
                        }
                        executedTasks.add(task.getName());
                    }

                    @Override
                    public void cancel() {

                    }
                };
            }
        };
    }

    @Test
    public void timeoutTask(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext fwOneWorkerContext = new TestWorkerContext(testInfo) {
            private List<String> taskCreated = new ArrayList<>();
            @Override
            public long timeoutPerTask() {
                return 50;
            }

            @Override
            public TaskRunnable createTaskRunnable(Task task) {
                taskCreated.add(task.getName());
                return new TaskRunnable() {
                    private boolean cancelled = false;
                    @Override
                    public void run() {
                        if(taskCreated.size() > 1) {
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < 2000000000; ++i) {
                                if (cancelled) {
                                    break;
                                }
                                sb.append(i);
                            }
                        }
                    }

                    @Override
                    public void cancel() {
                        cancelled = true;
                    }
                };
            }
        };

        frameworkOne.register(WorkerContext.class, fwOneWorkerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command1", 3,"first_"))
                .build();
        worker.start(workPackage);

        // This test has a single framework which means we rely on the heartbeat to progress
        // the work. So we wait a bit..
        assertEquals(Status.FAILED, worker.awaitCompletion(workPackage.getId(), TEST_HEARTBEAT_INTERVAL * 10));

        //Check if the tasks are completed and timed_out and that the overall workpackage is than failed
        Map<String, String> states = worker.listStates();
        assertEquals(5, states.size());
        assertTrue(states.values().contains("FAILED"));
        assertTrue(states.values().contains("COMPLETED"));
        // FIXME Disabled check for helix specific state. The whole listStates should be removed?
        // assertTrue(states.values().contains("TIMED_OUT"));
    }

    @Test
    public void listRunning(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext workerContext = new TestWorkerContext(testInfo, (ctx, task) -> true);

        frameworkOne.register(WorkerContext.class, workerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, TEST_HEARTBEAT_INTERVAL);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 1))
                .build();

        worker.start(workPackage);
        Map<String, String> running = worker.listRunning();
        assertEquals(1, running.size());
        assertTrue(running.containsKey(workPackage.getId()));
        assertTrue(running.containsValue(workPackage.getName()));

        workerContext.pauseLatch.await(5, TimeUnit.SECONDS);

        Status status = worker.getStatus(workPackage.getId());
        assertEquals(Status.IN_PROGRESS, status);

        running = worker.listRunning();
        assertEquals(1, running.size());
        assertTrue(running.containsKey(workPackage.getId()));
        assertTrue(running.containsValue(workPackage.getName()));

        workerContext.resumeLatch.countDown();

        worker.awaitCompletion(workPackage.getId(), TEST_HEARTBEAT_INTERVAL * 4);

        assertTrue(worker.listRunning().isEmpty());
    }

    @Test
    public void reUseWorkPackageId(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext workerContext = new TestWorkerContext(testInfo, (ctx, task) -> true);

        frameworkOne.register(WorkerContext.class, workerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 1))
                .build();

        worker.start(workPackage);

        try {
            worker.start(workPackage);
            fail("Expected exception");
        } catch (RuntimeException e) {
            // expected
        }

        workerContext.resumeLatch.countDown();

        worker.awaitCompletion(workPackage.getId(), 10000);

        try {
            worker.start(workPackage);
            fail("Expected exception");
        } catch (RuntimeException e) {
            // expected
        }
    }


    @Test
    public void cancel(TestInfo testInfo) throws Exception {

        Launchpad frameworkOne = startFramework("fwOne");

        TestWorkerContext workerContext = new TestWorkerContext(testInfo, (ctx, task) -> true);

        frameworkOne.register(WorkerContext.class, workerContext);

        Optional<Worker> service = frameworkOne.waitForService(Worker.class, 2000);
        Worker worker = service.orElseThrow(AssertionError::new);

        WorkPackage workPackage = WorkPackage.builder()
                .name("test-workPackage")
                .addTaskGroup(createTaskGroup("osgi-command", 2))
                .build();

        worker.start(workPackage);

        // Once the first task hits the pause latch we can assume things are in progress.
        workerContext.pauseLatch.await(5, TimeUnit.SECONDS);

        Status status = worker.getStatus(workPackage.getId());
        assertEquals(Status.IN_PROGRESS, status);

        worker.cancel(workPackage.getId());

        status = worker.awaitCompletion(workPackage.getId(), TEST_HEARTBEAT_INTERVAL * 2);

        Thread.sleep(TEST_HEARTBEAT_INTERVAL);
        assertEquals(1, workerContext.executionHistory.size());
        assertEquals(1, workerContext.cancellationHistory.size());
        assertEquals(Status.FAILED, status);
    }

    private TaskGroup createTaskGroup(String command, int noTasks) {
        TaskGroup.Builder taskGroupBuilder = TaskGroup.builder();
        for (int i = 0; i < noTasks; i++) {
            taskGroupBuilder.addTask(Task.builder()
                    .command(command)
                    .name("Task " + i)
                    .build());
        }
        return taskGroupBuilder.build();
    }

    private TaskGroup createTaskGroup(String command, int noTasks, String taskPrefix) {
        TaskGroup.Builder taskGroupBuilder = TaskGroup.builder();
        for (int i = 0; i < noTasks; i++) {
            taskGroupBuilder.addTask(Task.builder()
                    .command(command)
                    .name(taskPrefix + i)
                    .build());
        }
        return taskGroupBuilder.build();
    }

    private static class TestWorkerContext implements WorkerContext {

        final TestInfo testInfo;
        private final BiFunction<TestWorkerContext, Task, Boolean> pauseCondition;


        private final CountDownLatch pauseLatch = new CountDownLatch(1);
        private final CountDownLatch resumeLatch = new CountDownLatch(1);

        final List<Task> executionHistory = Collections.synchronizedList(new ArrayList<>());
        final List<Task> cancellationHistory = Collections.synchronizedList(new ArrayList<>());

        private TestWorkerContext(TestInfo testInfo) {
            this(testInfo, (ctx, task) -> false);
        }

        private TestWorkerContext(TestInfo testInfo, BiFunction<TestWorkerContext, Task, Boolean> pauseCondition) {
            this.testInfo = testInfo;
            this.pauseCondition = pauseCondition;
        }

        @Override
        public String name() {
            return testInfo.getTestMethod().orElseThrow(IllegalStateException::new).getName();
        }

        @Override
        public int concurrentTasksPerNode() {
            return 1;
        }

        @Override
        public TaskRunnable createTaskRunnable(Task task) {

            return new TaskRunnable() {
                @Override
                public void run() {
                    if (pauseCondition.apply(TestWorkerContext.this, task)) {
                        pauseLatch.countDown();
                        try {
                            if (!resumeLatch.await(5, TimeUnit.SECONDS)) {
                                throw new AssertionError("Paused but not resumed within 5 seconds");
                            }
                        } catch (InterruptedException e) {
                            throw new AssertionError(e);
                        }
                    }
                    System.out.println("Task execution history: " + task.getName());
                    executionHistory.add(task);
                }

                @Override
                public void cancel() {
                    System.out.println("Task cancel history: " + task.getName());
                    cancellationHistory.add(task);
                    resumeLatch.countDown();
                }
            };
        }
    }
}