/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;

import static org.amdatu.herding.kubernetes.impl.Util.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

public class UtilTest {


    private static final String ALPHANUMERIC = "abcdefghijklmnopqrstuvwxyz0123456789";

    @Test
    public void testSafeName(){

        assertThrows(IllegalArgumentException.class, () -> safeResourceName(null));
        assertThrows(IllegalArgumentException.class, () -> safeResourceName(""));
        assertThrows(IllegalArgumentException.class, () -> safeResourceName(" "));

        String justRight = randomString(253);
        assertDoesNotThrow(() -> safeResourceName(justRight));

        String tooLong = randomString(254);
        assertThrows(IllegalArgumentException.class, () -> safeResourceName(tooLong));

        assertEquals("hello-world", safeResourceName("hello-world"));
        assertEquals("hello-world", safeResourceName("hello_world"));
        assertEquals("h-llo-w-r-d", safeResourceName("h=llo-w&r!d"));

        assertEquals("aello-world", safeResourceName("-ello-world"));
        assertEquals("hello-worlz", safeResourceName("hello-worl."));
    }

    private String randomString(int length){
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = ThreadLocalRandom.current().nextInt(ALPHANUMERIC.length());
            sb.append(ALPHANUMERIC.charAt(index));
        }
        return sb.toString();
    }
}
