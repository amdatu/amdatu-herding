/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;


import aQute.launchpad.Launchpad;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingService;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

public class LockingTest extends  KubernetesTestBase {

    @Test
    public void testLock() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String lockKey = "testLock";

        Lock node1Lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2Lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1Lock.lock(300));
        assertTrue(node1Lock.acquired());
        assertFalse(node2Lock.lock(0));
        assertFalse(node2Lock.acquired());

        assertEquals("fwOne", node1Lock.getLockOwner());
        assertEquals("fwOne", node2Lock.getLockOwner());

        node1Lock.release();
        assertFalse(node1Lock.acquired());
        assertFalse(node2Lock.acquired());

        assertNull(node1Lock.getLockOwner());
        assertNull(node2Lock.getLockOwner());

        assertTrue(node2Lock.lock(0));
        assertTrue(node2Lock.acquired());
        assertFalse(node2Lock.lock(0)); // Can't lock twice
        assertTrue(node2Lock.acquired());

        assertEquals("fwTwo", node1Lock.getLockOwner());
        assertEquals("fwTwo", node2Lock.getLockOwner());

        node2Lock.release();

        assertFalse(node1Lock.acquired());

        assertTrue(node1Lock.lock(0));
        node1Lock.release();
    }

    @Test
    public void testReentrantLock() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String lockKey = "testReentrantLock";

        Lock node1Lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2Lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1Lock.lock(0));
        assertFalse(node2Lock.lock(0));

        assertEquals("fwOne", node1Lock.getLockOwner());
        assertEquals("fwOne", node2Lock.getLockOwner());

        node1Lock.release();

        assertNull(node1Lock.getLockOwner());
        assertNull(node2Lock.getLockOwner());

        assertTrue(node2Lock.lock(0));
        assertTrue(node2Lock.lock(0)); // Can lock twice from the same thread

        assertEquals("fwTwo", node1Lock.getLockOwner());
        assertEquals("fwTwo", node2Lock.getLockOwner());

        node2Lock.release();
        assertFalse(node1Lock.lock(0));
        node2Lock.release();

        assertTrue(node1Lock.lock(0));
    }

    @Test
    public void obtainRentrantLockMultipleTimes_reentrant() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        String lockKey = "obtainLockMultipleTimes";
        Lock node1lock1 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node1lock2 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock1.lock(0));
        assertFalse(node1lock2.lock(0));
        assertTrue(node1lock1.lock(0));

        node1lock1.release();
        assertFalse(node1lock2.lock(0));
        node1lock1.release();
        assertTrue(node1lock2.lock(0));
        node1lock2.release();

        assertTrue(node2lock.lock(0));
        assertFalse(node1lock1.lock(0));
        node2lock.release();
    }
}
