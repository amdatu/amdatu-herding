/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.ClusterService;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ClusterServiceTest extends KubernetesTestBase {

    @Test
    public void clusterService() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework("fwTwo");

        ClusterService clusterService1 = frameworkOne.waitForService(ClusterService.class, 2000).orElseThrow(AssertionError::new);
        ClusterService clusterService2 = frameworkTwo.waitForService(ClusterService.class, 2000).orElseThrow(AssertionError::new);

        Thread.sleep(TEST_HEARTBEAT_INTERVAL);

        Collection<String> fwOneMembers = clusterService1.getMembers();
        Collection<String> fwTwoMembers = clusterService2.getMembers();

        assertEquals(2, fwOneMembers.size());
        assertEquals(2, fwTwoMembers.size());
    }

    @Test
    public void clusterServiceNotAvailableOnBadName() throws Exception {
        Launchpad frameworkOne = startFramework("fwOne");
        Launchpad frameworkTwo = startFramework(null);

        Collection<String> fwOneMembers = frameworkOne.waitForService(ClusterService.class, 1000).map(ClusterService::getMembers).get();
        assertEquals(1, fwOneMembers.size());

        Thread.sleep(TEST_HEARTBEAT_INTERVAL); // NOSONAR wait an additional half a second after the service became available on fw1 to make sure it's not unavailable due to timing
        assertFalse(frameworkTwo.getService(ClusterService.class).isPresent());
    }
}
