/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.kubernetes;

import aQute.launchpad.Launchpad;
import aQute.launchpad.LaunchpadBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.server.mock.KubernetesMixedDispatcher;
import io.fabric8.kubernetes.client.server.mock.KubernetesMockServer;
import io.fabric8.kubernetes.client.utils.Serialization;
import io.fabric8.mockwebserver.Context;
import io.fabric8.mockwebserver.ServerRequest;
import io.fabric8.mockwebserver.ServerResponse;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockWebServer;
import org.amdatu.herding.kubernetes.impl.metatype.KubernetesConfiguration;
import org.junit.jupiter.api.*;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import static org.amdatu.herding.testsupport.config.ConfigUtils.updateConfiguration;


// Annotation fails on superclass
// @EnableKubernetesMockClient(crud = true)
public class KubernetesTestBase {

    public static final Long TEST_HEARTBEAT_INTERVAL = 1000L; // Used for sleeps
    private static final Logger LOG = LoggerFactory.getLogger(KubernetesTestBase.class);

    protected Map<String, ServiceRegistration<KubernetesClient>> clientRegistrations = new HashMap<>();
    protected Map<String, Launchpad> launchpads = new HashMap<>();
    protected KubernetesMockServer server;

    @BeforeAll
    public static void beforeClass() throws Exception {
        LOG.info("Suite starting");
    }

    @AfterAll
    public static void afterClass() throws Exception {
        LOG.info("Suite finished");
    }

    @BeforeEach
    public void beforeEach() throws InterruptedException {
        LOG.info("Test starting");
        Map<ServerRequest, Queue<ServerResponse>> responses = new HashMap();
        Dispatcher dispatcher = new KubernetesMixedDispatcher(responses);
        server = new KubernetesMockServer(new Context(Serialization.jsonMapper()), new MockWebServer(), responses, dispatcher, true);
        server.init();
    }

    @AfterEach
    public void afterEach() throws Exception {
        launchpads.forEach((name, launchpad) -> {
            try {
                LOG.info("Closing launchpad {}", name);
                unregisterClient(launchpad);
                launchpad.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        launchpads.clear();
        server.shutdown();
        LOG.info("Test finished");
    }

    protected Launchpad startFramework(String nodeName) throws Exception {
        Launchpad launchpad = getLaunchpad(nodeName);
        configureNodeService(launchpad, nodeName, "default");
        registerClient(launchpad);
        launchpads.put(nodeName, launchpad);
        return launchpad;
    }

    protected void stopFramework(Launchpad framework) throws Exception {
        unregisterClient(framework);
        framework.close();
        launchpads.remove(framework.getName());
    }

    protected void configureNodeService(Launchpad launchpad, String nodeName, String namespace) {
        updateConfiguration(launchpad, KubernetesConfiguration.PID, KubernetesConfiguration.class, builder -> builder
                .set(KubernetesConfiguration::nodeName).value(nodeName)
                .set(KubernetesConfiguration::namespace).value(namespace)
                .set(KubernetesConfiguration::membershipHeartbeat).value(TEST_HEARTBEAT_INTERVAL));
    }

    protected static Launchpad getLaunchpad(String nodeName) {
        Launchpad launchpad = new LaunchpadBuilder().set("biz.aQute.runtime.snapshot", "test-method") // NOSONAR
                .bndrun("test-launchpad.bndrun")
                .excludeExport("org.slf4j*")
                .notestbundle()
                .create(nodeName);

        updateConfiguration(launchpad, "org.ops4j.pax.logging", config -> config
                .set("log4j.rootLogger").value("WARN, stdout")
                .set("log4j.appender.stdout").value("org.apache.log4j.ConsoleAppender")
                .set("log4j.appender.stdout.layout").value("org.apache.log4j.PatternLayout")
                .set("log4j.appender.stdout.layout.ConversionPattern").value(nodeName + ": %d{HH:mm:ss} %-5p %c{1}:%L - %m%n")
                .set("log4j.logger.org.amdatu").value("DEBUG")
                .set("log4j.logger.okhttp3").value("INFO")

        );
        return launchpad;
    }

    protected void registerClient(Launchpad framework) throws InterruptedException {
        ServiceRegistration<KubernetesClient> registration = clientRegistrations.get(framework.getName());
//        Assertions.assertNull(registration, "Client already registered for framework " + framework.getName());
        registration = framework.register(KubernetesClient.class, server.createClient());
        clientRegistrations.put(framework.getName(), registration);
        Thread.sleep(100); // let service dynamics settle
    }

    protected void unregisterClient(Launchpad framework) throws InterruptedException {
        ServiceRegistration<KubernetesClient> registration = clientRegistrations.remove(framework.getName());
//        Assertions.assertNotNull(registration, "Client not registered for framework " + framework.getName());
        if(registration != null){
            registration.unregister();
        }
        Thread.sleep(100); // let service dynamics settle
    }
}
