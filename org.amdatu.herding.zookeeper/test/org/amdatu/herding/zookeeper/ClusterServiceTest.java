/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.ClusterService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ClusterServiceTest extends ZookeeperTestBase {

    private Launchpad frameworkOne;
    private Launchpad frameworkTwo;

    @BeforeEach
    public void startNodes() {
        frameworkOne = getLaunchpad("fwoOne");
        frameworkTwo = getLaunchpad("fwTwo");
    }

    @AfterEach
    public void stopNodes() {
        frameworkOne.stop();
        frameworkTwo.stop();
    }

    @Test
    public void clusterService() throws InterruptedException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");

        ClusterService clusterService1 = frameworkOne.waitForService(ClusterService.class, 2000).orElseThrow(AssertionError::new);
        ClusterService clusterService2 = frameworkTwo.waitForService(ClusterService.class, 2000).orElseThrow(AssertionError::new);

        Thread.sleep(200); // NOSONAR Allow some time for the nodes to find each other

        Collection<String> fwOneMembers = clusterService1.getMembers();
        Collection<String> fwTwoMembers = clusterService2.getMembers();

        assertEquals(2, fwOneMembers.size());
        assertEquals(2, fwTwoMembers.size());
    }

    @Test
    public void clusterServiceNotAvailableOnBadName() throws InterruptedException {
        configureZookeeper(frameworkTwo, null);
        configureZookeeper(frameworkOne, "fwOne");

        Collection<String> fwOneMembers = frameworkOne.waitForService(ClusterService.class, 1000).map(ClusterService::getMembers).get();
        assertEquals(1, fwOneMembers.size());

        Thread.sleep(500); // NOSONAR wait an additional half a second after the service became available on fw1 to make sure it's not unavailable due to timing
        assertFalse(frameworkTwo.getService(ClusterService.class).isPresent());
    }

}
