/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;


import aQute.launchpad.Launchpad;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

public class LockingTest extends ZookeeperTestBase {

    private Launchpad frameworkOne;
    private Launchpad frameworkTwo;

    @BeforeEach
    public void startNodes() {
        frameworkOne = getLaunchpad("fwoOne");
        frameworkTwo = getLaunchpad("fwTwo");
    }

    @AfterEach
    public void stopNodes() {
        frameworkOne.stop();
        frameworkTwo.stop();
    }

    @Test
    public void testLock() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");
        String lockKey = "testLocking";

        Lock node1Lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2Lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1Lock.lock(0));
        assertTrue(node1Lock.acquired());
        assertFalse(node2Lock.lock(0));
        assertFalse(node2Lock.acquired());

        assertEquals("fwOne", node1Lock.getLockOwner());
        assertEquals("fwOne", node2Lock.getLockOwner());

        node1Lock.release();
        assertFalse(node1Lock.acquired());
        assertFalse(node2Lock.acquired());

        assertNull(node1Lock.getLockOwner());
        assertNull(node2Lock.getLockOwner());

        assertTrue(node2Lock.lock(0));
        assertTrue(node2Lock.acquired());
        assertFalse(node2Lock.lock(0)); // Can't lock twice
        assertTrue(node2Lock.acquired());

        assertEquals("fwTwo", node1Lock.getLockOwner());
        assertEquals("fwTwo", node2Lock.getLockOwner());

        node2Lock.release();

        assertFalse(node1Lock.acquired());

        assertTrue(node1Lock.lock(0));
        node1Lock.release();
    }

    @Test
    public void testReentrantLock() throws InterruptedException, ExecutionException, LockingException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");
        String lockKey = "testLocking";

        Lock node1Lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2Lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1Lock.lock(0));
        assertFalse(node2Lock.lock(0));

        assertEquals("fwOne", node1Lock.getLockOwner());
        assertEquals("fwOne", node2Lock.getLockOwner());

        node1Lock.release();

        assertNull(node1Lock.getLockOwner());
        assertNull(node2Lock.getLockOwner());

        assertTrue(node2Lock.lock(0));
        assertTrue(node2Lock.lock(0)); // Can lock twice from the same thread

        assertFalse(Executors.newSingleThreadScheduledExecutor().submit(() -> node2Lock.lock(0)).get());

        assertEquals("fwTwo", node1Lock.getLockOwner());
        assertEquals("fwTwo", node2Lock.getLockOwner());

        node2Lock.release();
        assertFalse(node1Lock.lock(0));
        node2Lock.release();

        assertTrue(node1Lock.lock(0));
    }

    @Test
    public void obtainLockMultipleTimes() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");

        String lockKey = "obtainLockMultipleTimes";
        Lock node1lock1 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));


        Lock node1lock2 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock1.lock(0));
        assertTrue(node1lock1.acquired());
        assertFalse(node1lock2.lock(0));
        assertFalse(node1lock2.acquired());
        assertFalse(node2lock.lock(0));
        assertFalse(node2lock.acquired());

        node1lock1.release();
        assertFalse(node1lock1.acquired());
        assertFalse(node1lock2.acquired());

        assertTrue(node1lock2.lock(0));
        assertTrue(node1lock2.acquired());
        node1lock2.release();
        assertFalse(node1lock2.acquired());

        assertTrue(node2lock.lock(0));
        assertTrue(node2lock.acquired());
        assertFalse(node1lock1.lock(0));
        node2lock.release();
    }

    @Test
    public void obtainRentrantLockMultipleTimes() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");

        String lockKey = "obtainLockMultipleTimes";
        Lock node1lock1 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));


        Lock node1lock2 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock1.lock(0));
        assertTrue(node1lock1.acquired());
        assertFalse(node1lock2.lock(0));
        assertFalse(node1lock2.acquired());
        assertFalse(node2lock.lock(0));
        assertFalse(node2lock.acquired());

        node1lock1.release();
        assertFalse(node1lock1.acquired());
        assertFalse(node1lock2.acquired());

        assertTrue(node1lock2.lock(0));
        assertTrue(node1lock2.acquired());
        node1lock2.release();
        assertFalse(node1lock2.acquired());

        assertTrue(node2lock.lock(0));
        assertTrue(node2lock.acquired());
        assertFalse(node1lock1.lock(0));
        node2lock.release();
    }

    @Test
    public void obtainRentrantLockMultipleTimes_reentrant() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne");
        configureZookeeper(frameworkTwo, "fwTwo");

        String lockKey = "obtainLockMultipleTimes";
        Lock node1lock1 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));


        Lock node1lock2 = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock1.lock(0));
        assertFalse(node1lock2.lock(0));
        assertTrue(node1lock1.lock(0));

        node1lock1.release();
        assertFalse(node1lock2.lock(0));
        node1lock1.release();
        assertTrue(node1lock2.lock(0));
        node1lock2.release();


        assertTrue(node2lock.lock(0));
        assertFalse(node1lock1.lock(0));
        node2lock.release();
    }

    @Test
    public void differentLockNamespaces() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne", "nsone");
        configureZookeeper(frameworkTwo, "fwTwo");

        String lockKey = "differentNamespaces";
        Lock node1lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock.lock(0));
        assertTrue(node2lock.lock(0));
    }

    @Test
    public void differentReentrantLockNamespaces() throws InterruptedException, LockingException {
        configureZookeeper(frameworkOne, "fwOne", "nsone");
        configureZookeeper(frameworkTwo, "fwTwo");

        String lockKey = "differentNamespaces";
        Lock node1lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        Lock node2lock = frameworkTwo.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(node1lock.lock(0));
        assertTrue(node2lock.lock(0));
    }



    @Test
    public void lockInvalidAfterConnectionLoss() throws Exception {
        configureZookeeper(frameworkOne, "fwOne");
        String lockKey = "lockInvalidAfterConnectionLoss";

        Lock lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(lock.lock(0));
        assertTrue(lock.valid());

        Lock finalLock = lock;
        new Thread(() -> {
            try {
                finalLock.lock(10000);
            } catch (InterruptedException | LockingException e) {
                e.printStackTrace();
            }
        });

        testingServer.restart();

        assertFalse(lock.valid());

        lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(lock.lock(1000));
        assertEquals("fwOne", lock.getLockOwner());
        assertTrue(lock.valid());
    }

    @Test
    public void reentrantLockInvalidAfterConnectionLoss() throws Exception {
        configureZookeeper(frameworkOne, "fwOne");
        String lockKey = "lockInvalidAfterConnectionLoss";

        Lock lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(lock.lock(0));
        assertTrue(lock.valid());

        testingServer.restart();

        assertFalse(lock.valid());

        lock = frameworkOne.waitForService(LockingService.class, 5000)
                .map(lockingService -> lockingService.getReentrantLock(lockKey))
                .orElseThrow(() -> new AssertionError("Locking service not available"));

        assertTrue(lock.lock(1000));
        assertEquals("fwOne", lock.getLockOwner() );
        assertTrue(lock.valid());
    }

}
