/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import aQute.launchpad.Launchpad;
import aQute.lib.exceptions.Exceptions;
import aQute.lib.io.IO;
import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.zookeeper.metatype.ZookeeperConfiguration;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.amdatu.herding.testsupport.config.ConfigUtils.updateConfiguration;
import static org.junit.jupiter.api.Assertions.*;

public class PartitionLeaderElectionTest extends ZookeeperTestBase {

    @Test
    public void singleNodeTwoPartitions(TestInfo testInfo) throws Exception {

        try (Launchpad frameworkOne = createNode("fwOne")) {
            frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));

            long count = frameworkOne.getServices(PartitionLeader.class, null, 2, 5000, false).size();
            assertEquals(2, count);

            Optional<PartitionElectionService> partitionElectionServiceOpt = frameworkOne.waitForService(PartitionElectionService.class, 2000, PartitionLeader.groupFilter(electionGroupName(testInfo)));
            assertTrue(partitionElectionServiceOpt.isPresent());

            Thread.sleep(1000); //TODO:  FIXME there seems to be an issue in init

            PartitionElectionService partitionElectionService = partitionElectionServiceOpt.get();
            assertEquals("fwOne", partitionElectionService.getLeader(0));
            assertEquals("fwOne", partitionElectionService.getLeader(1));
            assertThrows(IllegalArgumentException.class, () -> partitionElectionService.getLeader(2));

            assertTrue(partitionElectionService.isLeader(0));
            assertTrue(partitionElectionService.isLeader(1));
            assertThrows(IllegalArgumentException.class, () -> partitionElectionService.isLeader(2));
        }
    }

    @Test
    public void partitionElectionService(TestInfo testInfo) throws Exception {

        try (Launchpad frameworkOne = createNode("fwOne");
             Launchpad frameworkTwo = createNode("fwTwo")) {
            frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));
            frameworkTwo.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));

            frameworkOne.waitForService(PartitionLeader.class, 1000);
            frameworkTwo.waitForService(PartitionLeader.class, 1000);

            assertEquals(1, (long) frameworkOne.getServices(PartitionLeader.class).size());
            assertEquals(1, (long) frameworkTwo.getServices(PartitionLeader.class).size());

            PartitionElectionService partitionedElectionServiceOne = frameworkOne.getService(PartitionElectionService.class).get();
            PartitionElectionService partitionedElectionServiceTwo = frameworkTwo.getService(PartitionElectionService.class).get();

            Collection<String> electionCandidates = partitionedElectionServiceOne.getElectionCandidates();
            assertEquals(2, electionCandidates.size());


            int fwOnePartition;
            int fwTwoPartition;
            if (partitionedElectionServiceOne.isLeader(0)) {
                fwOnePartition = 0;
                fwTwoPartition = 1;
            } else {
                fwOnePartition = 1;
                fwTwoPartition = 0;
            }

            Thread.sleep(500); //NOSONAR: The methods below depend information that needs to be synchronized across nodes, allow some time for that sync.

            assertEquals("fwOne", partitionedElectionServiceOne.getLeader(fwOnePartition));
            assertEquals("fwTwo", partitionedElectionServiceOne.getLeader(fwTwoPartition));

            assertEquals("fwOne", partitionedElectionServiceTwo.getLeader(fwOnePartition));
            assertEquals("fwTwo", partitionedElectionServiceTwo.getLeader(fwTwoPartition));

            assertTrue(partitionedElectionServiceOne.isLeader(fwOnePartition));
            assertFalse(partitionedElectionServiceOne.isLeader(fwTwoPartition));

            assertFalse(partitionedElectionServiceTwo.isLeader(fwOnePartition));
            assertTrue(partitionedElectionServiceTwo.isLeader(fwTwoPartition));

        }
    }

    @Test
    public void partitionAssignment(TestInfo testInfo) throws Exception {

        try (Launchpad frameworkOne = createNode("fwOne");
             Launchpad frameworkTwo = createNode("fwTwo");
             Launchpad frameworkThree = createNode("fwThree")) {

            ServiceRegistration<PartitionElectionGroup> fw1GroupReg = frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 6));

            List<Integer> collect = frameworkOne.getServices(PartitionLeader.class, null, 6, 5000L, true).stream()
                    .map(ref -> (int) ref.getProperty(PartitionElectionConstants.PARTITION))
                    .collect(Collectors.toList());

            for (int i = 0; i < 6; i++) {
                assertTrue(collect.contains(i), "Expect partition leader for partition " + i);
            }

            assertEquals(6, (long) frameworkOne.getServices(PartitionLeader.class).size());

            frameworkTwo.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 6));

            frameworkTwo.getServices(PartitionLeader.class, null, 3, 5000L, true);

            assertEquals(3, (long) frameworkOne.getServices(PartitionLeader.class).size());
            assertEquals(3, (long) frameworkTwo.getServices(PartitionLeader.class).size());

            frameworkThree.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 6));
            frameworkThree.getServices(PartitionLeader.class, null, 2, 5000L, true);

            assertEquals(2, (long) frameworkOne.getServices(PartitionLeader.class).size());
            assertEquals(2, (long) frameworkTwo.getServices(PartitionLeader.class).size());
            assertEquals(2, (long) frameworkThree.getServices(PartitionLeader.class).size());

            fw1GroupReg.unregister();

            frameworkTwo.getServices(PartitionLeader.class, null, 3, 5000L, true);
            frameworkThree.getServices(PartitionLeader.class, null, 3, 5000L, true);

            assertEquals(3, (long) frameworkTwo.getServices(PartitionLeader.class).size());
            assertEquals(3, (long) frameworkThree.getServices(PartitionLeader.class).size());
        }
    }

    @Test
    public void changingNumberOfPartitions(TestInfo testInfo) throws Exception {

        try (Launchpad frameworkOne = createNode("fwOne");
             Launchpad frameworkTwo = createNode("fwTwo");
             Launchpad frameworkThree = createNode("fwThree")) {

            ServiceRegistration<PartitionElectionGroup> fw1GroupReg = frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 6));

            List<Integer> collect = frameworkOne.getServices(PartitionLeader.class, null, 6, 1000L, true).stream()
                    .map(ref -> (int) ref.getProperty(PartitionElectionConstants.PARTITION))
                    .collect(Collectors.toList());

            for (int i = 0; i < 6; i++) {
                assertTrue(collect.contains(i), "Expect partition leader for partition " + i);
            }


            assertEquals(6, (long) frameworkOne.getServices(PartitionLeader.class).size());

            frameworkTwo.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 10));

            frameworkTwo.getServices(PartitionLeader.class, null, 3, 5000, true);

            assertEquals(3, (long) frameworkOne.getServices(PartitionLeader.class).size());
            assertEquals(3, (long) frameworkTwo.getServices(PartitionLeader.class).size());


            frameworkThree.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 10));
            frameworkThree.getServices(PartitionLeader.class, null, 2, 2000L, true);

            assertEquals(2, (long) frameworkOne.getServices(PartitionLeader.class).size());
            assertEquals(2, (long) frameworkTwo.getServices(PartitionLeader.class).size());
            assertEquals(2, (long) frameworkThree.getServices(PartitionLeader.class).size());

            fw1GroupReg.unregister();

            frameworkTwo.getServices(PartitionLeader.class, null, 5, 10000L, true);
            frameworkThree.getServices(PartitionLeader.class, null, 5, 1000L, true);


            frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));
            frameworkOne.waitForService(PartitionLeader.class, 5000).orElseThrow(AssertionError::new);

            frameworkTwo.close();
            frameworkThree.close();
            Thread.sleep(4000);
            assertEquals(2, frameworkOne.getServices(PartitionLeader.class).size());

        }
    }


    /**
     * Simple partitioned task that takes some time to stop when the PartitionLeader service becomes unavailable.
     */
    @Component
    public static class PartitionedTask {

        private static final Logger LOGGER = LoggerFactory.getLogger(PartitionedTask.class);

        @ServiceDependency
        private volatile PartitionLeader partitionLeader;

        @Start
        void start() {
            LOGGER.info("Start partitioned task");
        }

        @Stop
        void stop() throws InterruptedException {
            LOGGER.info("Stopping partitioned task");
            Thread.sleep(3000);
            LOGGER.info("Stopped partitioned task");
        }

    }

    @Test
    public void changingNumberOfPartitions2(TestInfo testInfo) throws Exception {
        try (Launchpad frameworkOne = createNode("fwOne");
             Launchpad frameworkTwo = createNode("fwTwo")) {

            // Install a bundle that contains a component with a dependency on the PartitionLeader service
            frameworkOne.bundle()
                    .addResource(PartitionedTask.class)
                    .instruction("-pluginpath", new File(IO.work, "../cnf/plugins/org.apache.felix.dependencymanager.annotation-4.2.1.jar").getAbsolutePath())
                    .instruction("-plugin", "org.apache.felix.dm.annotation.plugin.bnd.AnnotationPlugin; build-import-export-service=false; add-require-capability=true")
                    .header("Import-Package", "*")
                    .start();

            // Register the first PartitionElectionGroup and wait for it to become available before registering the second to make sure this one becomes the cluster master
            ServiceRegistration<PartitionElectionGroup> fw1GroupReg = frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 1));
            frameworkOne.waitForService(PartitionLeader.class, 1000).orElseThrow(AssertionError::new);

            // Now register the PartitionElectionGroup on the second node
            frameworkTwo.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 1));

            final PartitionLeader partitionLeader = frameworkOne.waitForService(PartitionLeader.class, 1000).orElseThrow(AssertionError::new);
            String partitionFilter = PartitionLeader.partitionFilter(partitionLeader.getElectionGroup(), partitionLeader.getPartition());


            // Unregister the service in a separate thread as its a synchronous process
            new Thread(fw1GroupReg::unregister).start();

            // There is a TestPartitionRunner active that takes 3 seconds before stopping validate that the partition it
            // is not assigned to another node before it's fully released.
            Thread.sleep(2000);

            frameworkTwo.getService(PartitionLeader.class, partitionFilter).ifPresent(s -> fail(""));

            assertTrue(frameworkTwo.waitForService(PartitionLeader.class, 5000, partitionFilter).isPresent());
            assertTrue(frameworkOne.getServices(PartitionLeader.class, partitionFilter).isEmpty());
        }
    }

    @Test
    public void changingNumberOfPartitions3(TestInfo testInfo) throws Exception {
        try (Launchpad frameworkOne = createNode("fwOne");
             Launchpad frameworkTwo = createNode("fwTwo")) {

            // Install a bundle that contains a component with a dependency on the PartitionLeader service
            frameworkOne.bundle()
                    .addResource(PartitionedTask.class)
                    .instruction("-pluginpath", new File(IO.work, "../cnf/plugins/org.apache.felix.dependencymanager.annotation-4.2.1.jar").getAbsolutePath())
                    .instruction("-plugin", "org.apache.felix.dm.annotation.plugin.bnd.AnnotationPlugin; build-import-export-service=false; add-require-capability=true")
                    .header("Import-Package", "*")
                    .start();

            CountDownLatch partitionsChangedLatch = new CountDownLatch(1);
            frameworkTwo.register(Partitioner.class, new Partitioner() {
                        @Override
                        public String getTopic() {
                            return null;
                        }

                        @Override
                        public int getPartitionCount() {
                            return 0;
                        }

                        @Override
                        public int computePartitionForResource(String resourceName) {
                            return 0;
                        }

                        @Override
                        public void discardResource(String resourceName) {

                        }

                        @Override
                        public Map<Integer, List<String>> getPartitionAssignments() {
                            return null;
                        }

                        @Override
                        public void partitionsChanged() {
                            partitionsChangedLatch.countDown();
                        }
                    },
                    Constants.SERVICE_RANKING, Integer.MAX_VALUE,
                    PartitionElectionConstants.GROUP_NAME, electionGroupName(testInfo));

            // Register the first PartitionElectionGroup and wait for it to become available before registering the second to make sure this one becomes the cluster master
            ServiceRegistration<PartitionElectionGroup> fw1GroupReg = frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 1));
            frameworkOne.waitForService(PartitionLeader.class, 1000).orElseThrow(AssertionError::new);

            // Now register the PartitionElectionGroup on the second node
            frameworkTwo.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));

            final PartitionLeader partitionLeader = frameworkOne.waitForService(PartitionLeader.class, 1000).orElseThrow(AssertionError::new);
            String partitionFilter = PartitionLeader.partitionFilter(partitionLeader.getElectionGroup(), partitionLeader.getPartition());


            long unregisterTs = System.currentTimeMillis();
            // Unregister the service in a separate thread as its a synchronous process
            new Thread(fw1GroupReg::unregister).start();

            // There is a TestPartitionRunner active that takes 3 seconds before stopping validate that the partition it
            // is not assigned to another node before it's fully released.
            assertTrue(partitionsChangedLatch.await(6000, TimeUnit.MILLISECONDS));
            long partitionChangeDelay = System.currentTimeMillis() - unregisterTs;
            assertTrue(partitionChangeDelay > 3000, "Expected partition change delay to be more than 3000 ms was: " + partitionChangeDelay);
        }
    }

    @Test
    public void testConnectionFailure(TestInfo testInfo) throws Exception {
        String nodeName = "fwOne";

        try (Launchpad frameworkOne = getLaunchpad(nodeName)) {
            updateConfiguration(frameworkOne, ZookeeperConfiguration.PID, ZookeeperConfiguration.class, builder -> builder
                    .set(ZookeeperConfiguration::connectString).value(testingServer.getConnectString())
                    .set(ZookeeperConfiguration::sessionTimeoutMs).value(2000)
                    .set(ZookeeperConfiguration::connectionTimeoutMs).value(1000)
                    .set(ZookeeperConfiguration::nodeName).value(nodeName));

            frameworkOne.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 2));
            frameworkOne.waitForService(PartitionElectionService.class, 5000).orElseThrow(AssertionError::new);

            List<ServiceReference<PartitionLeader>> partitionLeaderServices;
            partitionLeaderServices = frameworkOne.getServices(PartitionLeader.class, null, 2, 5000, false);
            assertEquals(2, partitionLeaderServices.size());

            System.out.println("=== STOP");
            testingServer.stop();

            assertTimeoutPreemptively(Duration.ofSeconds(10), () -> {
                while (frameworkOne.getService(PartitionLeader.class).isPresent())
                    Thread.sleep(50);
            }, "Expect all PartitionLeader services to be unregistered");


            Thread.sleep(3000);
            System.out.println("=== RESTART");
            testingServer.restart();

            partitionLeaderServices = frameworkOne.getServices(PartitionLeader.class, null, 2, 5000, false);
            assertEquals(2, partitionLeaderServices.size());
        }
    }

    /**
     * The first real usage tests indicated that partitions sometimes (when the node name contains a "-" character)
     * never stabilized after a node rejoined.
     *
     * When this surfaced:
     * <ol>
     *  <li>Start 3 nodes for a PartitionElectionGroup (node-a, node-b, node-c)</li>
     *  <li>Kill two of them (by killing the JVM)</li>
     *  <li>Re-start one of the killed nodes</li>
     * </ol>
     *
     * This resulted in a never stabilizing cluster where partitions keep moving around.
     *
     */
    @Test
    public void changingNumberOfNodes(TestInfo testInfo) throws Exception {

        try (Launchpad nodeA = createNode("node-a")) {
            try (Launchpad nodeB = createNode("node-b");
                 Launchpad nodeC = createNode("node-c")) {

                nodeA.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 5));
                nodeB.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 5));
                nodeC.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 5));

                nodeA.getServices(PartitionLeader.class, null, 1, 5000, true);
                nodeB.getServices(PartitionLeader.class, null, 1, 5000, true);
                nodeC.getServices(PartitionLeader.class, null, 1, 5000, true);


                // Force an unclean shutdown
                Stream.of(nodeB, nodeC).forEach(fw -> {
                    try {
                        PartitionElectionService partitionElectionService = fw.getService(PartitionElectionService.class).orElseThrow(AssertionError::new);
                        Field helixManagerField = partitionElectionService.getClass().getDeclaredField("helixManager");
                        helixManagerField.setAccessible(true);
                        Object helixManager = helixManagerField.get(partitionElectionService);
                        helixManager.getClass().getDeclaredMethod("disconnect").invoke(helixManager);
                    } catch (Exception e) {
                        Exceptions.duck(e);
                    }
                });

            } // At this point by we'll stop 2 frameworks (node-b and node-c)

            nodeA.getServices(PartitionLeader.class, null, 5, 60000, true);


            try (Launchpad nodeC = createNode("node-c")) {
                // Start listening new PartitionLeader services registered on node-a
                AtomicInteger registered = new AtomicInteger(0);
                nodeA.getBundleContext().addServiceListener(event -> {
                    if (event.getType() == ServiceEvent.REGISTERED &&
                            event.getServiceReference().getProperty(PartitionElectionConstants.GROUP_NAME).equals(electionGroupName(testInfo))) {
                        registered.incrementAndGet();
                    }
                });


                nodeC.register(PartitionElectionGroup.class, newPartitionElectionGroup(testInfo, 5));
                nodeC.getServices(PartitionLeader.class, null, 2, 60000, true);

                Thread.sleep(2000);
                assertEquals(0, registered.get(),
                        "Expect that the partitions assignments are stable partitions should be removed from node-a but not re-added to that node");

            }
        }
    }

    private PartitionElectionGroup newPartitionElectionGroup(TestInfo testInfo, int partitions) {
        return new PartitionElectionGroup(electionGroupName(testInfo), partitions);
    }

    private String electionGroupName(TestInfo testInfo) {
        return testInfo.getTestMethod()
                .map(Method::getName)
                .orElseThrow(() -> new IllegalStateException("test method name unavailable"));
    }

    private Launchpad createNode(String nodeName) {
        Launchpad launchpad = getLaunchpad(nodeName);
        configureZookeeper(launchpad, nodeName);
        return launchpad;

    }

}
