/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.amdatu.herding.partitioner.Partitioner;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PartitionerTest extends ZookeeperTestBase {

    private static final String NO_PARTITIONER_SERVICE = "No partitioner service!";
    private static final int TIMEOUT = 5000;
    private static final String NO_PARTITION_LEADER = "No partition leader!";

    @Test
    public void testBasicPartitioning() throws Exception {
        PartitionElectionGroup partitionElectionGroup = new PartitionElectionGroup("testBasicPartitioning", 3);

        // Launchpad frameworkTwo = getLaunchpad("fwTwo")
        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            configureZookeeper(frameworkOne, "fwOne");
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));

            computePartition(frameworkOne, "a");
            computePartition(frameworkOne, "b");
            computePartition(frameworkOne, "c");

            Partitioner partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            assertEquals(3, partitionerOne.getPartitionCount());
            Map<Integer, List<String>> partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(3, partitionAssignments.size());

            try (Launchpad frameworkTwo = getLaunchpad("fwTwo")) {
                configureZookeeper(frameworkTwo, "fwTwo");
                registerPartitionElectionGroup(frameworkTwo, partitionElectionGroup);
                frameworkTwo.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));

                computePartition(frameworkTwo, "c");
                computePartition(frameworkTwo, "d");
                computePartition(frameworkTwo, "e");

                Partitioner partitionerTwo = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
                partitionAssignments = partitionerTwo.getPartitionAssignments();

                assertPartitionAssignment("a", 0, partitionAssignments);
                assertPartitionAssignment("b", 1, partitionAssignments);
                assertPartitionAssignment("c", 2, partitionAssignments);
                assertPartitionAssignment("d", 0, partitionAssignments);
                assertPartitionAssignment("e", 1, partitionAssignments);
            }

            partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            partitionAssignments = partitionerOne.getPartitionAssignments();

            assertEquals(3, partitionAssignments.size());
            assertPartitionAssignment("a", 0, partitionAssignments);
            assertPartitionAssignment("b", 1, partitionAssignments);
            assertPartitionAssignment("c", 2, partitionAssignments);

        }

        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            configureZookeeper(frameworkOne, "fwOne");
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));
            Partitioner partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            Map<Integer, List<String>> partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(0, partitionAssignments.size());

            computePartition(frameworkOne, "f");
            partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            partitionAssignments = partitionerOne.getPartitionAssignments();
            assertPartitionAssignment("f", 0, partitionAssignments);
        }
    }

    @Test
    @Disabled // enable when interested in timings
    public void testPartitioningPerformance() throws Exception {
        PartitionElectionGroup partitionElectionGroup = new PartitionElectionGroup("testPartitioningPerformance", 6);
        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            configureZookeeper(frameworkOne, "fwOne");
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));
            Partitioner partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            Map<Integer, List<String>> partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(0, partitionAssignments.size());

            long start = System.currentTimeMillis();
            for (int i = 0; i < 500; i++) {
                computePartition(frameworkOne, "resource x" + i);
            }
            long duration = System.currentTimeMillis() - start;
            System.out.println("Compute duration: " + duration);

            start = System.currentTimeMillis();
            for (int i = 0; i < 500; i++) {
                discardResource(frameworkOne, "resource x" + i);
            }
            computePartition(frameworkOne, "test");
            duration = System.currentTimeMillis() - start;
            System.out.println("Discard and compute duration: " + duration);
        }
    }

    @Test
    public void testPartitionsChanged() throws Exception {
        PartitionElectionGroup partitionElectionGroup = new PartitionElectionGroup("testPartitionsChanged", 8);

        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            configureZookeeper(frameworkOne, "fwOne");
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));

            computePartition(frameworkOne, "a");
            computePartition(frameworkOne, "b");
            computePartition(frameworkOne, "c");

            Partitioner partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            Map<Integer, List<String>> partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(3, partitionAssignments.size());

            partitionerOne.partitionsChanged();
            partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(0, partitionAssignments.size());
        }
    }

    @Test
    public void testPartitionDiscarded() throws Exception {
        PartitionElectionGroup partitionElectionGroup = new PartitionElectionGroup("testPartitionsChanged", 8);

        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            configureZookeeper(frameworkOne, "fwOne");
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));

            assertEquals(0, computePartition(frameworkOne, "a"));
            assertEquals(1, computePartition(frameworkOne, "b"));

            discardResource(frameworkOne, "a"); // should make partition 0 'available' again
            assertEquals(0, computePartition(frameworkOne, "c"));
            computePartition(frameworkOne, "a");
        }
    }

    private void assertPartitionAssignment(String resource, int partition, Map<Integer, List<String>> partitionAssignments) {
        List<String> resources = partitionAssignments.get(partition);
        assertTrue(resources.contains(resource));
    }

    private int computePartition(Launchpad framework, String resource) {
        return framework.waitForService(Partitioner.class, 1000)
                .map(partitioner -> partitioner.computePartitionForResource(resource))
                .orElseThrow(() -> new AssertionError("Partitioner service not available"));
    }

    private void discardResource(Launchpad framework, String resource) {
        Partitioner partitioner = framework.waitForService(Partitioner.class, 1000).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
        partitioner.discardResource(resource);
    }

    private void registerPartitionElectionGroup(Launchpad framework, PartitionElectionGroup electionGroup) {
        framework.register(PartitionElectionGroup.class, electionGroup);
    }

}
