/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import aQute.launchpad.Launchpad;
import aQute.launchpad.LaunchpadBuilder;
import org.amdatu.herding.zookeeper.metatype.ZookeeperConfiguration;
import org.apache.curator.test.InstanceSpec;
import org.apache.curator.test.TestingServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;

import static org.amdatu.herding.testsupport.config.ConfigUtils.updateConfiguration;


public class ZookeeperTestBase {

    protected static TestingServer testingServer;

    @BeforeAll
    public static void beforeClass() throws Exception {
        InstanceSpec instanceSpec = new InstanceSpec(null, -1, -1, -1, true, -1, 1000, -1);
        testingServer = new TestingServer(instanceSpec, true);

        System.out.println("CONNECT: " + testingServer.getConnectString());
    }

    @AfterAll
    public static void afterClass() throws IOException {
        testingServer.stop();
    }

    @BeforeEach
    public void beforeEach() {
        System.out.println("Test starting");
    }

    @AfterEach
    public void afterEach() {
        System.out.println("Test finished");
//        ThreadMXBean threadMxBean = ManagementFactory.getThreadMXBean();
//
//        for (ThreadInfo ti : threadMxBean.dumpAllThreads(true, true)) {
//            System.out.print(ti.toString());
//        }
    }


    protected void configureZookeeper(Launchpad launchpad, String nodeName) {
        configureZookeeper(launchpad, nodeName, "test");
    }

    protected void configureZookeeper(Launchpad launchpad, String nodeName, String namespace) {
        updateConfiguration(launchpad, ZookeeperConfiguration.PID, ZookeeperConfiguration.class, builder -> builder
                .set(ZookeeperConfiguration::connectString).value(testingServer.getConnectString())
                .set(ZookeeperConfiguration::namespace).value(namespace)
                .set(ZookeeperConfiguration::nodeName).value(nodeName));
    }

    protected Launchpad getLaunchpad(String nodeName) {
        Launchpad launchpad = new LaunchpadBuilder().set("biz.aQute.runtime.snapshot", "test-method") // NOSONAR
                .bndrun("election-test.bndrun")
                .excludeExport("org.slf4j*")
                .notestbundle()
                .create();

        updateConfiguration(launchpad, "org.ops4j.pax.logging", config -> config
                .set("log4j.rootLogger").value("WARN, stdout")
                .set("log4j.appender.stdout").value("org.apache.log4j.ConsoleAppender")
                .set("log4j.appender.stdout.layout").value("org.apache.log4j.PatternLayout")
                .set("log4j.appender.stdout.layout.ConversionPattern").value(nodeName + ": %d{HH:mm:ss} %-5p %c{1}:%L - %m%n")
                .set("log4j.logger.org.amdatu").value("INFO")
                .set("log4j.logger.org.apache.helix.manager.zk").value("INFO")

        );
        return launchpad;
    }

}
