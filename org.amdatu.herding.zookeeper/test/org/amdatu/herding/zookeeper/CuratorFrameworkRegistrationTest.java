/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.zookeeper.metatype.ZookeeperConfiguration;
import org.apache.curator.framework.CuratorFramework;
import org.junit.jupiter.api.Test;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.amdatu.herding.testsupport.config.ConfigUtils.updateConfiguration;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * This test validates the (absesnse) of a CuratorFramework service. This service is registered but type resides in a
 * Private-Package so needs special attention (can't be obtained with the default Launchpad  methods to get a service).
 */
public class CuratorFrameworkRegistrationTest extends ZookeeperTestBase {

    public static final int CURATOR_FRAMEWORK_REGISTRATION_TIMEOUT_MS = 5000;

    @Test
    public void curatorFrameworkRegistered() throws Exception {
        try (Launchpad launchpad = getLaunchpad("broken")) {
            CountDownLatch curatorFrameworkRegisteredLatch = new CountDownLatch(1);
            ServiceTracker<Object, Object> t = new ServiceTracker<Object, Object>(launchpad.getBundleContext(), CuratorFramework.class.getName(), null) {
                @Override
                public Object addingService(ServiceReference reference) {
                    curatorFrameworkRegisteredLatch.countDown();
                    return super.addingService(reference);
                }
            };
            t.open(true);

            updateConfiguration(launchpad, ZookeeperConfiguration.PID, ZookeeperConfiguration.class, builder -> builder
                    .set(ZookeeperConfiguration::connectString).value(testingServer.getConnectString())
                    .set(ZookeeperConfiguration::nodeName).value("broken"));


            assertTrue(curatorFrameworkRegisteredLatch.await(CURATOR_FRAMEWORK_REGISTRATION_TIMEOUT_MS, TimeUnit.MILLISECONDS));
        }
    }

    @Test
    public void curatorFrameworkNotRegisteredIfZookeeperConnectionFail() throws Exception {

        try (Launchpad launchpad = getLaunchpad("broken")){
            updateConfiguration(launchpad, ZookeeperConfiguration.PID, ZookeeperConfiguration.class, builder -> builder
                    .set(ZookeeperConfiguration::connectString).value("localhost:12345") // NOTE THAT THIS IS WRONG
                    .set(ZookeeperConfiguration::nodeName).value("broken"));


            ServiceTracker<?, ?> t = new ServiceTracker<>(launchpad.getBundleContext(), CuratorFramework.class.getName(), null);
            t.open(true);

            // Wait for half a second longer fhan we do for the tegistration test, if it's not there by then we can be pretty sure it'll never be there.
            Thread.sleep(CURATOR_FRAMEWORK_REGISTRATION_TIMEOUT_MS + 500); //NOSONAR It's just a test..

            assertNull(t.getServiceReferences());
        }
    }

}
