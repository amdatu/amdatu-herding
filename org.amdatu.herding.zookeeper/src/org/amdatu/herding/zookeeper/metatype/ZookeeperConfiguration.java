/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.metatype;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(pid = ZookeeperConfiguration.PID,
        description = "Configuration for the Zookeeper based Amdatu Herding services")
public @interface ZookeeperConfiguration {

    String PID = "org.amdatu.herding.zookeeper";

    @AttributeDefinition(description = "Name for this node in the cluster. MUST be an unique name within the cluster")
    String nodeName();

    @AttributeDefinition(description = "Zookeeper connect string. Comma separated host:port pairs, each corresponding to a zk server. e.g. \"127.0.0.1:3000,127.0.0.1:3001,127.0.0.1:3002\".")
    String connectString();

    @AttributeDefinition(description = "Zookeeper namespace, used to prefix Zookeeper paths when applications clusters share a Zoookeeper cluster", required = false)
    String namespace() default "";

    @AttributeDefinition(description = "Zookeeper connection timeout", required = false)
    int connectionTimeoutMs() default 15 * 1000;

    @AttributeDefinition(description = "Zookeeper session timeout", required = false)
    int sessionTimeoutMs() default 30 * 1000;

}
