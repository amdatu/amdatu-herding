/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.data.Stat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Node name lookup service that provides a method to retrieve a cluster node name based on a Zookeeper session id.
 *
 * This can be used to get a node name based on a node's ephemeral owner information.
 */
public class NodeNameLookup {

    private volatile CuratorFramework client;

    private final Map<Long, String> sessionIdToNodeName = new HashMap<>();

    public String getNodeNameForSessionId(long sessioniId) {

        String s = sessionIdToNodeName.get(sessioniId);
        if (s != null) {
            return s;
        }

        synchronized (sessionIdToNodeName) {
            try {
                List<String> clusterNodes = client.getChildren().forPath("/amdatu-herding/cluster");
                List<Long> activeSessionIds = new ArrayList<>();
                for (String clusterNode : clusterNodes) {
                    Stat nodeStat = client.checkExists().forPath("/amdatu-herding/cluster/" + clusterNode);
                    sessionIdToNodeName.put(nodeStat.getEphemeralOwner(), clusterNode);
                    activeSessionIds.add(nodeStat.getEphemeralOwner());
                }

                sessionIdToNodeName.keySet().retainAll(activeSessionIds);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return sessionIdToNodeName.get(sessioniId);
    }
}