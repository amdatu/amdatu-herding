/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import org.amdatu.herding.ClusterService;
import org.amdatu.herding.CoordinatorStatusService;
import org.amdatu.herding.NodeService;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.taskmanager.Worker;
import org.amdatu.herding.taskmanager.WorkerContext;
import org.amdatu.herding.zookeeper.election.ZookeeperElectionService;
import org.amdatu.herding.zookeeper.locking.ZookeeperLockingService;
import org.amdatu.herding.zookeeper.metatype.ZookeeperConfiguration;
import org.amdatu.herding.zookeeper.partitionelection.HelixPartitionElectionGroupAdapter;
import org.amdatu.herding.zookeeper.partitioner.ZookeeperPartitioner;
import org.amdatu.herding.zookeeper.taskmanager.HelixWorker;
import org.apache.curator.framework.CuratorFramework;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;


public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) {
        manager.add(createComponent()
                .setImplementation(CuratorFrameworkManager.class)
                .setAutoConfig(Component.class, false)
                .add(createConfigurationDependency()
                        .setPid(ZookeeperConfiguration.PID)
                        .setConfigType(ZookeeperConfiguration.class))
        );

        manager.add(createComponent()
                .setInterface(NodeService.class.getName(), null)
                .setImplementation(ZookeeperNodeService.class)
                .add(createConfigurationDependency()
                        .setPid(ZookeeperConfiguration.PID)
                        .setConfigType(ZookeeperConfiguration.class))
        );

        manager.add(createComponent()
                .setInterface(ClusterService.class.getName(), null)
                .setImplementation(ZookeeperClusterService.class)
                .add(createServiceDependency()
                        .setService(NodeService.class).setRequired(true))
                .add(createServiceDependency()
                        .setService(CuratorFramework.class).setRequired(true))
        );

        manager.add(createComponent()
                .setInterface(CoordinatorStatusService.class.getName(), null)
                .setImplementation(ZookeeperStatusService.class)
                .add(createServiceDependency()
                        .setService(CuratorFramework.class)
                        .setRequired(true)
                )
        );

        manager.add(createFactoryComponent()
                .setFactoryPid(ElectionGroup.PID)
                .setConfigType(ElectionGroup.class)
                .setInterface(ElectionService.class.getName(), null)
                .setImplementation(ZookeeperElectionService.class)
                .add(createServiceDependency()
                        .setService(NodeService.class)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(CuratorFramework.class)
                        .setRequired(true))
        );


        manager.add(createComponent().setInterface(LockingService.class.getName(), null)
                .setImplementation(ZookeeperLockingService.class)
                .add(createServiceDependency()
                        .setService(NodeService.class)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(CuratorFramework.class)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(NodeNameLookup.class)
                        .setRequired(true)
                )
        );

        manager.add(createAdapterComponent()
                .setAdaptee(PartitionElectionGroup.class, null)
                .setImplementation(HelixPartitionElectionGroupAdapter.class)
                .setAutoConfig(Component.class, false)
        );


        manager.add(createAdapterComponent()
                .setAdaptee(PartitionElectionGroup.class, null)
                .setInterface(Partitioner.class.getName(), null)
                .setImplementation(ZookeeperPartitioner.class)
                .add(createServiceDependency().setService(CuratorFramework.class).setRequired(true))
                .add(createServiceDependency().setService(LockingService.class).setRequired(true))
                .add(createServiceDependency().setService(NodeService.class).setRequired(true))
        );



        manager.add(createComponent()
                .setInterface(NodeNameLookup.class.getName(), null)
                .setImplementation(NodeNameLookup.class)
                .add(createServiceDependency()
                        .setService(CuratorFramework.class).setRequired(true))
        );

        manager.add(createAdapterComponent()
                .setAdaptee(WorkerContext.class, null)
                .setInterface(Worker.class, null)
                .setImplementation(HelixWorker.class)
                .add(createServiceDependency()
                        .setService(CuratorFramework.class)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(NodeService.class)
                        .setRequired(true))
                .add(createServiceDependency()
                        .setService(LockingService.class)
                        .setRequired(true))
        );
    }

}
