/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.partitioner;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.partitioner.PartitionerException;
import org.apache.curator.framework.CuratorFramework;
import org.apache.felix.dm.Component;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Supplier;

/**
 * Partitioner that uses Zookeeper for semi-persistent storage.
 *
 * Zookeeper node structure:
 * - topic
 *      - partitions
 *          - partition
 *              - node_resource (ephemeral, key is concatenation of nodeId and resourceName)
 */
public class ZookeeperPartitioner implements Partitioner {

    private List<Integer> partitions;

    private static final String PARTITIONER_ROOT = "/partitioner";
    private static final Logger LOGGER = LoggerFactory.getLogger(ZookeeperPartitioner.class);
    private volatile CuratorFramework curatorFramework;
    private volatile NodeService nodeService;
    private volatile LockingService lockingService;
    private volatile PartitionElectionGroup partitionElectionGroup;

    void start(Component component) {
        LOGGER.info("Starting ZookeeperPartitioner for election group {}", partitionElectionGroup.name() );
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(PartitionElectionConstants.GROUP_NAME, partitionElectionGroup.name());
        component.setServiceProperties(serviceProperties);

        ensureZkNode(PARTITIONER_ROOT);
        ensureZkNode(getTopicPath());

        initializePartitions();
    }

    private void initializePartitions() {
        ensureZkNode(getPartitionsPath());
        partitions = new ArrayList<>();
        for (int i = 0; i < partitionElectionGroup.partitions(); i++) {
            partitions.add(i);
            ensureZkNode(getPartitionPath(i));
        }
    }

    void stop() {
        LOGGER.info("Stopping ZookeeperPartitioner for election group {}", partitionElectionGroup.name() );
    }

    private String getTopicPath() {
        return PARTITIONER_ROOT + "/" + getTopic();
    }

    private String getPartitionsPath() {
        return getTopicPath() + "/partitions";
    }

    private String getPartitionPath(int partition) {
        return getPartitionsPath() + "/" + partition;
    }

    private String getResourcePath(int partition, String resource) {
        return getPartitionPath(partition) + "/" + getNodeResourceName(resource);
    }

    private String getNodeResourceName(String resourceName) {
        return nodeService.getName() + "%" + resourceName;
    }

    private String getResourceName(String nodeResourceName) {
        return nodeResourceName.substring(nodeResourceName.indexOf('%') + 1);
    }

    @Override
    public String getTopic() {
        return partitionElectionGroup.name();
    }

    @Override
    public int getPartitionCount() {
        return partitions.size();
    }

    private <T> T getWithPartitionerLock(String resourceName, Supplier<T> supplier) {
        Lock lock = lockingService.getLock(resourceName != null ? ZookeeperPartitioner.class.getName() + "_" + resourceName : ZookeeperPartitioner.class.getName());

        try {
            if (lock.lock(10000)) {
                try {
                    return supplier.get();
                } finally {
                    if (lock.valid()) { // Check
                        releaseUnchecked(lock);
                    }
                }
            } else {
                throw new PartitionerException("Could not obtain lock.");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new PartitionerException("Interrupted while trying to obtain lock.", e);
        } catch (LockingException e) {
            throw new PartitionerException("Exception while obtaining lock.", e);
        }
    }

    private void releaseUnchecked(Lock lock) {
        try {
            lock.release();
        } catch (LockingException e) {
            LOGGER.warn("Failed to release lock.", e);
        }
    }

    private void withPartitionerLock(Runnable runnable) {
        getWithPartitionerLock(null, () -> {
            runnable.run();
            return null;
        });
    }

    private void withPartitionerLock(String resourceName, Runnable runnable) {
        getWithPartitionerLock(resourceName, () -> {
           runnable.run();
           return null;
        });
    }

    @Override
    public int computePartitionForResource(String resourceName) {
        return getWithPartitionerLock(resourceName, () -> {
            // read current partition assignment from zookeeper, if not present, compute
            Map<String, Integer> resourceToPartitionAssignments = getCurrentResourcePartitionAssignments(resourceName);
            Integer partition = resourceToPartitionAssignments.get(resourceName);
            if (partition != null) {
                ensureEphemeralZkNode(getResourcePath(partition, resourceName));
                return partition;
            }

            Map<Integer, Integer> partitionResourceCount = countPartitionAssignments(resourceToPartitionAssignments);
            partition = computeBestPartition(partitionResourceCount);
            ensureEphemeralZkNode(getResourcePath(partition, resourceName));

            LOGGER.info("Computed partition {} for resource {}", partition, resourceName);
            return partition;
        });
    }

    @Override
    public void discardResource(String resourceName) {
        withPartitionerLock(resourceName, () -> {
            LOGGER.info("Discarding resource {}. ", resourceName);
            // find the partition for the resource
            Map<String, Integer> partitionAssignments = getCurrentResourcePartitionAssignments(resourceName);
            Integer partition = partitionAssignments.get(resourceName);
            if (partition != null) {
                String resourceNodePath = getResourcePath(partition, resourceName);
                try {
                    curatorFramework.delete().forPath(resourceNodePath);
                } catch (KeeperException.NoNodeException ne) {
                    // if it's not there it's ok, someone probably got here ahead of us
                    LOGGER.info("Node {} for resource to discard was not found.", resourceNodePath);
                }  catch (Exception e) {
                    throw new PartitionerException("Failed to delete resource node " + resourceNodePath, e);
                }
            } else {
                LOGGER.warn("Resource {} to discard was not assigned to any partition.", resourceName);
            }
        });
    }

    private Map<Integer, Integer> countPartitionAssignments(Map<String, Integer> resourceToPartitionAssignments) {
        Map<Integer, Integer> partitionCount = new HashMap<>();
        resourceToPartitionAssignments.forEach((key, value) -> partitionCount.compute(value, (k, v) -> v == null ? 1 : v + 1));
        // add all unassigned partitions to the map
        partitions.forEach(partition -> {
            if (!partitionCount.containsKey(partition)) {
                partitionCount.put(partition, 0);
            }
        });
        return partitionCount;
    }

    private Integer computeBestPartition(Map<Integer, Integer> partitionCount) {
        // choose the partition with the lowest value
        Integer lowestPartition = null;
        int lowestPartitionCount = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Integer> entry : partitionCount.entrySet()) {
            Integer partition = entry.getKey();
            int count = entry.getValue();
            if (count < lowestPartitionCount) {
                lowestPartition = partition;
                lowestPartitionCount = count;
            }
        }
        return lowestPartition;
    }

    private Map<String, Integer> getCurrentResourcePartitionAssignments() {
        return getCurrentResourcePartitionAssignments(null);
    }

    /**
     * Gets the current resource partition assignments. Optionally takes a resourceName argument, when provided
     * and a partition is found for the given resource only that assignment will be returned. When the partition
     * is not found all partition assignments for all resources will be returned.
     * @param resourceName
     * @return
     */
    private Map<String, Integer> getCurrentResourcePartitionAssignments(String resourceName) {
        Map<String, Integer> assignments = new HashMap<>();
        try {
            List<String> partitionNames = curatorFramework.getChildren().forPath(getPartitionsPath());
            for (String partitionName : partitionNames) {
                // check if there are any children
                Integer partition = Integer.parseInt(partitionName);
                List<String> resourceNodes = curatorFramework.getChildren().forPath(getPartitionPath(partition));
                for (String resourceNode : resourceNodes) {
                    String assignmentResourceName = getResourceName(resourceNode);
                    if (assignmentResourceName.equals(resourceName)) {
                        return Collections.singletonMap(assignmentResourceName, partition); // just return this assignment
                    }
                    assignments.put(assignmentResourceName, partition);
                }
            }
        } catch (Exception e) {
            throw new PartitionerException("Error reading zookeeper node for path: " + getPartitionsPath(), e);
        }
        return assignments;
    }

    private void ensureZkNode(String path) {
        ensureZkNode(path, false);
    }

    private void ensureEphemeralZkNode(String path) {
        createZkNode(path, true, null);
    }

    private void ensureZkNode(String path, boolean ephemeral) {
        createZkNode(path, ephemeral, null);
    }

    private void createZkNode(String path, boolean ephemeral, byte[] data) {
        try {
            if (ephemeral) {
                if (data == null) {
                    curatorFramework.create().orSetData().withMode(CreateMode.EPHEMERAL).forPath(path);
                } else {
                    curatorFramework.create().orSetData().withMode(CreateMode.EPHEMERAL).forPath(path, data);
                }
            } else {
                if (data == null) {
                    curatorFramework.create().orSetData().forPath(path);
                } else {
                    curatorFramework.create().orSetData().forPath(path, data);
                }
            }
        } catch (Exception e) {
            throw new PartitionerException("Error creating Zookeper node for path: " + path, e);
        }
    }

    private void deleteZkNodeRecursive(String path) {
        try {
            LOGGER.info("Deleting ZK node {}.", path);
            curatorFramework.delete().deletingChildrenIfNeeded().forPath(path);
        } catch (KeeperException.NoNodeException e) {
            LOGGER.warn("Could not delete ZK node {} as it does not exist.", path);
        } catch (Exception e) {
            throw new PartitionerException("Error deleting Zookeeper node for path: " + path, e);
        }
    }

    @Override
    public Map<Integer, List<String>> getPartitionAssignments() {
        Map<Integer, List<String>> partitionAssignments = new HashMap<>();
        getCurrentResourcePartitionAssignments()
                .forEach((key, value) -> partitionAssignments.compute(value, (k, v) -> createOrAddToAssignmentList(v, key)));
        return partitionAssignments;
    }

    /**
     * Resets the partition assignments. Note: the partitioner must not be used while this method is running.
     */
    @Override
    public void partitionsChanged() {
        LOGGER.info("Partitions changed. Flushing partition assignments.");
        withPartitionerLock(() -> {
            // flush the current administration
            deleteZkNodeRecursive(getPartitionsPath());
            initializePartitions();
        });
    }

    private List<String> createOrAddToAssignmentList(List<String> resources, String resource) {
        List<String> newResources = new ArrayList<>();
        if (resources != null) {
            newResources.addAll(resources);
        }
        newResources.add(resource);
        return newResources;
    }
}
