/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.partitionelection;

import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessSemaphoreMutex;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.helix.NotificationContext;
import org.apache.helix.model.Message;
import org.apache.helix.participant.statemachine.StateModel;
import org.apache.helix.participant.statemachine.StateModelFactory;
import org.apache.helix.participant.statemachine.StateModelInfo;
import org.apache.helix.participant.statemachine.Transition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class OnlineOfflineStateModelFactory extends StateModelFactory<StateModel> {

    private final DependencyManager dependencyManager;
    private final String nodeName;
    private final PartitionElectionGroup electionGroup;
    private final CuratorFramework curatorFramework;

    OnlineOfflineStateModelFactory(DependencyManager dependencyManager, String nodeName, PartitionElectionGroup electionGroup, CuratorFramework curatorFramework) {
        this.dependencyManager = dependencyManager;
        this.nodeName = nodeName;
        this.electionGroup = electionGroup;
        this.curatorFramework = curatorFramework;
    }

    @Override
    public StateModel createNewStateModel(String stateUnitKey, String partitionName) {
        int partition = Integer.parseInt(partitionName.substring(electionGroup.name().length() + 1));
        return new OnlineOfflineStateModel(dependencyManager, curatorFramework, nodeName, electionGroup.name(), partition);
    }

    @StateModelInfo(states = {OnlineOfflineStateModel.STATE_ONLINE, OnlineOfflineStateModel.STATE_OFFLINE, OnlineOfflineStateModel.STATE_DROPPED}, initialState = OnlineOfflineStateModel.STATE_OFFLINE)
    public static class OnlineOfflineStateModel extends StateModel {

        private static final Logger LOGGER = LoggerFactory.getLogger(OnlineOfflineStateModel.class); // NOSONAR
        public static final String STATE_ONLINE = "ONLINE";
        public static final String STATE_OFFLINE = "OFFLINE";
        public static final String STATE_DROPPED = "DROPPED";

        private final DependencyManager dependencyManager;
        private final CuratorFramework curatorFramework;
        private final String nodeName;
        private final String electionGroupName;
        private final int partition;
        private final InterProcessSemaphoreMutex mutex;
        private final String mutexPath;
        private final ConnectionStateListener connectionStateListener;
        private final LeadershipLockManager leadershipLockManager;

        private final ExecutorService executorService = Executors.newSingleThreadExecutor();

        private Component partitionLeaderComponent;

        private OnlineOfflineStateModel(DependencyManager dependencyManager, CuratorFramework curatorFramework, String nodeName, String electionGroupName, int partition) {
            this.dependencyManager = dependencyManager;
            this.nodeName = nodeName;
            this.electionGroupName = electionGroupName;
            this.partition = partition;
            this.curatorFramework = curatorFramework;
            this.leadershipLockManager = new LeadershipLockManager();
            mutexPath = "/amdatu-herding/" + getClass().getSimpleName() + "-" + electionGroupName + "-" + partition;
            mutex = new InterProcessSemaphoreMutex(curatorFramework, mutexPath);

            LOGGER.info("Create state model for election group {} partition {} ", electionGroupName, partition);

            connectionStateListener = (curatorFw, connectionState) ->
                    executorService.execute(() -> {
                        if (connectionState == ConnectionState.RECONNECTED) {
                            leadershipLockManager.onZookeeperConnectionRestored();
                        } else if (connectionState == ConnectionState.SUSPENDED) {
                            leadershipLockManager.onZookeeperConnectionLost();
                        } else if (connectionState == ConnectionState.LOST) {
                            leadershipLockManager.onZookeeperConnectionLost();
                        }
                    });

            curatorFramework.getConnectionStateListenable().addListener(connectionStateListener);
        }


        @Transition(from = STATE_OFFLINE, to = STATE_ONLINE)
        public void onLeadershipAcquired(Message message, NotificationContext context) {
            leadershipLockManager.onLeadershipAcquired();
        }

        @Transition(from = STATE_ONLINE, to = STATE_OFFLINE)
        public void onLeadershipRelinquished(Message message, NotificationContext context) {
            leadershipLockManager.onLeadershipRelinquished();
        }

        @Transition(from = STATE_OFFLINE, to = STATE_DROPPED)
        public void offlineToDropped(Message message, NotificationContext context) {
            LOGGER.info("State model for election group {} partition {} DROPPED, removing connection state listener", electionGroupName, partition);
            curatorFramework.getConnectionStateListenable().removeListener(connectionStateListener);
            executorService.shutdown();
        }

        class LeadershipLockManager {

            private final AtomicBoolean leader = new AtomicBoolean(false);
            private Thread acquireLockThread;

            void onLeadershipAcquired() {
                this.leader.set(true);

                acquireLockAndRegisterPartitionLeaderService();
            }

            /**
             * When the connection is restored first we need to take some steps to make sure we get into a known state.
             *
             *  - Unregister the PartitionLeader service
             *      To make sure it's not there as we don't actually know if we're still in the lead
             *  - release the lock we might hold as that's unstable
             *      We think we have this one but Zookeeper might disagree and think otherwise
             *  - re-acquire the lock
             *      If Helix still has this partition assigned to us we try become master again fist step there is acquire the lock
             *  - register the PartitionLeader service
             *
             */
            void onZookeeperConnectionRestored() {
                LOGGER.info("Zookeeper connection restored for election group {} partition {} ", electionGroupName, partition);
                synchronized (leader) {
                    if (partitionLeaderComponent != null) {
                        LOGGER.error("PartitionLeader component available on connection restored, that should not be possible.");
                        dependencyManager.remove(partitionLeaderComponent);
                        partitionLeaderComponent = null;
                    }

                    if (mutex.isAcquiredInThisProcess()) {
                        releaseLock();
                    }
                }
                acquireLockAndRegisterPartitionLeaderService();
            }

            void onLeadershipRelinquished() {
                this.leader.set(false); // this will interrupt the lock acquisition

                synchronized (leader) {
                    // interrupt the waiting for a lock and unregister DM components if registered
                    if (partitionLeaderComponent != null) {
                        dependencyManager.remove(partitionLeaderComponent);
                        partitionLeaderComponent = null;
                    }
                    if (mutex.isAcquiredInThisProcess()) {
                        releaseLock();
                    }
                    LOGGER.info("Node {} has relinquished leadership for election group {} partition {} ", nodeName, electionGroupName, partition);
                }
            }

            /**
             * Note in this method we can't release the lock we hold as that would require us to talk to Zookeeper. So
             * releasing the lock will be part of the re-connect handling.
             */
            void onZookeeperConnectionLost() {
                LOGGER.info("Zookeeper connection lost, unregistering PartitionLeader for election group {} partition {} ", electionGroupName, partition);
                synchronized (leader) {
                    if (partitionLeaderComponent != null) {
                        dependencyManager.remove(partitionLeaderComponent);
                        partitionLeaderComponent = null;
                    }
                }
            }

            void acquireLockAndRegisterPartitionLeaderService() {
                // try obtaining a lock (on a separate thread) and register DM components when lock becomes available
                acquireLockThread = new Thread(() -> {
                    synchronized (leader) {
                        if (leader.get() && acquireLock() && leader.get()) { // NOSONAR During acquireLock leader state can change
                            LOGGER.info("Node {} acquired leadership for election group {} partition {} ", nodeName, electionGroupName, partition);

                            Dictionary<String, Object> properties = new Hashtable<>();
                            properties.put(PartitionElectionConstants.GROUP_NAME, electionGroupName);
                            properties.put(PartitionElectionConstants.PARTITION, partition);
                            partitionLeaderComponent = dependencyManager.createComponent()
                                    .setInterface(PartitionLeader.class, properties)
                                    .setImplementation(new PartitionLeader(electionGroupName, partition));

                            dependencyManager.add(partitionLeaderComponent);
                        }
                    }
                });
                acquireLockThread.start();
            }

            private boolean acquireLock() {
                try {
                    while (!mutex.acquire(5, TimeUnit.SECONDS)) {
                        if (!curatorFramework.getZookeeperClient().isConnected()) {
                            LOGGER.info("Stopped trying to acquire lock for group: {} and partition: {} and path: {} as the Zookeeper connection got disconnected.", electionGroupName, partition, mutexPath);
                            return false;
                        }
                        if (!leader.get()) {
                            LOGGER.info("Stopped trying to acquire lock for group: {} and partition: {} and path: {} as we're no longer the leader for this partition.", electionGroupName, partition, mutexPath);
                            return false;
                        }
                        LOGGER.warn("Trying to become leader for group: {} partition: {} but failed to get partition lock for path {}.", electionGroupName, partition, mutexPath);

                        // There are some scenario's in which the acquire fails but returns immediately
                        // prevent this loop from spinning out of control when that happens
                        Thread.sleep(250);
                    }
                    LOGGER.info("Acquired lock for group: {} and partition: {} and path: {}", electionGroupName, partition, mutexPath);
                    return true;
                } catch (Exception e) {
                    LOGGER.error("Exception while trying to acquire partition leader lock for election group: {}, partition: {}, path: {}. " +
                            "Not re-trying this partition will not have a leader", electionGroupName, partition, mutexPath, e);
                    throw new RuntimeException(e);
                }
            }

            private void releaseLock() {
                try {
                    mutex.release();
                    LOGGER.info("Released lock for group: {} and partition: {} and path: {}", electionGroupName, partition, mutexPath);
                } catch (Exception e) {
                    LOGGER.error("Exception while trying to release partition leader lock election group: {}, partition: {}, path: {}", electionGroupName, partition, mutexPath, e);
                }
            }
        }


    }
}