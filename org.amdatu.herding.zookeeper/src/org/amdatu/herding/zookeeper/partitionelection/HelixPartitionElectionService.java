/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.partitionelection;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.zookeeper.helix.ToggleServiceDependency;
import org.amdatu.herding.zookeeper.partitionelection.OnlineOfflineStateModelFactory.OnlineOfflineStateModel;
import org.apache.curator.framework.CuratorFramework;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.apache.helix.*;
import org.apache.helix.api.listeners.ControllerChangeListener;
import org.apache.helix.manager.zk.ZKHelixAdmin;
import org.apache.helix.manager.zk.ZKHelixManager;
import org.apache.helix.model.ExternalView;
import org.apache.helix.model.HelixConfigScope;
import org.apache.helix.model.IdealState;
import org.apache.helix.model.builder.HelixConfigScopeBuilder;
import org.apache.helix.participant.StateMachineEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.amdatu.herding.zookeeper.helix.HelixUtil.cleanInstancesList;
import static org.amdatu.herding.zookeeper.helix.HelixUtil.withHelixClassLoader;
import static org.apache.helix.model.BuiltInStateModelDefinitions.OnlineOffline;

public class HelixPartitionElectionService implements PartitionElectionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelixPartitionElectionService.class);

    private volatile DependencyManager dependencyManager;
    private final PartitionElectionGroup electionGroup;
    private volatile Partitioner partitioner;
    private volatile NodeService nodeService;
    private volatile LockingService lockingService;
    private volatile CuratorFramework curatorFramework;

    private HelixManager helixManager;

    private final AtomicBoolean isLeader = new AtomicBoolean(false);
    private ToggleServiceDependency helixInitToggle;

    public HelixPartitionElectionService(PartitionElectionGroup electionGroup) {
        this.electionGroup = electionGroup;
    }

    void init(Component component) {
        helixInitToggle = new ToggleServiceDependency();

        component.add(helixInitToggle);

        LOGGER.info("Initializing Helix election service on node {}", nodeService.getName());

        withHelixClassLoader(() -> {
            ZKHelixAdmin admin = null;
            try {
                admin = new ZKHelixAdmin(getZookeeperConnectString());
                Lock lock = lockingService.getLock("HelixPartitionElectionServiceInitialization");
                boolean initialized = false;
                while (!initialized) {
                    try {
                        if (lock.lock(30000)) {
                            try {
                                ensureClusterExists(admin);
                                cleanInstancesList(curatorFramework, admin, electionGroup.name());
                                initialized = true;
                            } finally {
                                if (lock.valid()) {
                                    releaseUnchecked(lock);
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    } catch (LockingException e) {
                        LOGGER.warn("Failed to obtain lock", e);
                    } catch(Exception e) {
                        LOGGER.warn("Something went wrong", e);
                    }
                    if (!initialized) {
                        // just sleep and retry
                        try {
                            LOGGER.warn("Failed to initialize Helix, trying again in 5 seconds.");
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            LOGGER.error("Interrupted while trying to initialize Helix. Giving up.");
                            return; // we'll never get to initialize Helix so the toggle dependency will remain in the false state.
                        }
                    }
                }
            } finally {
                if (admin != null) {
                    admin.close();
                }
            }
        });

        withHelixClassLoader(this::initHelix);

        LOGGER.info("Helix election service on node {} initialized", nodeService.getName());
    }

    private void releaseUnchecked(Lock lock) {
        try {
            lock.release();
        } catch (LockingException e) {
            LOGGER.warn("Failed to release lock", e);
        }
    }

    void stop() {
        LOGGER.info("Stopping Helix election service on node {}", nodeService.getName());
        HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();
        clusterManagementTool.enableInstance(electionGroup.name(), nodeService.getName(), false);

        try {
            boolean allPartitionsOffline = false;
            while (!allPartitionsOffline) {
                ExternalView resourceExternalView = clusterManagementTool.getResourceExternalView(electionGroup.name(), electionGroup.name());

                allPartitionsOffline = resourceExternalView.getPartitionSet().stream()
                        .flatMap(p -> resourceExternalView.getStateMap(p).entrySet().stream())
                        .filter(e -> e.getKey().equals(nodeService.getName()))
                        .map(Map.Entry::getValue)
                        .noneMatch(OnlineOfflineStateModel.STATE_ONLINE::equals);


                if (!allPartitionsOffline) {
                    if (LOGGER.isInfoEnabled()) {
                        logPartitionState(resourceExternalView, "Waiting for node to drop leadership for all partitions: ");
                    }
                    Thread.sleep(200L);
                } else {
                    LOGGER.info("All partitions offline");
                }
            }

        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException while waiting for node to drop partition lead", e);
            Thread.currentThread().interrupt();
        }
        helixManager.disconnect();
        LOGGER.info("Stopped Helix election service on node {}", nodeService.getName());
    }

    private void ensureClusterExists(HelixAdmin helixAdmin) {
        List<String> clusters = helixAdmin.getClusters();
        if (!clusters.contains(electionGroup.name())) {

            LOGGER.info("Creating Helix cluster for ElectionGroup {} ", electionGroup.name());
            if (!helixAdmin.addCluster(electionGroup.name())) {
                LOGGER.error("Failed to create Helix cluster for election group {}", electionGroup.name());
                return;
            }

            final HelixConfigScope scope = new HelixConfigScopeBuilder(HelixConfigScope.ConfigScopeProperty.CLUSTER)
                    .forCluster(electionGroup.name())
                    .build();
            final Map<String, String> props = new HashMap<>();
            props.put(ZKHelixManager.ALLOW_PARTICIPANT_AUTO_JOIN, String.valueOf(true));
            helixAdmin.setConfig(scope, props);

            LOGGER.info("Helix cluster for ElectionGroup {} created", electionGroup.name());
        }

    }



    private void initHelix() {
        helixManager = HelixManagerFactory.getZKHelixManager(electionGroup.name(), nodeService.getName(), InstanceType.CONTROLLER_PARTICIPANT, getZookeeperConnectString());
        try {
            StateMachineEngine stateMachineEngine = helixManager.getStateMachineEngine();
            stateMachineEngine.registerStateModelFactory(OnlineOffline.name(), new OnlineOfflineStateModelFactory(dependencyManager, nodeService.getName(), electionGroup, curatorFramework));

            helixManager.connect();

            HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();

            clusterManagementTool.addStateModelDef(electionGroup.name(), OnlineOffline.name(), OnlineOffline.getStateModelDefinition());

            helixManager.addControllerListener((ControllerChangeListener) this::onControllerChange);

            // It's possible that this instance has been disabled while stopping. Make sure it's enabled
            clusterManagementTool.enableInstance(electionGroup.name(), nodeService.getName(), true);

            ExternalView resourceExternalView = null;
            while (resourceExternalView == null) {
                resourceExternalView = clusterManagementTool.getResourceExternalView(electionGroup.name(), electionGroup.name());
                if (resourceExternalView == null) {
                    LOGGER.info("Waiting for resourceExternalView to become available for election group {}", electionGroup.name());
                    Thread.sleep(500);
                }
            }

            helixInitToggle.activate(true);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Failed to initialize Helix controller and participant", e);
            helixManager.disconnect();
        } catch (Exception e) {
            LOGGER.error("Failed to initialize Helix controller and participant", e);
            helixManager.disconnect();
        }
    }


    private void onControllerChange(@SuppressWarnings("unused") NotificationContext changeContext) { // NOSONAR: used as ControllerChangeListener#onControllerChange
        synchronized (isLeader) {
            boolean wasLeader = isLeader.getAndSet(helixManager.isLeader());
            boolean leadershipAcquired = !wasLeader && isLeader.get();

            if (!leadershipAcquired) {
                // We did not become the leader for this cluster on this change, no need to do anything
                return;
            }

            LOGGER.info("Node {} acquired leadership for ElectionGroup: {}", nodeService.getName(), electionGroup.name());
            HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();
            if (!clusterManagementTool.getResourcesInCluster(electionGroup.name()).contains(electionGroup.name())) {
                LOGGER.info("Cluster resource for election group {} does not exist yet, creating resource.", electionGroup.name());

                partitioner.partitionsChanged();

                clusterManagementTool.addResource(electionGroup.name(), electionGroup.name(), electionGroup.partitions(), OnlineOffline.name(), IdealState.RebalanceMode.FULL_AUTO.name());
                clusterManagementTool.rebalance(electionGroup.name(), electionGroup.name(), 1);
            } else {
                LOGGER.info("Cluster resource for election group {} exists.", electionGroup.name());
                IdealState resourceIdealState = clusterManagementTool.getResourceIdealState(electionGroup.name(), electionGroup.name());
                int numPartitions = resourceIdealState.getNumPartitions();

                if (numPartitions != electionGroup.partitions()) {
                    LOGGER.info("Number of partitions for election group {} changed from {}  to {}.", electionGroup.name(), electionGroup.partitions(), numPartitions);
                }
                if (!resourceIdealState.isEnabled()) {
                    LOGGER.info("Resource ideal state for election group {} is not enabled.", electionGroup.name());
                }
                if (numPartitions != electionGroup.partitions() || !resourceIdealState.isEnabled()) {
                    LOGGER.info("Cluster resource for election group {} exists but needs to be updated.", electionGroup.name());
                    new Thread(this::updateClusterResource).start();
                } else {
                    LOGGER.info("Cluster resource for election group {} exists and is up-to-date, no action required", electionGroup.name());
                }
            }
        }
    }

    /**
     * The resource needs to be updated because the number of partitions has changed. This is a potentially expensive
     * operation, while processing changes all partitions will be taken offline and online again.
     *
     * <ul>
     * <li>
     * Step 1: The resource will be disabled this will make that all {@link org.amdatu.herding.partitionelection.PartitionLeader}
     * services for this election group are unregistered.
     * ** CLUSTER WIDE SO NOT JUST THIS NODE **
     * </li>
     * <li>
     * Step 2: The {@link Partitioner#partitionsChanged()} ()} callback will be called to allow the partitioner to process the changed number of partitions
     * </li>
     * <li>
     * Step 3: The number of partitions for the Apache Helix resource will be updated.
     * </li>
     * <li>
     * Step 3: The resource will be enabled again. This will trigger the {@link org.amdatu.herding.partitionelection.PartitionLeader}
     * services to be registered again.
     * </li>
     * </ul>
     */
    private void updateClusterResource() {
        LOGGER.info("Updating cluster resource for election group {}.", electionGroup.name());
        HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();
        try {
            IdealState resourceIdealState = clusterManagementTool.getResourceIdealState(electionGroup.name(), electionGroup.name());
            if (resourceIdealState.isEnabled()) {
                LOGGER.info("Disabling partition election for group {}", electionGroup.name());
                clusterManagementTool.enableResource(electionGroup.name(), electionGroup.name(), false);
            }

            boolean allPartitionsOffline = false;
            while (!allPartitionsOffline) {
                ExternalView resourceExternalView = clusterManagementTool.getResourceExternalView(electionGroup.name(), electionGroup.name());

                allPartitionsOffline = resourceExternalView.getPartitionSet().stream()
                        .flatMap(p -> resourceExternalView.getStateMap(p).entrySet().stream())
                        .map(Map.Entry::getValue)
                        .allMatch("OFFLINE"::equals);

                if (!allPartitionsOffline) {
                    if (LOGGER.isInfoEnabled()) {
                        logPartitionState(resourceExternalView, "Waiting for all partitions to go offline: ");
                    }
                    Thread.sleep(200L);
                } else {
                    LOGGER.info("All partitions offline");
                }
            }

            partitioner.partitionsChanged();

            // Drop and add instead of update, updating the num resources in the ideal state only works when the number pf partitions has increased
            clusterManagementTool.dropResource(electionGroup.name(), electionGroup.name());
            clusterManagementTool.addResource(electionGroup.name(), electionGroup.name(), electionGroup.partitions(), OnlineOffline.name(), IdealState.RebalanceMode.FULL_AUTO.name());

            clusterManagementTool.rebalance(electionGroup.name(), electionGroup.name(), 1);

            helixManager.getClusterManagmentTool().enableResource(electionGroup.name(), electionGroup.name(), true);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException while updating number of partitions for {}.", electionGroup.name(), e);
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            LOGGER.error("Exception while updating number of partitions for {}.", electionGroup.name(), e);
        }
        LOGGER.info("Updated cluster resource for election group {}.", electionGroup.name());
    }

    private void logPartitionState(ExternalView resourceExternalView, String s) {
        StringBuilder message = new StringBuilder(s);

        for (String partition : resourceExternalView.getPartitionSet()) {
            for (Map.Entry<String, String> e : resourceExternalView.getStateMap(partition).entrySet()) {
                message
                        .append("\n")
                        .append(partition)
                        .append(": ")
                        .append(e.getValue())
                        .append(" [")
                        .append(e.getKey())
                        .append("]");
            }
        }
        LOGGER.info(message.toString());
    }

    @Override
    public boolean isLeader(int partition) {
        if (partition >= electionGroup.partitions()) {
            throw new IllegalArgumentException("Partition " + partition + " does not exist");
        }

        HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();
        ExternalView resourceExternalView = clusterManagementTool.getResourceExternalView(electionGroup.name(), electionGroup.name());

        String partitionName = electionGroup.name() + "_" + partition;
        Map<String, String> stateMap = resourceExternalView.getStateMap(partitionName);

        return OnlineOfflineStateModel.STATE_ONLINE.equals(stateMap.get(nodeService.getName()));
    }

    @Override
    public String getLeader(int partition) {
        if (partition >= electionGroup.partitions()) {
            throw new IllegalArgumentException("Partition " + partition + " does not exist");
        }

        HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();
        ExternalView resourceExternalView = clusterManagementTool.getResourceExternalView(electionGroup.name(), electionGroup.name());

        String partitionName = electionGroup.name() + "_" + partition;
        Map<String, String> stateMap = resourceExternalView.getStateMap(partitionName);

        if (stateMap == null) {
            return null;
        }

        return stateMap.entrySet().stream()
                .filter(e -> e.getValue().equals(OnlineOfflineStateModel.STATE_ONLINE))
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(null);
    }

    @Override
    public Collection<String> getElectionCandidates() {
        return helixManager.getClusterManagmentTool().getInstancesInCluster(electionGroup.name());
    }

    /**
     * Get the Zookeeper client connect string. As we have a CuratorFramework dependency already we can ask that for the
     * connect string.
     *
     * As we don't have the namespace support as we have with Apache Curator in this Apache Helix backed component
     * append the namespace to the connect string. This will use Zookeeper's chroot feature to mimic the namespace
     *
     * @return The Zookeeper connect string to be used by this service
     */
    private String getZookeeperConnectString() {
        String connectionString = curatorFramework.getZookeeperClient().getCurrentConnectionString();

        if (curatorFramework.getNamespace() != null) {
            connectionString = connectionString.trim() + "/" + curatorFramework.getNamespace();
        }

        LOGGER.info("Partition election is using Zookeeper connect string {}", connectionString);

        return connectionString;
    }

}
