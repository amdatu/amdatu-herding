/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.partitionelection;

import java.util.Properties;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitioner.Partitioner;
import org.apache.curator.framework.CuratorFramework;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.amdatu.herding.partitionelection.PartitionElectionConstants.GROUP_NAME;

public class HelixPartitionElectionGroupAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelixPartitionElectionGroupAdapter.class);

    private volatile DependencyManager dependencyManager;
    private volatile PartitionElectionGroup electionGroup;

    private Component partitionElectionServiceComponent;

    void start() {
        LOGGER.info("Registering HelixPartitionElectionService component for {}", electionGroup);
        Properties serviceProperties = new Properties();
        serviceProperties.put(GROUP_NAME, electionGroup.name());

        partitionElectionServiceComponent = dependencyManager.createComponent()
                .setInterface(PartitionElectionService.class, serviceProperties)
                .setImplementation(new HelixPartitionElectionService(electionGroup))
                .add(dependencyManager.createServiceDependency().setService(NodeService.class).setRequired(true))
                .add(dependencyManager.createServiceDependency().setService(CuratorFramework.class).setRequired(true))
                .add(dependencyManager.createServiceDependency().setService(LockingService.class).setRequired(true))
                .add(dependencyManager.createServiceDependency().setService(Partitioner.class, String.format("(%s=%s)", GROUP_NAME, electionGroup.name())).setRequired(true));
        dependencyManager.add(partitionElectionServiceComponent);
    }

    void stop() {
        if (partitionElectionServiceComponent != null) {
            LOGGER.info("Unregistering HelixPartitionElectionService component for {}", electionGroup);
            dependencyManager.remove(partitionElectionServiceComponent);
        }
    }
}
