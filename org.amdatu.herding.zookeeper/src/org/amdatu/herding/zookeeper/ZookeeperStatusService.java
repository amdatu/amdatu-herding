/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import org.amdatu.herding.CoordinatorStatusService;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * A {@link CoordinatorStatusService} that provides information about Zookeeper (e.g. whether there is a working connection)
 */
public class ZookeeperStatusService implements CoordinatorStatusService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZookeeperStatusService.class);
    private volatile CuratorFramework curatorFramework;

    public boolean isHealthy() {
        try {
            return curatorFramework.blockUntilConnected(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Could not connect with zookeeper", e);
            Thread.currentThread().interrupt();
            return false;
        }
    }
}
