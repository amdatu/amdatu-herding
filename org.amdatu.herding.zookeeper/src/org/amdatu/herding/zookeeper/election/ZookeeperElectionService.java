/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.election;

import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.election.Leader;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListener;
import org.apache.curator.framework.recipes.leader.Participant;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ElectionService} implementation backed by Zookeeper
 */
public class ZookeeperElectionService implements ElectionService, LeaderSelectorListener {

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperElectionService.class);

    @SuppressWarnings("unused") // Injected by DM
    private volatile DependencyManager dependencyManager;
    @SuppressWarnings("unused") // Injected by DM
    private volatile Component component;
    @SuppressWarnings("unused") // Injected by DM
    private volatile CuratorFramework curatorFramework;
    @SuppressWarnings("unused") // Injected by DM
    private volatile NodeService nodeService;

    private ElectionGroup electionGroup;

    private LeaderSelector leaderSelector;

    private volatile boolean started = false;

    @Override
    public Collection<String> getElectionCandidates() {
        try {
            return leaderSelector.getParticipants().stream()
                    .map(Participant::getId)
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isLeader() {
        return leaderSelector.getId().equals(getLeader());
    }

    @Override
    public String getLeader() {
        Participant leader = getLeadingParticipant();

        if (leader.getId().equals("")){
            return null;
        }
        return leader.getId();
    }

    private Participant getLeadingParticipant() {
        try {
            if (!curatorFramework.blockUntilConnected(0, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Not connected to zookeeper");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("Not connected to zookeeper", e);
        }

        try {
            return leaderSelector.getLeader();
        } catch (Exception e) {
            throw new RuntimeException("Failed to get leader", e);
        }
    }

    @SuppressWarnings("unused") // DM callback
    protected final void updated(ElectionGroup electionGroup) throws ConfigurationException, InterruptedException {
        if (electionGroup == null) {
            return;
        }
        LOG.info("Update configuration for election group '{}'", electionGroup.name());

        //noinspection ConstantConditions
        if (electionGroup.name() == null || electionGroup.name().trim().isEmpty()) {
            throw new ConfigurationException("name", "Name is required");
        }

        this.electionGroup = electionGroup;

        if (started) {
            stop();
            start();
        }

    }

    protected final void start() {
        LOG.info("Starting election service for group '{}' on node '{}'", electionGroup.name(), nodeService.getName());
        started = true;
        String path = electionGroup.name();
        if (!path.startsWith("/")) {
            path = "/" + path;
        }

        leaderSelector = new LeaderSelector(curatorFramework, path, this);
        leaderSelector.setId(nodeService.getName());
        leaderSelector.start();

        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        if (serviceProperties == null) {
            serviceProperties = new Hashtable<>();
        }
        serviceProperties.put(Leader.GROUP_NAME, electionGroup.name());
        component.setServiceProperties(serviceProperties);
    }

    protected final void stop() throws InterruptedException {

        LOG.info("Stopping election service for group '{}' on node '{}'", electionGroup.name(), nodeService.getName());
        started = false;

        try {
            leaderSelector.interruptLeadership();
            if (leaderSelector.hasLeadership()) {
                while (leaderSelector.hasLeadership()) {
                    LOG.info("Waiting for leadership of group {} to be released", electionGroup.name());
                    Thread.sleep(100);
                }
            }
            leaderSelector.close();
            leaderSelector = null;

            LOG.info("Node '{}' gave up leadership of group '{}' ", nodeService.getName(), electionGroup.name());
        } catch (RuntimeException e) {
            LOG.error("Exception in stop", e);
        }
    }


    @Override
    public void takeLeadership(CuratorFramework framework) {
        LOG.info("Node '{}' taking leadership of group '{}'", nodeService.getName(), electionGroup.name());

        Dictionary<String, Object> props = new Hashtable<>();

        props.put(Leader.GROUP_NAME, electionGroup.name());
        Component leaderComponent = dependencyManager.createComponent()
                .setInterface(Leader.class.getName(), props)
                .setImplementation(new Leader());
        try {
            dependencyManager.add(leaderComponent);

            Thread.sleep(Long.MAX_VALUE);

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOG.info("Node '{}' gave up leadership of group '{}'", nodeService.getName(), electionGroup.name());
        } catch (Exception e) {
            LOG.error("Unexpected exception on node '{}' while in the lead of group '{}', giving up leadership", nodeService.getName(), electionGroup.name(), e);
        } finally {
            dependencyManager.remove(leaderComponent);
            if (started) {
                try {
                    leaderSelector.requeue();
                } catch (RuntimeException e) {
                    LOG.error("Node '{}' failed to requeue leader selector for lead of group '{}'", nodeService.getName(), electionGroup.name(), e);
                }
            }
        }
    }

    @Override
    public void stateChanged(CuratorFramework curatorFramework, ConnectionState connectionState) {

        if (connectionState == ConnectionState.SUSPENDED || connectionState == ConnectionState.LOST) {
            if (leaderSelector.hasLeadership()) {
                LOG.info("Connection problems while in the lead on node '{}'", nodeService.getName());
                leaderSelector.interruptLeadership();
            }
        } else if (connectionState == ConnectionState.RECONNECTED) {
            LOG.info("Connection restored on node '{}', try to become leader again. ", nodeService.getName());
            leaderSelector.requeue();
        }
    }

}
