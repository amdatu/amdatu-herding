/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import java.util.Collection;

import org.amdatu.herding.ClusterService;
import org.amdatu.herding.NodeService;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.nodes.GroupMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ClusterService} implementation backed by Zookeeper
 */
public class ZookeeperClusterService implements ClusterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZookeeperClusterService.class);

    private static final String CLUSTER_GROUP_PATH = "/amdatu-herding/cluster";

    @SuppressWarnings("unused") // Injected by DM
    private volatile CuratorFramework curatorFramework;
    @SuppressWarnings("unused") // Injected by DM
    private volatile NodeService nodeService;

    private GroupMember groupMember;

    @Override
    public Collection<String> getMembers() {
        return groupMember.getCurrentMembers().keySet();
    }

    @SuppressWarnings("unused") // DM callback
    protected final void start() {
        LOGGER.info("Starting ZookeeperClusterService");
        groupMember = new GroupMember(curatorFramework, CLUSTER_GROUP_PATH, nodeService.getName(), nodeService.getName().getBytes());
        groupMember.start();
        LOGGER.info("Started ZookeeperClusterService");
    }

    @SuppressWarnings("unused") // DM callback
    protected final void stop() {
        LOGGER.info("Stopping ZookeeperClusterService");
        if (groupMember != null) {
            try {
                groupMember.close();
            } catch (RuntimeException e) {
                LOGGER.warn("Exception while closing ZookeeperClusterService groupMember", e);
            }
            groupMember = null;
        }
        LOGGER.info("Stopped ZookeeperClusterService");
    }
}
