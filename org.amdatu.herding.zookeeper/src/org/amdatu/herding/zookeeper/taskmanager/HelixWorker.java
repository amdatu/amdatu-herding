/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.taskmanager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.taskmanager.Task;
import org.amdatu.herding.taskmanager.*;
import org.amdatu.herding.zookeeper.helix.HelixServiceBase;
import org.amdatu.herding.zookeeper.helix.HelixUtil;
import org.amdatu.herding.zookeeper.helix.ToggleServiceDependency;
import org.apache.curator.framework.CuratorFramework;
import org.apache.helix.*;
import org.apache.helix.model.BuiltInStateModelDefinitions;
import org.apache.helix.participant.StateMachineEngine;
import org.apache.helix.task.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class HelixWorker extends HelixServiceBase implements Worker {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelixWorker.class);
    private static final String KEY_TASK_NAME = Task.INTERNAL_KEY_PREFIX + "TASK_NAME";
    private static final String KEY_TASK_COMMAND = Task.INTERNAL_KEY_PREFIX  + "TASK_COMMAND";
    private static final String WORKER_CONTEXT_COMMAND = "WORKER_CONTEXT_COMMAND";
    private static final int UUID_STRING_LENGTH = UUID.randomUUID().toString().length();

    private volatile WorkerContext workerContext;

    private volatile CuratorFramework curatorFramework;
    private volatile NodeService nodeService;
    private volatile LockingService lockingService;

    private ToggleServiceDependency helixInitToggle = new ToggleServiceDependency();
    private HelixManager helixManager;
    private TaskDriver taskDriver;
    private TaskStateModelFactory taskStateModelFactory;
    private LoadingCache<String, String> workflowIdCache;

    public HelixWorker() {
        workflowIdCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .build(new CacheLoader<String, String>() {
                           @Override
                           public String load(String s) {
                               Map<String, WorkflowConfig> workflows = taskDriver.getWorkflows();
                               return workflows.keySet().stream()
                                       .filter(key -> key.startsWith(s + ": ") )
                                       .findFirst()
                                       .orElse(null);
                           }
                       }
                );
    }

    @Override
    protected void init(org.apache.felix.dm.Component component) {
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(Worker.WORKER_NAME, workerContext.name());
        component.setServiceProperties(serviceProperties);

        component.add(helixInitToggle);
        LOGGER.info("Initializing task manager cluster");

        super.init(component);
        LOGGER.info("Initialized task manager cluster");

        HelixUtil.withHelixClassLoader(this::initHelix);
    }

    private void initHelix() {
        helixManager = HelixManagerFactory.getZKHelixManager(clusterName(), nodeName(), InstanceType.CONTROLLER_PARTICIPANT, getZookeeperConnectString());
        try {
            StateMachineEngine stateMach = helixManager.getStateMachineEngine();
            Map<String, TaskFactory> taskRegistry = new ConcurrentHashMap<>();
            taskRegistry.put(WORKER_CONTEXT_COMMAND, this::createNewTask);

            taskStateModelFactory = new TaskStateModelFactory(helixManager, taskRegistry);
            stateMach.registerStateModelFactory(BuiltInStateModelDefinitions.Task.name(), taskStateModelFactory);

            helixManager.connect();

            HelixAdmin clusterManagementTool = helixManager.getClusterManagmentTool();

            clusterManagementTool.addStateModelDef(clusterName(), BuiltInStateModelDefinitions.Task.getStateModelDefinition().getId(), BuiltInStateModelDefinitions.Task.getStateModelDefinition(), true);
            helixInitToggle.activate(true);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Failed to initialize Helix controller and participant", e);
            helixManager.disconnect();
        } catch (Exception e) {
            LOGGER.error("Failed to initialize Helix controller and participant", e);
            helixManager.disconnect();
        }
    }


    private org.apache.helix.task.Task createNewTask(TaskCallbackContext context) {
        TaskConfig taskConfig = context.getTaskConfig();
        Map<String, String> taskConfigMap = taskConfig.getConfigMap();
        Task.TaskBuilder taskBuilder = Task.builder()
                .id(taskConfig.getId())
                .name(taskConfigMap.get(KEY_TASK_NAME))
                .command(taskConfigMap.get(KEY_TASK_COMMAND));

        for (Map.Entry<String, String> contextEntry : taskConfigMap.entrySet()) {
            if (contextEntry.getKey().startsWith(Task.INTERNAL_KEY_PREFIX)) {
                continue; // Skip internal keys
            }
            taskBuilder.setContextProperty(contextEntry.getKey(), contextEntry.getValue());
        }

        TaskRunnable taskRunnable = workerContext.createTaskRunnable(taskBuilder.build());

        return new org.apache.helix.task.Task() {
            private boolean cancelled = false;
            @Override
            public TaskResult run() {
                taskRunnable.run();
                if(cancelled) {
                    return new TaskResult(TaskResult.Status.CANCELED, null);
                }
                return new TaskResult(TaskResult.Status.COMPLETED, null);
            }

            @Override
            public void cancel() {
                taskRunnable.cancel();
                cancelled = true;
            }
        };
    }

    void start() {
        HelixUtil.withHelixClassLoader(() -> taskDriver = new TaskDriver(helixManager));
    }

    void stop() {
        taskDriver = null;
    }

    protected void destroy() {
        if (taskStateModelFactory != null) {
            taskStateModelFactory.shutdown();
        }
        if (helixManager != null) {
            helixManager.disconnect();
        }
    }

    /**
     * Get the Zookeeper client connect string. As we have a CuratorFramework dependency already we can ask that for the
     * connect string.
     * <p>
     * As we don't have the namespace support as we have with Apache Curator in this Apache Helix backed component
     * append the namespace to the connect string. This will use Zookeeper's chroot feature to mimic the namespace
     *
     * @return The Zookeeper connect string to be used by this service
     */
    private String getZookeeperConnectString() {
        String connectionString = curatorFramework.getZookeeperClient().getCurrentConnectionString();

        if (curatorFramework.getNamespace() != null) {
            connectionString = connectionString.trim() + "/" + curatorFramework.getNamespace();
        }

        LOGGER.info("Partition election is using Zookeeper connect string {}", connectionString);

        return connectionString;
    }

    @Override
    protected String nodeName() {
        return nodeService.getName();
    }

    @Override
    protected String clusterName() {
        return "TaskManager-" + workerContext.name();
    }

    @Override
    protected String zookeeperConnectString() {
        return getZookeeperConnectString();
    }

    @Override
    protected LockingService lockingService() {
        return lockingService;
    }

    @Override
    protected CuratorFramework curatorFramework() {
        return curatorFramework;
    }

    @Override
    public void start(WorkPackage... workPackages) {
        List<Workflow> workflows = stream(workPackages).map(this::buildWorkflow).collect(toList());

        for (Workflow workflow : workflows) {
            LOGGER.info("Starting work package '{}'", workflow.getName());
            taskDriver.start(workflow);
        }

        for (Workflow workflow : workflows) {
            try {
                taskDriver.pollForWorkflowState(workflow.getName(), TaskState.values());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new WorkerException("Interrupted while waiting for work package to get started", e);
            }
        }
    }

    private Workflow buildWorkflow(WorkPackage workPackage) {
        String workflowId = workPackage.getId() + ": " + workPackage.getName();

        Workflow.Builder workflowBuilder = new Workflow.Builder(workflowId);
        TaskGroup previous = null;
        for (TaskGroup taskGroup : workPackage.getTaskGroups()) {
            JobConfig.Builder jobConfigBuilder = new JobConfig.Builder()
                    .setJobId(taskGroup.getId())
                    .setNumConcurrentTasksPerInstance(workerContext.concurrentTasksPerNode())
                    .setTimeoutPerTask(workerContext.timeoutPerTask())
                    .setMaxAttemptsPerTask(1)
                    .addTaskConfigs(taskGroup.getTasks().stream()
                            .map(this::toHelixTask)
                            .collect(toList()));

            workflowBuilder.addJob(taskGroup.getId(), jobConfigBuilder);

            if (previous != null) {
                workflowBuilder.addParentChildDependency(previous.getId(), taskGroup.getId());
            }
            previous = taskGroup;
        }

        return workflowBuilder.build();
    }

    @Override
    public void cancel(String workPackageId) {
        String workflowId = getWorkflowId(workPackageId);
        taskDriver.stop(workflowId);
    }

    @Override
    public void delete(String workPackageId) {
        String workflowId = getWorkflowId(workPackageId);
        taskDriver.delete(workflowId);
    }

    @Override
    public Status getStatus(String workPackageId) {
        String workflowId = getWorkflowId(workPackageId);

        WorkflowContext workflowContext = taskDriver.getWorkflowContext(workflowId);
        if (workflowContext != null) {
            TaskState taskState = workflowContext.getWorkflowState();
            Status status = toStatus(taskState);
            if(LOGGER.isDebugEnabled() && status == Status.FAILED) {
                LOGGER.debug("The status of the workflow is failed, the status of the jobs and tasks are {}", listStates());
            }
            return status;
        }
        return null;
    }

    private String getWorkflowId(String workPackageId) {
        try {
            return workflowIdCache.get(workPackageId);
        } catch (ExecutionException e) {
            throw new WorkerException("Failed to get workflow id for work package", e);
        }
    }

    @Override
    public Status awaitCompletion(String workPackageId, long timeout) throws TimeoutException, InterruptedException {
        String workflowId;
        try {
            workflowId = workflowIdCache.get(workPackageId);
        } catch (ExecutionException e) {
            throw new WorkerException(e);
        }
        try {
            TaskState taskState = taskDriver.pollForWorkflowState(workflowId, timeout, TaskState.FAILED, TaskState.COMPLETED, TaskState.ABORTED, TaskState.TIMED_OUT, TaskState.STOPPED);
            Status status =  toStatus(taskState);
            if(LOGGER.isDebugEnabled() && status == Status.FAILED) {
                LOGGER.debug("Await for completion resulted in FAILED, the status of the jobs and tasks are {}", listStates());
            }
            return status;
        } catch (HelixException e) {
            if (e.getMessage().contains("context is empty or not in states")) {
                throw new TimeoutException(String.format("Workpackage %s did not complete within %dms.", workPackageId, timeout));
            } else {
                throw new WorkerException(e);
            }
        }
    }

    @Override
    public Map<String, String> listStates() {
        Map<String, String> states = new HashMap<>();
        Collection<WorkflowConfig> workflowConfigs = taskDriver.getWorkflows().values();

        for(WorkflowConfig workflowConfig: workflowConfigs) {
            WorkflowContext workflowContext = taskDriver.getWorkflowContext(workflowConfig.getWorkflowId());
            TaskState workflowState = workflowContext.getWorkflowState();
            states.put("Workflow: " + workflowConfig.getWorkflowId(), workflowState.name());

            Map<String, TaskState> jobStates = workflowContext.getJobStates();
            if(jobStates != null) {
                jobStates.forEach((k,v) ->{
                    states.put("Job: " + k , v.name());
                    JobContext jobContext = taskDriver.getJobContext(k);
                    if(jobContext != null) {
                        int i = 0;
                        TaskPartitionState partitionState;
                        while((partitionState = jobContext.getPartitionState(i)) != null){
                            states.put("Partition state: "+ i, partitionState.name());
                            i++;
                        }
                    }
                });
            }
        }
        return states;
    }


    @Override
    public Map<String, String> listRunning() {
        return taskDriver.getWorkflows().keySet().stream()
                .filter((workflowId) -> {
                    WorkflowContext workflowContext = taskDriver.getWorkflowContext(workflowId);

                    if (workflowContext == null) {
                        LOGGER.warn("No workflow context for workflow: {}. Assume not running.", workflowId);
                        return false;
                    }

                    TaskState workflowState = workflowContext.getWorkflowState();
                    return workflowState == TaskState.NOT_STARTED ||
                            workflowState == TaskState.IN_PROGRESS ||
                            workflowState == TaskState.TIMING_OUT ||
                            workflowState == TaskState.FAILING;

                })
                .collect(Collectors.toMap(
                        HelixWorker::getWorkPackageId,
                        HelixWorker::getWorkPackageName
                ));
    }

    private static String getWorkPackageId(String workflowId) {
        return workflowId.substring(0, UUID_STRING_LENGTH);
    }

    private static String getWorkPackageName(String workflowId) {
        return workflowId.substring(UUID_STRING_LENGTH + 2);
    }

    private TaskConfig toHelixTask(Task task) {
        TaskConfig.Builder taskConfigBuilder = new TaskConfig.Builder()
                .setCommand(WORKER_CONTEXT_COMMAND)
                .addConfig(KEY_TASK_NAME, task.getName())
                .addConfig(KEY_TASK_COMMAND, task.getCommand())
                .setTaskId(task.getId());

        for (Map.Entry<String, String> taskContextEntry : task.getContext().entrySet()) {
            String key = taskContextEntry.getKey();
            taskConfigBuilder.addConfig(key, taskContextEntry.getValue());
        }

        return taskConfigBuilder.build();
    }


    private Status toStatus(TaskState taskState) {
        switch (taskState) {
            case NOT_STARTED:
                return Status.TODO;
            case IN_PROGRESS:
            case STOPPING:
            case TIMING_OUT:
            case FAILING:
                return Status.IN_PROGRESS;
            case STOPPED:
            case FAILED:
            case ABORTED:
            case TIMED_OUT:
                return Status.FAILED;
            case COMPLETED:
                return Status.COMPLETED;
            default:
                throw new IllegalStateException("Unexpected value: " + taskState);
        }
    }

}
