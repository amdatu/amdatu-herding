/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper;

import org.amdatu.herding.zookeeper.metatype.ZookeeperConfiguration;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class CuratorFrameworkManager {

    private static final Logger LOG = LoggerFactory.getLogger(CuratorFrameworkManager.class);
    public static final int ZOOKEEPER_INITIAL_CONNECT_TIMEOUT = 30;

    @SuppressWarnings("unused") // Injected by DM
    private volatile DependencyManager dependencyManager;
    @SuppressWarnings("unused") // Injected by DM
    private volatile ZookeeperConfiguration zookeeperConfiguration;

    private volatile CuratorFramework curatorFramework;
    private volatile Component curatorFrameworkComponent;

    @SuppressWarnings("unused") // DM callback
    protected final void updated(ZookeeperConfiguration zookeeperConfiguration) throws ConfigurationException {
        if (zookeeperConfiguration == null) {
            return;
        }
        LOG.info("Zookeeper configuration updated on node '{}'", zookeeperConfiguration.nodeName());

        if (zookeeperConfiguration.connectString() == null || zookeeperConfiguration.connectString().trim().isEmpty()) {
            throw new ConfigurationException("connectString", "Zookeeper connect string is required");

        }
        this.zookeeperConfiguration = zookeeperConfiguration;

        if (curatorFramework != null) {
            stop();
            start();
        }
    }

    protected final void start() {

        LOG.info("Start curator framework with connect string '{}' on node '{}'", zookeeperConfiguration.connectString(), zookeeperConfiguration.nodeName());

        curatorFramework = CuratorFrameworkFactory.builder()
                .namespace(zookeeperConfiguration.namespace())
                .connectString(zookeeperConfiguration.connectString())
                .connectionTimeoutMs(zookeeperConfiguration.connectionTimeoutMs())
                .sessionTimeoutMs(zookeeperConfiguration.sessionTimeoutMs())
                .retryPolicy(new ExponentialBackoffRetry(10, 29, 10_000))
                .build();

        new Thread(() -> {
            LOG.info("Starting curatorFramework");

            curatorFramework.start();
            try {
                // validate the connection
                while (!curatorFramework.blockUntilConnected(ZOOKEEPER_INITIAL_CONNECT_TIMEOUT, TimeUnit.SECONDS)) {
                    LOG.warn("Zookeeper connection not available after {} seconds, node: {}, zookeeper connect '{}'. " +
                            "Will keep trying but this is bad", ZOOKEEPER_INITIAL_CONNECT_TIMEOUT, zookeeperConfiguration.nodeName(), zookeeperConfiguration.connectString());
                }
            } catch (InterruptedException e) {
                LOG.error("Zookeeper connection failed, node: {}, zookeeper connect '{}'.", zookeeperConfiguration.nodeName(), zookeeperConfiguration.connectString(), e);
                Thread.currentThread().interrupt();
                return;
            } catch (Exception e) {
                LOG.error("Zookeeper connection failed, node: {}, zookeeper connect '{}'.", zookeeperConfiguration.nodeName(), zookeeperConfiguration.connectString(), e);
            }

            LOG.info("Started curatorFramework");

            curatorFrameworkComponent = dependencyManager.createComponent()
                    .setInterface(CuratorFramework.class.getName(), null)
                    .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy")
                    .setImplementation(curatorFramework);

            dependencyManager.add(curatorFrameworkComponent);
        }).start();
    }


    protected final void stop() {
        LOG.info("Stop curator framework on node '{}'", zookeeperConfiguration.nodeName());
        if (curatorFrameworkComponent != null) {
            dependencyManager.remove(curatorFrameworkComponent);
            curatorFrameworkComponent = null;
        }
        curatorFramework.close();
        curatorFramework = null;
    }

}
