/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.locking;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.zookeeper.NodeNameLookup;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.*;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * {@link LockingService} implementation backed by Zookeeper
 */
public class ZookeeperLockingService implements LockingService {

    private static final Logger LOG = LoggerFactory.getLogger(ZookeeperLockingService.class);

    @SuppressWarnings("unused") // Injected by DM
    private volatile CuratorFramework curatorFramework;
    @SuppressWarnings("unused") // Injected by DM
    private volatile NodeService nodeService;
    @SuppressWarnings("unused") // Injected by DM
    private volatile NodeNameLookup nodeNameLookup;

    private ConnectionStateListener connectionStateListener;

    private ScheduledExecutorService executorService;

    private final List<WeakReference<AbstractZookeeperLock>> locks = new ArrayList<>();
    private final List<AbstractZookeeperLock> invalidatedLocks = new ArrayList<>();

    @Override
    public Lock getLock(String name) {
        return getLock(name, false);
    }


    @Override
    public Lock getReentrantLock(String name) {
        return getLock(name, true);
    }

    private Lock getLock(String name, boolean reentrant) {

        ZookeeperLock lock;
        if (reentrant) {
            lock = new ReentrantZookeeperLock(curatorFramework, name);
        } else {
            lock = new ZookeeperLock(curatorFramework, name);
        }

        synchronized (locks) {
            locks.add(new WeakReference<>(lock));
        }
        return lock;
    }

    @SuppressWarnings("unused") // DM callback
    protected final void start() {
        LOG.info("Start locking service on node '{}'", nodeService.getName());
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(() -> {
            synchronized (locks) {
                locks.removeIf(ref -> ref.get() == null);
            }
        }, 60, 60, TimeUnit.SECONDS);


        connectionStateListener = (curatorFw, connectionState) -> {
            if (connectionState == ConnectionState.SUSPENDED || connectionState == ConnectionState.LOST) {
                LOG.info("Connection problems on node '{}', locks will be invalidated,", nodeService.getName());
                invalidateLocks();
            } else if (connectionState == ConnectionState.CONNECTED || connectionState == ConnectionState.RECONNECTED) {
                LOG.info("Zookeeper re-connected problems on node '{}', locks will be invalidated,", nodeService.getName());

                List<AbstractZookeeperLock> locksToRelease;
                synchronized (invalidatedLocks) {
                    locksToRelease = new ArrayList<>(invalidatedLocks);
                    invalidatedLocks.clear();
                }

                locksToRelease.forEach((lock) -> {
                    try {
                        if (lock.acquiredInternal()) {
                            lock.releaseInternal();
                        }
                    } catch (LockingException e) {
                        // Not much we can do here just log the issue.
                        LOG.error("Failed to release lock: '{}'. At this point the lock state is unknown and the lock is potentially locked until node '{}' is restarted.", lock.name, nodeService.getName(), e);
                    }
                });
            }
        };
        curatorFramework.getConnectionStateListenable().addListener(connectionStateListener);

    }

    @SuppressWarnings("unused") // DM callback
    protected final void stop() {
        LOG.info("Stop locking service on node '{}'", nodeService.getName());

        curatorFramework.getConnectionStateListenable().removeListener(connectionStateListener);
        invalidateLocks();

        executorService.shutdown();
        executorService = null;
    }

    private void invalidateLocks() {
        LOG.info("Invalidate locks on node '{}'", nodeService.getName());

        List<AbstractZookeeperLock> locksToInvalidate;
        synchronized (locks) {
            locksToInvalidate = locks.stream()
                    .map(WeakReference::get)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            locks.clear();
        }

        synchronized (invalidatedLocks) {
            invalidatedLocks.addAll(locksToInvalidate);
        }

        locksToInvalidate.forEach(AbstractZookeeperLock::invalidate);
    }


    private abstract class AbstractZookeeperLock implements Lock {

        private final CuratorFramework client;
        private final AtomicBoolean valid = new AtomicBoolean(true);
        protected final String name;
        private final InterProcessLock mutex;

        private final LockInternalsSorter sorter = StandardLockInternalsDriver::standardFixForSorting;

        AbstractZookeeperLock(CuratorFramework client, String name) {
            if (name.startsWith("/")) {
                throw new IllegalArgumentException("Lock name should not start with '/'");
            }

            this.client = client.newWatcherRemoveCuratorFramework();
            this.name = name;
            this.mutex = new InterProcessSemaphoreMutex(curatorFramework, getLockPath());
        }

        protected abstract String getLockPath();

        @Override
        public boolean lock(long timeout) throws LockingException {
            ensureLockValid();
            try {
                boolean acquire = mutex.acquire(timeout, TimeUnit.MILLISECONDS);
                if (!acquire) {
                    LOG.info("Failed to acquire lock, lock is currently held by node: '{}'", getLockOwner());
                }
                return acquire;
            } catch (Exception e) {
                throw new LockingException("Failed to acquire lock", e);
            }
        }

        @Override
        public void release() throws LockingException {
            releaseInternal();
        }

        private void releaseInternal() throws LockingException {
            try {
                mutex.release();
            } catch (Exception e) {
                throw new LockingException("Failed to release lock", e);
            }
        }

        @Override
        public String getLockOwner() throws LockingException {
            ensureLockValid();
            try {
                String lockContenderPath = getLockPath() + "/leases";
                Iterator<String> participantIterator = LockInternals.getParticipantNodes(client, lockContenderPath, name, sorter).iterator();
                if (!participantIterator.hasNext()) {
                    return null;
                }

                String next = participantIterator.next();
                Stat stat = client.checkExists().forPath(next);
                long lockOwner = stat.getEphemeralOwner();

                return nodeNameLookup.getNodeNameForSessionId(lockOwner);
            } catch (Exception e) {
                throw new LockingException("Failed to get the owner for lock: " + name, e);
            }
        }

        private void ensureLockValid() throws LockingException {
            if (!valid()) {
                throw new LockingException("The lock is not valid.");
            }
        }

        @Override
        public boolean acquired() {
            return valid() && acquiredInternal();
        }

        private boolean acquiredInternal() {
            return mutex.isAcquiredInThisProcess();
        }

        @Override
        public boolean valid() {
            return valid.get();
        }

        void invalidate() {
            this.valid.set(false);
        }
    }

    private class ZookeeperLock extends AbstractZookeeperLock {

        ZookeeperLock(CuratorFramework client, String name) {
            super(client, name);
        }

        @Override
        protected String getLockPath() {
            return "/amdatu-herding/locks/" + name;
        }
    }

    private class ReentrantZookeeperLock extends ZookeeperLock {

        private AtomicInteger lockCount = new AtomicInteger(0);
        private Thread owner;

        ReentrantZookeeperLock(CuratorFramework client, String name) {
            super(client, name);
        }

        @Override
        public boolean lock(long timeout) throws LockingException {

            if (owner == Thread.currentThread()) {
                lockCount.incrementAndGet();
                return true;
            }

            boolean gotLock = super.lock(timeout);

            if (gotLock) {
                owner = Thread.currentThread();
                lockCount.incrementAndGet();
            }

            return gotLock;
        }

        @Override
        public void release() throws LockingException {
            if (owner != Thread.currentThread()) {
                throw new LockingException("Lock owned by another thread");
            }

            if (lockCount.decrementAndGet() == 0) {
                owner = null;
                super.release();
            }
        }

        @Override
        public boolean acquired() {
            return owner == Thread.currentThread() && super.acquired();
        }

        @Override
        protected String getLockPath() {
            return "/amdatu-herding/reentrant-locks/" + name;
        }

    }

}
