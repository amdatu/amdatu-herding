/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.helix;

import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;
import org.apache.curator.framework.CuratorFramework;
import org.apache.felix.dm.Component;
import org.apache.helix.manager.zk.ZKHelixAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.amdatu.herding.zookeeper.helix.HelixUtil.*;

public abstract class HelixServiceBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelixServiceBase.class);

    protected abstract String nodeName();
    protected abstract String clusterName();
    protected abstract String zookeeperConnectString();
    protected abstract LockingService lockingService();
    protected abstract CuratorFramework curatorFramework();


    protected void init(Component component) {
        withHelixClassLoader(() -> {
            ZKHelixAdmin admin = null;
            try {
                admin = new ZKHelixAdmin(zookeeperConnectString());
                Lock lock = lockingService().getLock("helix-cluster-init-" + clusterName());
                boolean initialized = false;
                while (!initialized) {
                    try {
                        if (lock.lock(30000)) {
                            try {
                                ensureClusterExists(admin, clusterName());
                                cleanInstancesList(curatorFramework(), admin, clusterName());
                                initialized = true;
                            } finally {
                                if (lock.valid()) {
                                    releaseUnchecked(lock);
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    } catch (LockingException e) {
                        LOGGER.warn("Failed to obtain lock", e);
                    } catch (Exception e) {
                        LOGGER.warn("Something went wrong", e);
                    }
                    if (!initialized) {
                        // just sleep and retry
                        try {
                            LOGGER.warn("Failed to initialize Helix, trying again in 5 seconds.");
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            LOGGER.error("Interrupted while trying to initialize Helix. Giving up.");
                            return; // we'll never get to initialize Helix so the toggle dependency will remain in the false state.
                        }
                    }
                }
            } finally {
                if (admin != null) {
                    admin.close();

                }
            }
        });
    }

    private void releaseUnchecked(Lock lock) {
        try {
            lock.release();
        } catch (LockingException e) {
            LOGGER.warn("Failed to release lock", e);
        }
    }
}
