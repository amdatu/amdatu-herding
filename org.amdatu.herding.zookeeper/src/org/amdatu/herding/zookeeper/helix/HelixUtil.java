/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.zookeeper.helix;

import org.apache.curator.framework.CuratorFramework;
import org.apache.helix.HelixAdmin;
import org.apache.helix.HelixManagerFactory;
import org.apache.helix.PropertyPathBuilder;
import org.apache.helix.manager.zk.ZKHelixManager;
import org.apache.helix.model.HelixConfigScope;
import org.apache.helix.model.InstanceConfig;
import org.apache.helix.model.builder.HelixConfigScopeBuilder;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.Instant.now;
import static java.time.Instant.ofEpochMilli;

public class HelixUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelixUtil.class);

    private HelixUtil() {
        // non-instantiable
    }

    public static void withHelixClassLoader(Runnable helixClassLoaderAction) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(HelixManagerFactory.class.getClassLoader());
            helixClassLoaderAction.run();
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    public static void ensureClusterExists(HelixAdmin helixAdmin, String clusterName) {
        List<String> clusters = helixAdmin.getClusters();
        if (!clusters.contains(clusterName)) {

            LOGGER.info("Creating Helix cluster '{}' ", clusterName);
            if (!helixAdmin.addCluster(clusterName)) {
                LOGGER.error("Failed to create Helix cluster '{}'", clusterName);
                return;
            }

            final HelixConfigScope scope = new HelixConfigScopeBuilder(HelixConfigScope.ConfigScopeProperty.CLUSTER)
                    .forCluster(clusterName)
                    .build();
            final Map<String, String> props = new HashMap<>();
            props.put(ZKHelixManager.ALLOW_PARTICIPANT_AUTO_JOIN, String.valueOf(true));
            helixAdmin.setConfig(scope, props);

            LOGGER.info("Helix cluster '{}' created", clusterName);
        }
    }


    /**
     * Remove stale instances from a Helix cluster.
     *
     * As we allow auto joining to this cluster the list of instances keeps growoing. This method can be used to remove
     * stale instances from the cluster.
     *
     * @param curatorFramework
     * @param helixAdmin
     * @param clusterName
     */
    public static void cleanInstancesList(CuratorFramework curatorFramework, HelixAdmin helixAdmin, String clusterName) throws Exception {
        List<String> instances = curatorFramework.getChildren().forPath(PropertyPathBuilder.instance(clusterName));
        List<String> liveInstances = curatorFramework.getChildren().forPath(PropertyPathBuilder.liveInstance(clusterName));
        instances.removeAll(liveInstances);

        if (instances.isEmpty()) {
            LOGGER.info("NOT DPOPPING any inactive instances");
        }

        instances.forEach(instance -> {
            try {
                Stat stat = curatorFramework.checkExists().forPath(PropertyPathBuilder.instance(clusterName, instance));
                Instant instant = ofEpochMilli((stat.getCtime()));
                if(instant.isBefore(now().minusMillis(1000))) {
                    LOGGER.info("DPOPPING instance: {}", instance);
                    InstanceConfig instanceConfig = InstanceConfig.toInstanceConfig(instance);
                    helixAdmin.dropInstance(clusterName, instanceConfig);
                    LOGGER.info("Dropped instance: {}", instance);
                } else {
                    LOGGER.info("Not dropping recent instance: {}", instance);
                }
            } catch (Exception e) {
                LOGGER.warn("Instance not dropped: '{}'", e.getMessage());
            }
        });
    }

}
