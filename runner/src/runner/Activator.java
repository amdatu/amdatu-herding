/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package runner;

import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) {
        // register partition election group
        manager.add(createComponent()
                .setImplementation(new ZookeeperConfigurator())
                .add(createServiceDependency()
                        .setService(ConfigurationAdmin.class)
                        .setRequired(true)));

        PartitionElectionGroup group = new PartitionElectionGroup("partitionelectiontest", 6);
        manager.add(createComponent()
                .setInterface(PartitionElectionGroup.class.getName(), null)
                .setImplementation(group));

        manager.add(createComponent()
                .setImplementation(new PartitionLeaderWatcher())
                .add(createServiceDependency()
                        .setService(PartitionLeader.class)
                        .setCallbacks("takePartitionLead", "dropPartitionLead")
                        .setRequired(false))
        );
    }

    class ZookeeperConfigurator {
        private volatile ConfigurationAdmin configurationAdmin;

        void start() throws IOException {
            Dictionary<String, Object> configuration = new Hashtable<>();
            configuration.put("connectString", "localhost:2181");
            configuration.put("namespace", "runner");
            configuration.put("nodeName", System.getProperty("nodename"));
            configurationAdmin.getConfiguration("org.amdatu.herding.zookeeper", null)
                    .update(configuration);
        }
    }

    class PartitionLeaderWatcher {

        private final Logger logger = LoggerFactory.getLogger(PartitionLeaderWatcher.class);

        void takePartitionLead(ServiceReference<?> ref) {
            Integer partition = (Integer) ref.getProperty(PartitionElectionConstants.PARTITION);
            logger.info("Taking partition lead for partition: {}", partition);
        }

        void dropPartitionLead(ServiceReference<?> ref) {
            Integer partition = (Integer) ref.getProperty(PartitionElectionConstants.PARTITION);
            logger.info("Dropping partition lead for partition: {}", partition);
        }
    }

}
