/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode.partitionelection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.partitionelection.PartitionElectionConstants;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;

public class SingleNodePartitionElectionService implements PartitionElectionService {

    private volatile DependencyManager dependencyManager;

    private volatile PartitionElectionGroup electionGroup;

    private volatile NodeService nodeService;
    private final List<Component> partitionLeaderComponents = new ArrayList<>();

    void start(Component component) {
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(PartitionElectionConstants.GROUP_NAME, electionGroup.name());
        component.setServiceProperties(serviceProperties);


        synchronized (partitionLeaderComponents) {
            for (int partition = 0; partition < electionGroup.partitions(); partition++) {
                Dictionary<String, Object> properties = new Hashtable<>();
                properties.put(PartitionElectionConstants.GROUP_NAME, electionGroup.name());
                properties.put(PartitionElectionConstants.PARTITION, partition);
                partitionLeaderComponents.add(dependencyManager.createComponent()
                        .setInterface(PartitionLeader.class, properties)
                        .setImplementation(new PartitionLeader(electionGroup.name(), partition))
                );
            }

        }
        partitionLeaderComponents.forEach(dependencyManager::add);
    }

    void stop() {
        ArrayList<Component> toBeRemoved;
        synchronized (partitionLeaderComponents) {
            toBeRemoved = new ArrayList<>(partitionLeaderComponents);
            partitionLeaderComponents.clear();
        }
        toBeRemoved.forEach(dependencyManager::remove);
    }


    @Override
    public boolean isLeader(int partition) {
        if (partition >= electionGroup.partitions()) {
            throw new IllegalArgumentException("Partition " + partition + " does not exist");
        }
        return true;
    }

    @Override
    public String getLeader(int partition) {
        if (partition >= electionGroup.partitions()) {
            throw new IllegalArgumentException("Partition " + partition + " does not exist");
        }
        return nodeService.getName();
    }

    @Override
    public Collection<String> getElectionCandidates() {
        return Collections.singleton(nodeService.getName());
    }
}
