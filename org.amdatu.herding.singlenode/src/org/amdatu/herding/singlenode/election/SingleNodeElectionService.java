/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode.election;


import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.Leader;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentDeclaration;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ElectionService} implementation that assumes the cluster consists of a single node. This node will always be
 * "elected" leader node.
 */
public class SingleNodeElectionService implements ElectionService {

    private static final Logger LOG = LoggerFactory.getLogger(SingleNodeElectionService.class);

    @SuppressWarnings("unused")
    private volatile DependencyManager dependencyManager;

    @SuppressWarnings("unused")
    private volatile NodeService nodeService;

    @SuppressWarnings("unused")
    private volatile Component component;

    private ElectionGroup electionGroup;
    private Component leaderComponent;

    protected final void updated(ElectionGroup electionGroup) throws ConfigurationException {
        this.electionGroup = electionGroup;

        if (this.electionGroup == null) {
            return;
        }

        //noinspection ConstantConditions
        if (this.electionGroup.name() == null || this.electionGroup.name().trim().isEmpty()) {
            throw new ConfigurationException("name", "name is required");
        }
        if (component.getComponentDeclaration().getState() == ComponentDeclaration.STATE_REGISTERED) {
            stop();
            start();
        }
    }

    @SuppressWarnings("unused")
    protected final void start() {
        LOG.info("Registering Leader service for group {}", electionGroup.name());
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(Leader.GROUP_NAME, electionGroup.name());

        leaderComponent = dependencyManager.createComponent()
                .setInterface(Leader.class.getName(), properties)
                .setImplementation(new Leader());
        dependencyManager.add(leaderComponent);

        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        if (serviceProperties == null) {
            serviceProperties = new Hashtable<>();
        }
        serviceProperties.put(Leader.GROUP_NAME, electionGroup.name());
        component.setServiceProperties(serviceProperties);
    }

    @SuppressWarnings("unused")
    protected final void stop() {
        if (leaderComponent != null) {
            dependencyManager.remove(leaderComponent);
            leaderComponent = null;
        }
    }

    @Override
    public boolean isLeader() {
        return true;
    }

    @Override
    public String getLeader() {
        return nodeService.getName();
    }

    @Override
    public Collection<String> getElectionCandidates() {
        return Collections.singleton(nodeService.getName());
    }
}
