/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import org.amdatu.herding.ClusterService;
import org.amdatu.herding.NodeService;
import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.singlenode.election.SingleNodeElectionService;
import org.amdatu.herding.singlenode.locking.SingleNodeLockingService;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.singlenode.partitionelection.SingleNodePartitionElectionService;
import org.amdatu.herding.singlenode.partitioner.SingleNodePartitioner;
import org.amdatu.herding.singlenode.taskmanager.SingleNodeWorker;
import org.amdatu.herding.taskmanager.Worker;
import org.amdatu.herding.taskmanager.WorkerContext;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) {
        manager.add(createComponent()
                .setImplementation(new SingleNodeHerdingLauncher())
                .add(createConfigurationDependency()
                        .setPid(SingleNodeHerdingConfiguration.PID)
                        .setConfigType(SingleNodeHerdingConfiguration.class))
        );

        manager.add(createComponent()
                .setInterface(ClusterService.class.getName(), null)
                .setImplementation(SingleNodeClusterService.class)
                .add(createServiceDependency().setService(NodeService.class).setRequired(true))
        );

        manager.add(createFactoryComponent()
                .setFactoryPid(ElectionGroup.PID)
                .setConfigType(ElectionGroup.class)
                .setInterface(ElectionService.class.getName(), null)
                .setImplementation(SingleNodeElectionService.class)
                .setAutoConfig(Component.class, "component")
                .add(createServiceDependency().setService(NodeService.class).setRequired(true))
        );

        manager.add(createComponent()
                .setInterface(LockingService.class.getName(), null)
                .setImplementation(SingleNodeLockingService.class)
                .add(createServiceDependency().setService(NodeService.class).setRequired(true))
        );

        manager.add(createAdapterComponent()
                .setInterface(PartitionElectionService.class, null)
                .setImplementation(SingleNodePartitionElectionService.class)
                .setAdaptee(PartitionElectionGroup.class, null)
                .add(createServiceDependency().setService(NodeService.class).setRequired(true))
        );

        manager.add(createAdapterComponent()
                .setAdaptee(PartitionElectionGroup.class, null)
                .setInterface(Partitioner.class.getName(), null)
                .setImplementation(SingleNodePartitioner.class)
        );

        manager.add(createAdapterComponent()
                .setAdaptee(WorkerContext.class, null)
                .setInterface(Worker.class, null)
                .setImplementation(SingleNodeWorker.class)
        );
    }

    class SingleNodeHerdingLauncher {

        private volatile SingleNodeHerdingConfiguration configuration;
        private volatile Component nodeServiceComponent;

        protected final void updated(SingleNodeHerdingConfiguration configuration) {
            this.configuration = configuration;
        }

        protected final void start() {
            nodeServiceComponent = createComponent()
                    .setInterface(NodeService.class.getName(), null)
                    .setImplementation(new NodeServiceImpl(configuration.nodeName()));
            getDependencyManager().add(nodeServiceComponent);
        }

        protected final void stop() {
            if (nodeServiceComponent != null) {
                getDependencyManager().remove(nodeServiceComponent);
            }
        }
    }

}
