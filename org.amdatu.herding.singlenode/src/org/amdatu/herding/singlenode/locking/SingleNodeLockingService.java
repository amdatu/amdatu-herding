/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode.locking;

import org.amdatu.herding.NodeService;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingException;
import org.amdatu.herding.locking.LockingService;

import java.lang.ref.WeakReference;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * {@link LockingService} implementation that assumes the cluster consists of a single node. Locks provided by this
 * implementation are backed by a {@link ReentrantLock}
 */
public class SingleNodeLockingService implements LockingService {

    private static final ConcurrentHashMap<String, WeakReference<Semaphore>> semaphores = new ConcurrentHashMap<>();


    private volatile ScheduledExecutorService executorService;
    private volatile NodeService nodeService;

    protected final void start() {
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(() -> {
            synchronized (semaphores) {
                semaphores.entrySet().removeIf(ref -> ref.getValue().get() == null);
            }
        }, 60, 60, TimeUnit.SECONDS);

    }

    protected final void stop() {
        executorService.shutdown();
        executorService = null;

    }

    @Override
    public Lock getLock(String name) {

        Semaphore lock = getSemaphore(name);


        return new SingleNodeLock(lock);
    }



    @Override
    public Lock getReentrantLock(String name) {


        return new ReentrantSingleNodeLock(getSemaphore(name));
    }

    private Semaphore getSemaphore(String name) {
        Semaphore semaphore = null;
        WeakReference<Semaphore> lockWeakReference = semaphores.get(name);
        if (lockWeakReference != null) {
            semaphore = lockWeakReference.get();
        }

        if (semaphore == null) {
            synchronized (semaphores) {
                lockWeakReference = semaphores.get(name);
                if (lockWeakReference != null) {
                    semaphore = lockWeakReference.get();
                }

                if (semaphore == null) {
                    semaphore = new Semaphore(1);
                    semaphores.put(name, new WeakReference<>(semaphore));
                }
            }
        }
        return semaphore;
    }

    public class SingleNodeLock implements Lock {

        private final Semaphore lock;
        private final AtomicBoolean acquired = new AtomicBoolean(false);

        SingleNodeLock(Semaphore lock) {
            this.lock = lock;
        }

        @Override
        public boolean lock(long timeout) throws InterruptedException {
            boolean locked = lock.tryAcquire(timeout, TimeUnit.MILLISECONDS);
            if (locked) {
                acquired.set(true);
            }
            return locked;
        }

        @Override
        public void release() throws LockingException {
            if (acquired.compareAndSet(true, false)) {
                lock.release();
            }
        }

        @Override
        public boolean acquired() {
            return acquired.get();
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public String getLockOwner() {
            return nodeService.getName();
        }
    }

    public class ReentrantSingleNodeLock extends SingleNodeLock {

        private AtomicInteger lockCount = new AtomicInteger(0);
        private Thread owner;

        public ReentrantSingleNodeLock(Semaphore lock) {
            super(lock);
        }

        @Override
        public boolean lock(long timeout) throws InterruptedException {
            if (owner == Thread.currentThread()) {
                lockCount.incrementAndGet();
                return true;
            }

            boolean gotLock = super.lock(timeout);

            if (gotLock) {
                owner = Thread.currentThread();
                lockCount.incrementAndGet();
            }

            return gotLock;
        }

        @Override
        public void release() throws LockingException {
            if (owner != Thread.currentThread()) {
                throw new LockingException("Lock owned by another thread");
            }

            if (lockCount.decrementAndGet() == 0) {
                owner = null;
                super.release();
            }
        }

        @Override
        public boolean acquired() {
            return owner == Thread.currentThread() && super.acquired();
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public String getLockOwner() {
            return nodeService.getName();
        }
    }

}
