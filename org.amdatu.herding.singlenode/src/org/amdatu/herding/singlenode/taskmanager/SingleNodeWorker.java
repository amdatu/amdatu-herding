/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode.taskmanager;

import org.amdatu.herding.taskmanager.*;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.annotation.api.Init;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static java.util.Arrays.stream;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class SingleNodeWorker implements Worker {

    // We use a single thread executor to make sure tasks don't run out of order. As this is intended as a local testing
    // implementation that should be good enough.
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final Map<WorkPackage, CompletableFuture<Void>> statusMap = new HashMap<>();

    private volatile WorkerContext workerContext;

    @Init
    void init(Component component) {
        Dictionary<Object, Object> serviceProperties = component.getServiceProperties();
        serviceProperties.put(Worker.WORKER_NAME, workerContext.name());
        component.setServiceProperties(serviceProperties);
    }

    @Override
    public void start(WorkPackage... workPackage) {
        stream(workPackage).forEach(this::start);
    }

    private void start(final WorkPackage workPackage) {
        CompletableFuture<Void> workPackageFuture = completedFuture(null);
        for (TaskGroup taskGroup : workPackage.getTaskGroups()) {
            workPackageFuture = workPackageFuture.thenCompose(v -> runTaskGroup(taskGroup));
        }
        statusMap.put(workPackage, workPackageFuture);
    }

    private CompletableFuture<Void> runTaskGroup(final TaskGroup taskGroup) {
        List<CompletableFuture<Void>> taskFutures = taskGroup.getTasks().stream()
                .map(this::runTask)
                .collect(toList());

        return CompletableFuture.allOf(taskFutures.toArray(new CompletableFuture[0]));
    }

    private CompletableFuture<Void> runTask(final Task task) {
        CompletableFuture<Void> taskFuture = new CompletableFuture<>();
        executor.submit(() -> {
            try {
                if (taskFuture.isCompletedExceptionally()) {
                    return;
                }

                TaskRunnable taskRunnable = workerContext.createTaskRunnable(task);

                if (taskRunnable == null) {
                    throw new IllegalStateException("Task runnable for task: " + task + " is null");
                }
                taskRunnable.run();
                taskFuture.complete(null);
            } catch (Throwable t) {
                taskFuture.completeExceptionally(t);
            }
        });

        return taskFuture;
    }

    @Override
    public void cancel(String workPackageId) {
        statusMap.entrySet().stream()
                .filter(e -> e.getKey().getId().equals(workPackageId))
                .findFirst()
                .orElseThrow(RuntimeException::new)
                .getValue()
                .cancel(false);
    }

    @Override
    public void delete(String workPackageId) {
        statusMap.entrySet().stream()
                .filter(e -> e.getKey().getId().equals(workPackageId))
                .findAny()
                .ifPresent(entry -> entry.getValue().cancel(false));
    }

    @Override
    public Status getStatus(String workPackageId) {
        try {
            CompletableFuture<Void> future = statusMap.entrySet().stream()
                    .filter(e -> e.getKey().getId().equals(workPackageId))
                    .findFirst()
                    .map(Map.Entry::getValue)
                    .orElse(null);

            if (future == null) {
                return null;
            } else if (future.isCompletedExceptionally()) {
                return Status.FAILED;
            } else if (future.isDone()) {
                return Status.COMPLETED;
            } else {
                return Status.IN_PROGRESS;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, String> listRunning() {
        return statusMap.entrySet().stream()
                .filter(e -> !e.getValue().isDone())
                .map(Map.Entry::getKey)
                .collect(toMap(WorkPackage::getId, WorkPackage::getName));
    }

    @Override
    public Status awaitCompletion(String workPackageId, long timeout) throws InterruptedException, TimeoutException {
        CompletableFuture<Void> workPackageFuture = statusMap.entrySet().stream()
                .filter(e -> e.getKey().getId().equals(workPackageId))
                .findFirst()
                .map(Map.Entry::getValue)
                .orElseThrow(RuntimeException::new);
        try {
            if (workPackageFuture.isCompletedExceptionally()) {
                return Status.FAILED;
            } else {
                workPackageFuture.get(timeout, TimeUnit.MILLISECONDS);
                return Status.COMPLETED;
            }
        } catch (ExecutionException | TimeoutException e) {
            return Status.FAILED;
        }

    }

}
