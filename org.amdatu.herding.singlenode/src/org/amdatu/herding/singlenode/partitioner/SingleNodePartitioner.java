/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode.partitioner;

import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitioner.Partitioner;

import java.util.*;

/**
 * Simple partitioner implementation that assigns all resources to partition 0
 * for use with a single node scenario.
 */
public class SingleNodePartitioner implements Partitioner {

    private volatile PartitionElectionGroup partitionElectionGroup;
    private Set<String> resources = new LinkedHashSet<>();

    @Override
    public String getTopic() {
        return partitionElectionGroup.name();
    }

    @Override
    public int getPartitionCount() {
        return partitionElectionGroup.partitions();
    }

    @Override
    public int computePartitionForResource(String resourceName) {
        resources.add(resourceName);
        return 0; // all resources can go into the first partition
    }

    @Override
    public Map<Integer, List<String>> getPartitionAssignments() {
        Map<Integer, List<String>> assignments = new HashMap<>();
        assignments.put(0, new ArrayList<>(resources));
        return assignments;
    }

    @Override
    public void discardResource(String resourceName) {
        // nothing to do here
    }

    @Override
    public void partitionsChanged() {
        // nothing to do here
    }
}
