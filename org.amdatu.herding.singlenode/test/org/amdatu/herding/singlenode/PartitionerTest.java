/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.amdatu.herding.partitioner.Partitioner;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.testsupport.config.ConfigUtils;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PartitionerTest extends SinglenodeTestBase {
    private static final String NO_PARTITIONER_SERVICE = "No partitioner service!";
    private static final int TIMEOUT = 5000;
    private static final String NO_PARTITION_LEADER = "No partition leader!";

    @Test
    public void testBasicPartitioning() throws Exception {
        PartitionElectionGroup partitionElectionGroup = new PartitionElectionGroup("testBasicPartitioning", 3);

        // Launchpad frameworkTwo = getLaunchpad("fwTwo")
        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {
            ConfigUtils.updateConfiguration(frameworkOne, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("fwOne")
            );
            registerPartitionElectionGroup(frameworkOne, partitionElectionGroup);
            frameworkOne.waitForService(PartitionLeader.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITION_LEADER));

            computePartition(frameworkOne, "a");
            computePartition(frameworkOne, "b");
            computePartition(frameworkOne, "c");

            Partitioner partitionerOne = frameworkOne.waitForService(Partitioner.class, TIMEOUT).orElseThrow(() -> new IllegalStateException(NO_PARTITIONER_SERVICE));
            assertEquals(3, partitionerOne.getPartitionCount());
            Map<Integer, List<String>> partitionAssignments = partitionerOne.getPartitionAssignments();
            assertEquals(1, partitionAssignments.size());

            assertPartitionAssignment("a", 0, partitionAssignments);
            assertPartitionAssignment("b", 0, partitionAssignments);
            assertPartitionAssignment("c", 0, partitionAssignments);

        }
    }

    private void assertPartitionAssignment(String resource, int partition, Map<Integer, List<String>> partitionAssignments) {
        List<String> resources = partitionAssignments.get(partition);
        assertTrue(resources.contains(resource));
    }

    private int computePartition(Launchpad framework, String resource) {
        return framework.waitForService(Partitioner.class, 1000)
                .map(partitioner -> partitioner.computePartitionForResource(resource))
                .orElseThrow(() -> new AssertionError("Partitioner service not available"));
    }

    private void registerPartitionElectionGroup(Launchpad framework, PartitionElectionGroup electionGroup) {
        framework.register(PartitionElectionGroup.class, electionGroup);
    }
}
