/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;


import java.util.List;

import org.amdatu.herding.election.ElectionGroup;
import org.amdatu.herding.election.ElectionService;
import org.amdatu.herding.election.Leader;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.testsupport.config.ConfigUtils;
import org.junit.jupiter.api.Test;
import org.osgi.service.cm.Configuration;

import aQute.launchpad.Launchpad;
import static org.amdatu.herding.election.Leader.groupFilter;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ElectionTest extends SinglenodeTestBase {

    @Test
    public void participatingInLeaderElection() throws Exception {
        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );


            List<Leader> leaderServices = launchpad.getServices(Leader.class);

            assertEquals(0, leaderServices.size());


            Configuration factoryConfiguration = ConfigUtils.createFactoryConfiguration(launchpad, ElectionGroup.PID, ElectionGroup.class,
                    configuration -> configuration.set(ElectionGroup::name).value("testGroup")
            );

            launchpad.waitForService(Leader.class, 1000).get();

            leaderServices = launchpad.getServices(Leader.class, null);
            assertEquals(1, leaderServices.size());

            assertEquals(1, launchpad.getServices(ElectionService.class, groupFilter("testGroup")).size());

            factoryConfiguration.delete();

            Thread.sleep(100); // NOSONAR
            leaderServices = launchpad.getServices(Leader.class, null);
            assertEquals(0, leaderServices.size());
        }
    }
}
