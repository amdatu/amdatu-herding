/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import aQute.launchpad.Launchpad;
import org.amdatu.herding.taskmanager.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SingleNodeWorkerTest extends SinglenodeTestBase {
    private static class TestWorkerContext implements WorkerContext {

        final TestInfo testInfo;

        final LinkedHashMap<Task, String> executionHistory = new LinkedHashMap<>();

        private TestWorkerContext(TestInfo testInfo) {
            this.testInfo = testInfo;
        }

        @Override
        public String name() {
            return testInfo.getTestMethod().orElseThrow(IllegalStateException::new).getName();
        }

        @Override
        public TaskRunnable createTaskRunnable(Task task) {

            return new TaskRunnable() {
                @Override
                public void run() {
                    executionHistory.put(task, "todo");
                }

                @Override
                public void cancel() {

                }
            };
        }
    }

    @Test
    public void taskWorkerTest(TestInfo testInfo) throws Exception {

        try (Launchpad launchpad = getLaunchpad("fwTwo")) {

            TestWorkerContext testWorkerContext = new TestWorkerContext(testInfo);

            launchpad.register(WorkerContext.class, testWorkerContext);

            Optional<Worker> service = launchpad.waitForService(Worker.class, 5000);
            Worker worker = service.orElseThrow(AssertionError::new);

            WorkPackage workPackage = WorkPackage.builder()
                    .name("test-workPackage")
                    .addTaskGroup(createTaskGroup("osgi-command", 10))
                    .build();
            worker.start(workPackage);

            worker.awaitCompletion(workPackage.getId(), 2000);

            assertEquals(10, testWorkerContext.executionHistory.size());
            assertTrue(testWorkerContext.executionHistory.keySet().stream()
                    .map(Task::getCommand)
                    .allMatch("osgi-command"::equals));
        }
    }

    @Test
    public void failingTestTaskShouldNotBeExecuted(TestInfo testInfo) throws Exception {
        List<String> executedTasks = Collections.synchronizedList(new ArrayList<>());

        try (Launchpad launchpad = getLaunchpad("fwTwo")) {

            TestWorkerContext testWorkerContext = new TestWorkerContext(testInfo){
                @Override
                public TaskRunnable createTaskRunnable(Task task) {
                    return new TaskRunnable() {
                        @Override
                        public void run() {
                            if(task.getName().startsWith("second_")) {
                                throw new RuntimeException("The second test should fail");
                            }
                            executedTasks.add(task.getName());
                        }

                        @Override
                        public void cancel() {
                        }
                    };
                }
            };

            launchpad.register(WorkerContext.class, testWorkerContext);

            Optional<Worker> service = launchpad.waitForService(Worker.class, 5000);
            Worker worker = service.orElseThrow(AssertionError::new);

            WorkPackage workPackage = WorkPackage.builder()
                    .name("test-workPackage")
                    .addTaskGroup(createTaskGroup("osgi-command", 2,"first_"))
                    .addTaskGroup(createTaskGroup("osgi-command", 3,"second_"))
                    .addTaskGroup(createTaskGroup("osgi-command", 2,"third_"))
                    .build();
            worker.start(workPackage);

            Status status = worker.awaitCompletion(workPackage.getId(), 2000);
            assertEquals(Status.FAILED,status);

            assertTrue(executedTasks.size() == 2, "Expected only tasks from the first task group to be executed, actual is " + String.join(",", executedTasks));
            assertTrue(executedTasks.contains("first_0"));
            assertTrue(executedTasks.contains("first_1"));
        }
    }

    private TaskGroup createTaskGroup(String command, int noTasks) {
        return createTaskGroup(command, noTasks, "Task ");
    }

    private TaskGroup createTaskGroup(String command, int noTasks, String taskPrefix) {
        TaskGroup.Builder taskGroupBuilder = TaskGroup.builder().name(taskPrefix+"GROUP");
        for (int i = 0; i < noTasks; i++) {
            taskGroupBuilder.addTask(Task.builder()
                    .command(command)
                    .name(taskPrefix + i)
                    .build());
        }
        return taskGroupBuilder.build();
    }
}