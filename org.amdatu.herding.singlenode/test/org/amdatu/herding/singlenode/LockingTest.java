/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import aQute.launchpad.Launchpad;
import aQute.launchpad.Service;
import org.amdatu.herding.locking.Lock;
import org.amdatu.herding.locking.LockingService;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.testsupport.config.ConfigUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LockingTest extends SinglenodeTestBase {

    @Service(timeout = 2000)
    private volatile LockingService lockingService;

    @Test
    public void obtainLock() throws Exception {
        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );

            launchpad.inject(this);

            Lock lock = lockingService.getLock("test-lock");

            assertTrue(lock.lock(0));
            assertTrue(lock.acquired());

            lock.release();
            assertFalse(lock.acquired());


        }
    }

    @Test
    public void obtainReentrantLock() throws Exception {
        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );

            launchpad.inject(this);

            Lock lock = lockingService.getReentrantLock("test-lock");

            assertTrue(lock.lock(0));
            assertTrue(lock.acquired());

            lock.release();
            assertFalse(lock.acquired());
        }
    }

    @Test
    public void obtainLockTwice() throws Exception {
        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );

            launchpad.inject(this);

            Lock lock1 = lockingService.getLock("test-lock");
            Lock lock2 = lockingService.getLock("test-lock");

            assertTrue(lock1.lock(0));
            assertFalse(lock2.lock(0));
            assertTrue(lock1.acquired());
            assertFalse(lock2.acquired());

            lock1.release();
            assertFalse(lock1.acquired());
            assertFalse(lock2.acquired());

            assertTrue(lock2.lock(0));
            assertFalse(lock1.acquired());
            assertTrue(lock2.acquired());
        }

    }

    @Test
    public void obtainReentrantLockTwice() throws Exception {
        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );

            launchpad.inject(this);

            Lock lock1 = lockingService.getReentrantLock("test-lock");
            Lock lock2 = lockingService.getReentrantLock("test-lock");

            assertTrue(lock1.lock(0));
            assertFalse(lock2.lock(0));
            assertTrue(lock1.acquired());
            assertFalse(lock2.acquired());

            lock1.release();
            assertFalse(lock1.acquired());
            assertFalse(lock2.acquired());

            assertTrue(lock2.lock(0));
            assertFalse(lock1.acquired());
            assertTrue(lock2.acquired());
        }

    }

}
