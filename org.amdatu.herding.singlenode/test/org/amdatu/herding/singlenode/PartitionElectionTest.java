/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import java.util.Optional;

import org.amdatu.herding.partitionelection.PartitionElectionGroup;
import org.amdatu.herding.partitionelection.PartitionElectionService;
import org.amdatu.herding.partitionelection.PartitionLeader;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.testsupport.config.ConfigUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import aQute.launchpad.Launchpad;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PartitionElectionTest extends SinglenodeTestBase {


    public static final String TEST_ELECTION_GROUP_NAME = "test-election-group";

    @Test
    public void singleNodeTwoPartitions(TestInfo testInfo) throws Exception {

        try (Launchpad frameworkOne = getLaunchpad("fwOne")) {

            ConfigUtils.updateConfiguration(frameworkOne, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("fwOne")
            );

            frameworkOne.register(PartitionElectionGroup.class, new PartitionElectionGroup(TEST_ELECTION_GROUP_NAME, 2));

            long count = frameworkOne.getServices(PartitionLeader.class, null, 2, 5000, false).size();
            assertEquals(2, count);

            Optional<PartitionElectionService> partitionElectionServiceOpt = frameworkOne.waitForService(PartitionElectionService.class, 2000, PartitionLeader.groupFilter(TEST_ELECTION_GROUP_NAME));
            assertTrue(partitionElectionServiceOpt.isPresent());

            PartitionElectionService partitionElectionService = partitionElectionServiceOpt.get();
            assertEquals("fwOne", partitionElectionService.getLeader(0));
            assertEquals("fwOne", partitionElectionService.getLeader(0));
            assertThrows(IllegalArgumentException.class, () -> partitionElectionService.getLeader(2));

            assertTrue(partitionElectionService.isLeader(0));
            assertTrue(partitionElectionService.isLeader(1));
            assertThrows(IllegalArgumentException.class, () -> partitionElectionService.isLeader(2));
        }
    }

}
