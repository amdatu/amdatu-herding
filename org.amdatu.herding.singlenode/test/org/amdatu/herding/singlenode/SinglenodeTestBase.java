/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import aQute.launchpad.Launchpad;
import aQute.launchpad.LaunchpadBuilder;
import static org.amdatu.herding.testsupport.config.ConfigUtils.updateConfiguration;

public class SinglenodeTestBase {

    protected Launchpad getLaunchpad(String nodeName) {
        Launchpad launchpad = new LaunchpadBuilder() // NOSONAR
                .bndrun("election-test.bndrun")
                .excludeExport("org.slf4j*")
                .notestbundle()
                .create();

        updateConfiguration(launchpad, "org.ops4j.pax.logging", config -> config
                .set("log4j.rootLogger").value("WARN, stdout")
                .set("log4j.appender.stdout").value("org.apache.log4j.ConsoleAppender")
                .set("log4j.appender.stdout.layout").value("org.apache.log4j.PatternLayout")
                .set("log4j.appender.stdout.layout.ConversionPattern").value(nodeName + ": %d{HH:mm:ss} %-5p %c{1}:%L - %m%n")
                .set("log4j.logger.org.amdatu").value("INFO")
        );
        return launchpad;
    }

}
