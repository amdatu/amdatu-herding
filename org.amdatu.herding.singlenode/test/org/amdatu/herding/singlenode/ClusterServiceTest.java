/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.herding.singlenode;

import org.amdatu.herding.ClusterService;
import org.amdatu.herding.singlenode.metatype.SingleNodeHerdingConfiguration;
import org.amdatu.herding.testsupport.config.ConfigUtils;
import org.junit.jupiter.api.Test;

import aQute.launchpad.Launchpad;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ClusterServiceTest extends SinglenodeTestBase {

    @Test
    public void getMembers() throws Exception {

        try (Launchpad launchpad = getLaunchpad("test-node")) {
            ConfigUtils.updateConfiguration(launchpad, SingleNodeHerdingConfiguration.PID, SingleNodeHerdingConfiguration.class,
                    configuration -> configuration.set(SingleNodeHerdingConfiguration::nodeName).value("test-node")
            );

            ClusterService clusterService = launchpad.waitForService(ClusterService.class, 2000).get();

            assertEquals(1, clusterService.getMembers().size());
            assertEquals("test-node", clusterService.getMembers().iterator().next());
        }

    }

}
